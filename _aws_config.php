<?php return array(
    'services' => array(
        'default_settings' => array(
            'params' => array()
        ),
        'dynamodb' => array(
            'alias'   => 'DynamoDb',
            'extends' => 'default_settings',
            'class'   => 'Aws\DynamoDb\DynamoDbClient',
            'params'  => array(
                'key'    => '<Access Key Id>',
                'secret' => '<Secret Access Key>',
                'region' => 'us-east-1'
            )
        ),
        's3' => array(
            'alias'   => 'S3',
            'extends' => 'default_settings',
            'class'   => 'Aws\S3\S3Client'
        )
    )
);
?>
