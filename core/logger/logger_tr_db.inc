<?php
/*
 * Created on 20.09.2008
 *
 * транспорт для логера, пишет в сообщения в СУБД
 *
 *  * Возможные предопределения:
 *
 * Logger_TRDB_MainCacheSize - integer, определяет максимально возможное
 *      число записей в таблице logger_base, предназначенной для хранения
 *	корневых записей logger. При переполнении таблицы старые записи
 *	будут удаляться вместе со связанными записями из таблицы
 *	logger_message.
 *      Default: 10000
 *
 *  * Logger_TRDB_DelPart - integer, определяет размер порции записей,
 *	которые будут удалены из таблицы logger_base при переполнении.
 *      Default: 10
 */

if ( ! defined('Logger_TRDB_MainCacheSize') )
    define('Logger_TRDB_MainCacheSize', 10000);
if ( ! defined('Logger_TRDB_DelPart') )
    define('Logger_TRDB_DelPart', 10);

 require_once 'logger_transport.inc';
 require_once 'logger_transport_class.inc';


 class logger_tr_db extends logger_transport_class implements logger_transport {
 
 /*
  * этот транспорт выводит сообщения на stdout, т.е. непосредственно в ответ
  * http-сервера на страницу браузера
  */

	private static $Transport = null;
        private static $ClearOK = FALSE;

 	static function GetTransport() {

		if ( self::$Transport == null ) {
			self::$Transport = new logger_tr_db;
		}
		
		return self::$Transport;
 
 	}
 
	function TransferMessage($MSG, $LoggerID, $Destination) {

                $SQL = $Destination['SQL'];
                $TableMSG = $Destination['TablePrefix'] . 'message';

                $SQL->query("insert into `${TableMSG}` set
                    `LoggerID` = '${LoggerID}',
                    `MTime` = '" . $MSG->MTime['Cur_time'] . "',
                    `MType` = '" . $MSG->MText['Type'] . "',
                    `File` = '" . $MSG->MFile['Filename'] . "',
                    `Line` = '" . $MSG->MFile['Line'] . "',
                    `MText` = '" . $SQL->escape_string($MSG->MText['Message']) . "',
                    `MCode` = '" . $SQL->escape_string($MSG->MText['Code']) . "',
                    `MTrace` = '" . $SQL->escape_string(serialize($MSG->MTrace)) . "'
                ", __file__, __LINE__);


//                echo '<hr><pre>';
//                echo $MSG->toString();

	}

	function MakeDestination($Logger, $param) {
		/*
                 * $Logger - указатель на текущий экземпляр logger'а
		 * $param - массив параметров для создания Destination
                 * Возвращает массив $ret, который сохраняется внутри logger для
                 * дальнейшего использования при обработки сообщений данного
                 * типа.
                 *
		 * формат массива $param:
                 * SQL - ссылка на объект типа SQL с установленным коннектором
                 *       к нужной схеме СУБД. Будет использоваться для проверки
                 *       и работы с необходимыми logger таблицами
                 * TablePrefix - префикс таблиц которые транспорт проверяет или
                 *       создает в схеме, к которой подключен SQL.
                 *       Default: 'logger_'
                 *       Обычно создается пара таблиц: logger_base для хранения
                 *       ключей-логгеров и logger_message - для собственно
                 *       сообщений.
                 *
                 * формат массива $ret:
                 * SQL - коннектор к СУБД
                 * TablePrefix - префикс таблиц
                 *
                 * Можно использовать различные настройки одного и того же
                 * транспорта для сохранения сообщений разного типа. Т.е.
                 * сообщения разного типа могут иметь разные схемы/серверы для
                 * сохранения.
		 * 
		 */

                $ret = array();

                if (
                        ! isset($param['TablePrefix'])
                        or empty($param['TablePrefix'])
                ) {
                    $ret['TablePrefix'] = 'logger_';
                } else {
                    $ret['TablePrefix'] = $param['TablePrefix'];
                }

                if (
                        ! isset($param['SQL'])
                        or ! $param['SQL'] instanceof SQL
                )
                    throw new Exception("param['SQL'] is not inscance of SQL.");

                $SQL = $ret['SQL'] = $param['SQL'];
                $TableBase = $ret['TablePrefix'] . 'base';
                $TableMSG = $ret['TablePrefix'] . 'message';
                $TableTMP = $ret['TablePrefix'] . 'temp';
                /*
                 * *** структура таблиц ***
                 *
                 * ** base
                 * id
                 * // keys:
                 * ServerName
                 * RequestURI
                 * QueryString
                 * RemoteHost
                 * Section
                 * Module
                 * Block
                 * SaveDays
                 * // info:
                 * LoggerID
                 * LogStart
                 * InputPOST
                 * InputGET
                 * InputCoockie
                 *
                 * ** message
                 * id
                 * // keys:
                 * LoggerID
                 * File
                 * Line
                 * // info:
                 * MTime
                 * MType
                 * MText
                 * MCode
                 * MTrace
                 *
                 *
                 */

                if ( ! $SQL->checkTable($TableBase) ) {
                    $SQL->query("create table `${TableBase}` (
                            `id` int unsigned not null auto_increment,
                            `ServerName` varchar(100) not null default '',
                            `RequestURI` varchar(255) not null default '',
                            /*`QueryString` varchar(255) not null default '',*/
                            `RefererURI` varchar(255) not null default '',
                            `RemoteHost` int unsigned not null default 0,
                            `Section` varchar(50) not null default '',
                            `Module` varchar(50) not null default '',
                            `Block` varchar(50) not null default '',
                            `SaveDays` tinyint unsigned not null default 0,
                            `LoggerID` varchar(32) not null default '',
                            `LogStart` decimal(14,4) unsigned not null default 0,
                            `InputPOST` mediumtext,
                            `InputGET` text,
                            `InputCoockie` text,
                            PRIMARY KEY (`id`),
                            KEY (`ServerName`),
                            KEY (`RequestURI`),
                            /*KEY (`QueryString`),*/
                            KEY (`RefererURI`),
                            KEY (`RemoteHost`),
                            KEY (`Section`),
                            KEY (`Module`),
                            KEY (`Block`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin", __file__, __line__);
                    if ( ! $SQL->checkTable($TableBase) )
                        throw new Exception("Unable create $TableBase table.");
                    self::$ClearOK = TRUE;

                }

                if ( ! $SQL->checkTable($TableMSG) ) {
                    $SQL->query("create table `$TableMSG` (
                            `id` int unsigned not null auto_increment,
                            `LoggerID` varchar(32) not null default '',
                            `File` varchar(200) not null default '',
                            `Line` varchar(10) not null default '',
                            `MTime` decimal(14,4) unsigned not null default 0,
                            `MType` int unsigned not null default 0,
                            `MText` text,
                            `MCode` varchar(10) not null default '',
                            `MTrace` mediumtext,
                            PRIMARY KEY (`id`),
                            KEY (`LoggerID`)
                        ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin", __file__, __line__);
                    if ( ! $SQL->checkTable($TableMSG) )
                        throw new Exception("Unable create $TableMSG table.");
                }

                $SQL->query("select id from `$TableBase`
                        where `LoggerID` = '" . $Logger->GetLoggerID() . "'
                    ", __file__, __LINE__);
                if ( ! $SQL->fetch() ) {
                            /*`QueryString` = '" . $SQL->escape_string($Logger->SystemKeys['QueryString']) . "',*/
                    $SQL->query("insert into `$TableBase` set
                            `LoggerID` = '" . $Logger->GetLoggerID() . "',
                            `LogStart` = '" . $Logger->DTimeStart . "',
                            `ServerName` = '" . $SQL->escape_string($Logger->SystemKeys['ServerName']) . "',
                            `RequestURI` = '" . $SQL->escape_string($Logger->SystemKeys['RequestURI']) . "',
                            `RefererURI` = '" . $SQL->escape_string($Logger->SystemKeys['Referer']) . "',
                            `RemoteHost` = inet_aton('" . $Logger->SystemKeys['RemoteHost'] . "'),
                            `Section` = '" . $Logger->UserKeys['Section'] . "',
                            `Module` = '" . $Logger->UserKeys['Module'] . "',
                            `Block` = '" . $Logger->UserKeys['Block'] . "',
                            `InputPOST` = '" . $SQL->escape_string(serialize($_POST)) . "',
                            `InputGET` = '" . $SQL->escape_string(serialize($_GET)) . "',
                            `InputCoockie` = '" . $SQL->escape_string(serialize($_COOKIE)) . "'
                        ", __file__, __LINE__);

                }

                if ( ! self::$ClearOK ) {
                    // check and clear tables
                    $SQL->query("select count(*) as count from `${TableBase}`", __file__, __line__);
                    $SQL->fetch();
                    if ( $SQL->row['count'] > Logger_TRDB_MainCacheSize ) {
                        $SQL->query("create temporary table `${TableTMP}`
                            select `id` from `${TableBase}`
                            where unix_timestamp() - round(`LogStart`, 0) > `SaveDays` * 3600 * 24
                            order by `LogStart`
                            limit " . Logger_TRDB_DelPart . "
                        ", __FILE__, __LINE__);
                        $SQL->query("delete quick
                            `base`, `msg`, `tmp`
                            from `${TableBase}` as `base`,
                            `${TableMSG}` as `msg`,
                            `${TableTMP}` as `tmp`
                            where `base`.`LoggerID` = `msg`.`LoggerID`
                            and `base`.`id` = `tmp`.`id`
                        ", __FILE__, __LINE__);

                        $SQL->query("drop table `${TableTMP}`", __FILE__, __LINE__);
                    }

                    self::$ClearOK = TRUE;

                }

		return $ret;

	}
 
 
 }
?>
