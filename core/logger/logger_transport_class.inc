<?php
/*
 * Created on 20.09.2008
 *
 * базовый класс транспорта для логера
 * 
 */
 
 class logger_transport_class {
 	/*
 	 * базовый класс для создания расширений транспортов сообщений
 	 */

     public $CustomMsgType = array();    // тут сохраним нестандартные типы сообщений

     function toStringUserKeys($UserKeys) {
         /*
          * преобразует пользовательские ключи logger в строку
          */

         $ret = '';

         // block
         if (
                 isset( $UserKeys['Block'])
                 and $UserKeys['Block'] != ''
         )
            $ret = ":${UserKeys['Block']}${ret}";

         // module
         if (
                 isset( $UserKeys['Module'])
                 and ! empty( $UserKeys['Module'] )
         )
            $ret = ":${UserKeys['Module']}${ret}";
        elseif ( ! empty($ret) )
            $ret = ":${ret}";

         // section
         if (
                 isset( $UserKeys['Section'])
                 and ! empty( $UserKeys['Section'] )
         )
            $ret = "${UserKeys['Section']}${ret}";



         return $ret;

     }

        function GetTextMsgType($MsgType) {
            /*
             * возвращает текстовое название типа сообщения
             * LOG_EMERG - серьезная авария, крах системы, ппц
             * LOG_ALERT - серьезная ошибка, сбой
             * LOG_CRIT - ошибка, не позволяющая продолжать работу
             * LOG_ERR - ошибка
             * LOG_WARNING - предупреждение
             * LOG_NOTICE - нотация, сообщение о необходимости произвести корректировку
             * LOG_INFO - информационное сообщение
             * LOG_DEBUG - отладочная информация
             *
             */

            switch ($MsgType) {

                case 0:
                    return 'LOG_EMERG';
                case 1:
                    return 'LOG_ALERT';
                case 2:
                    return 'LOG_CRIT';
                case 3:
                    return 'LOG_ERR';
                case 4:
                    return 'LOG_WARNING';
                case 5:
                    return 'LOG_NOTICE';
                case 6:
                    return 'LOG_INFO';
                case 7:
                    return 'LOG_DEBUG';
                default:
                    if ( isset ($this->CustomMsgType[$MsgType]) )
                            return $this->CustomMsgType[$MsgType];
                    else
                        return "Type${MsgType}";

            }

        }

 }
 
?>
