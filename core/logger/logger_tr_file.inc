<?php
/*
 * Created on 20.09.2008
 *
 * транспорт для логера, пишет в файл
 */
 require_once 'logger_transport.inc';
 require_once 'logger_transport_class.inc';

 class logger_tr_file extends logger_transport_class implements logger_transport {

 /*
  * этот транспорт выводит сообщения в файл, указанный при привязке транспорта
  * к типу сообщений
  */

	private static $Transport = null;

 	static function GetTransport() {

		if ( self::$Transport == null ) {
			self::$Transport = new logger_tr_file;
		}

		return self::$Transport;

 	}

	function TransferMessage($MSG, $LoggerID, $Destination) {

		fwrite($Destination['FileHandler'], str_repeat('*', 25) . "\n");
		fwrite($Destination['FileHandler'], date('r') . "\n");
                foreach ( $Destination['SysKeys'] as $key => $val ) {
                    if ( empty($val) )
                        continue;
                    fwrite($Destination['FileHandler'], "${key}: ${val}\n");
                }
                fwrite($Destination['FileHandler'], "LoggerID: ${LoggerID}\n");
		fwrite($Destination['FileHandler'], str_repeat('-', 10) . "\n");
		fwrite($Destination['FileHandler'], "Logger{$Destination['UserKey']} message:\n");
                fprintf($Destination['FileHandler'],
                    "%s at %0.4f+%0.4f: [%s] %s in %s:%d\n",
                    $this->GetTextMsgType($MSG->MText['Type']),
                    $MSG->MTime['Start_time'],
                    $MSG->MTime['Cur_time'] - $MSG->MTime['Start_time'],
                    $MSG->MText['Code'],
                    $MSG->MText['Message'],
                    $MSG->MFile['Filename'],
                    $MSG->MFile['Line']
                );
                foreach ( $MSG->MTrace as $level => $MT ) {
                    fwrite($Destination['FileHandler'], "backtrace $level:\n");
                    fwrite($Destination['FileHandler'], $MSG->toStringTrace($MT));
                }
//                echo '<hr><pre>';
//                echo $MSG->toString();

                fflush($Destination['FileHandler']);

	}

	function MakeDestination($Logger, $param) {
		/*
                 * $Logger - указатель на текущий экземпляр logger'а
		 * $param - массив параметров для создания Destination
		 * 
                 * формат массива $param:
		 * FPath - путь к файлу, в который будут записываться сообщения
		 *
		 */

		$ret = array();

                $FH = fopen($param['FPath'], 'a');
                if ( $FH === FALSE )
                    throw new Exception("Unable open to write log-file '${param['FPath']}'.");
                $ret['FileHandler'] = $FH;

                $ret['SysKeys'] = $Logger->SystemKeys;
                $ret['UserKey'] = $this->toStringUserKeys($Logger->UserKeys);
                if ( ! empty($ret['UserKey']) )
                    $ret['UserKey'] = "[${ret['UserKey']}]";

		return $ret;

	}


 }
 ?>
