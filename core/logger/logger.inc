<?php
/*
 * Created on 14.09.2008
 *
 * Базовые типы сообщений:
 * LOG_EMERG - серьезная авария, крах системы, ппц
 * LOG_ALERT - серьезная ошибка, сбой
 * LOG_CRIT - ошибка, не позволяющая продолжать работу
 * LOG_ERR - ошибка
 * LOG_WARNING - предупреждение
 * LOG_NOTICE - нотация, сообщение о необходимости произвести корректировку
 * LOG_INFO - информационное сообщение
 * LOG_DEBUG - отладочная информация
 *
 * Краткое описание.
 * Для сообщений каждого типа может быть подключено произвольное кол-во
 * транспортов для доставки сообщения к месту хранения. При генерации сообщения
 * создается объект-сообщение, в котором формируется вся необходимая информация.
 * Далее этот обект-сообщение последовательно передается всем транспортам,
 * привязанным к сообщениям данного типа. После чего объект-сообщение
 * разрушается.
 *
 * Транспорты оформляются как ссылки на экземпляр класса, реализующий интерфейс
 * logger_transport, и храняться в массиве TransportMsgBind. Метод TransportBind
 * позволяет привязать требуемый транспорт(ы) к нужному типу сообщений.
 *
 * 
 * Возможные предопределения:
 *
 * Logger_MSG_Class - text, определяет тип сообщения. Должен содержать название
 *      класса, происходящего от logger_message. Файл с описание класса должен
 *      быть подключен на момент создания экземпляра класса. Или он должен
 *	иметь расширение .inc и быть размещен в том же каталоге, где находится
 *	сам logger.inc.
 *      Default: 'logger_msg_base'
 *
 * Logger_MSG_DefaultType - integer, определяет тип ссобщения в том случае, когда
 *      тип явно не задан в методе message.
 *      Default: LOG_NOTICE
 *
 * Logger_MSG_AutofillCode - boolean, определяет, будет ли автоматически
 *      формироваться код сообщения в случае отсутствия его явного указания при
 *      вызове message(). Если будет, код принимает вид:
 *      0_<номер строки>
 *      где <номер строки> - это номер строки исходного файла, где был вызван
 *      message().
 *      Default: TRUE
 *
 * Logger_MSG_TraceLevel - integer, определяет кол-во элементов backtrace,
 *      передаваемых в каждое сообщение.
 *      Default: 0
 *
 * Важные методы:
 *
 * GetLogger($section='', $module='',  $block='') - возвращает указатель на
 *      текущий объект. При отсутствии - создает новый с заданными ключами.
 * NewLogger($section='', $module='',  $block='') - возвращает указатель на
 *      новый экземпляр с заданными ключами.
 * CloneLogger - возвращает указатель на вновь созданный клон текущего
 *      экземпляра класса.
 * SetMainLogger - назначает собственный экземпляр класса текущим экземпляром и
 *      будет в дальнейшем использовать именно его в вызовах GetLogger и
 *      CloneLogger.
 * GetLoggerID - возвращает 32-символьный уникальный идентификатор текущего
 *      экземпляра logger.
 * message($msg, $code=NULL, $type=NULL, $level=100) - создает сообщение $msg
 *      типа $type с кодом $code. Передает это сообщение всем привязанным к типу
 *      $type транспортам. $level используется для ограничения вывода элементов
 *      цепочки backtrace.
 * SetDefaultMsgType - назначает тип сообщений по умолчанию. Т.е. в том случае,
 *      когда тип явно не указан в вызове message.
 * TransportBind($MsgType, $TransportName, $Parameters=NULL) - привязывает
 *      транспорт $TransportName к сообщениям типа $MsgType. Если объекта
 *      транспорта не существует - он будет создан. Для этого файл с описанием
 *      класса должен быть подключен, или иметь название вида
 *      "logger_tr_${TransportName}.inc" и находиться в том же каталоге, что
 *      и сам logger.inc. Класс транспорта должен иметь такое же имя
 *      "logger_tr_${TransportName}" и должен реализовывать интерфейс
 *      logger_transport.
 *      $Parameters может содержать конкретный набор данных для доставки
 *      сообщений типа $MsgType транспортом типа $TransportName, что позволяет,
 *      к примеру, сообщения разного типа доставлять по разным e-Mail. Формат
 *      $Parameters индивидуален для каждого типа транспорта.
 * TransportInit($MsgType) - сбрасывает все привязки транспорта для сообщений
 *      типа $MsgType
 * TODO: SetError_toLogger($ErrType, $MsgType) - устанавливает logger в качестве
 *      обработчика системных ошибок типа $ErrType. Ошибки фиксируются как
 *      сообщения типа $MsgType. Значение старого обработчика сохраняется в
 *      экземпляре класса и может быть восстановлено вызовом
 *      RestoreError_fromLogger($ErrType).
 * TODO: RestoreError_fromLogger($ErrType) - восстанавливает оригинальный обработчик
 *      системных ошибок типа $ErrType.
 *
 */

if ( ! defined('Logger_MSG_Class') )
    define('Logger_MSG_Class', 'logger_msg_base');

if ( ! defined('Logger_MSG_DefaultType') )
    define('Logger_MSG_DefaultType', LOG_NOTICE);

if ( ! defined('Logger_MSG_AutofillCode') )
    define('Logger_MSG_AutofillCode', TRUE);

if ( ! defined('Logger_MSG_TraceLevel') )
    define('Logger_MSG_TraceLevel', 0);

class logger {

    private static $Logger = null;

    var $DTime;                 // time-break
    var $DTimeStart;            // logger start at
    var $DLastTime;             // last messages time
    private $TransportMsgBind = array();
    var $LogMsg;                // ref to logger_msg object
    private $SelfPath;              // path to myself
    var $DefaultMsgType;
    var $UserKeys = array();
    var $SystemKeys = array();
    private $LoggerID;              // UniqLoggerID

    function __construct($section='', $module='',  $block='') {
		/*
		 * Каждое сообщение имеет набор системных и пользовательских
                 * ключей. Пользовательские ключи задаются параметрами
                 * конструктора logger и могут быть опущены. Системные ключи
                 * создаются конструктором logger автоматически.
		 */

		/*
		 * TODO: в зависимости от типа сообщения, можно указывать различный транспорт для фиксации
		 * к примеру, <= LOG_CRIT - слать на почту
		 * любые - писать в БД
		 * > LOG_INFO но < LOG_DEBUG - писать в лог-файл
		 * 
		 * из транспортов пока ожидаются:
		 * 1. рассылка по адресам eMail
		 * 2. запись в лог-файлы
		 * 3. запись в БД
		 * 4. stdout
		 * 
		 * сообщение каждого типа имеет в массиве $this->TransportMsgBind
		 * набор используемых транспортов в виде ссылки на объект транспорта с массивом параметров.
		 * при получении сообщения, генерируется вызов нужного транспорта с массивом связанных параметров
		 * + сформированный объект-сообщение
		 * 
		 */

        $this->_set_time();
        $this->DTimeStart = $this->DTime();

        $this->_setLoggerID();

        $this->SelfPath = dirname(__FILE__) . DIRECTORY_SEPARATOR;
        $this->DefaultMsgType = Logger_MSG_DefaultType;

        if ( ! class_exists(Logger_MSG_Class) )
            require_once $this->SelfPath . Logger_MSG_Class . '.inc';

        if (
            ! class_exists(Logger_MSG_Class)
            or ! in_array('logger_message', class_implements(Logger_MSG_Class) )
        )
            trigger_error("Class " . Logger_MSG_Class . " not exists or wrong type", E_USER_ERROR);

        $this->_transport_setup();

        $this->_setUserKeys($section, $module, $block);

        $this->SystemKeys['ServerName'] = $_SERVER['SERVER_NAME'];
        if ( isset($_SERVER["HTTP_REFERER"]) )
            $this->SystemKeys['Referer'] = $_SERVER["HTTP_REFERER"];
        else
            $this->SystemKeys['Referer'] = '';
        $this->SystemKeys['RequestURI'] = $_SERVER['REQUEST_URI'];
        $this->SystemKeys['RequestMethod'] = $_SERVER["REQUEST_METHOD"];
//        $this->SystemKeys['QueryString'] = $_SERVER['QUERY_STRING'];
        $this->SystemKeys['RemoteHost'] = $_SERVER['REMOTE_ADDR'];
        if ( isset($_SERVER['HTTP_X_FORWARDED_FOR']) )
            $this->SystemKeys['RemoteHost'] = $_SERVER['HTTP_X_FORWARDED_FOR'] . ' over ' . $this->SystemKeys['RemoteHost'];

        if ( defined('Logger_Transport_Type') ) {
                    /*
                     * забиндим тут все типы сообщений если нужно
                     */
        }

    }

    private function _setLoggerID() {
                /*
                 * генерирует и устанавливает уникальный LoggerID
                 */

        $this->LoggerID = md5(
                    $this->DTimeStart .
                    print_r($this->SystemKeys, TRUE) .
                    print_r($this->UserKeys, TRUE) .
                    mt_rand(PHP_INT_MAX / 10, PHP_INT_MAX)
                );

    }

    function GetLoggerID() {

                /*
                 * возвращает текущий идентификатор LoggerID
                 */

        return $this->LoggerID;

    }

    static function GetLogger($section='', $module='',  $block='') {
                /*
                 * возвращает указатель на текущий экземпляр класса
                 * или на вновь созданный, если текущий отсутствует
                 */

        if ( self::$Logger === null ) {
            self::$Logger = new logger($section, $module,  $block);
        }

        return self::$Logger;


    }

    function NewLogger($section='', $module='',  $block='') {
                /*
                 * возвращает указатель на новый уникальный экземпляр класса
                 */

        return new logger($section, $module,  $block);

    }

    function SetMainLogger() {
                /*
                 * устанавливает указатель на текущий экземпляр класса в
                 * статическое поле класса logger::Logger. Используется для
                 * переназначения экземпляра класса для последующих вызовов
                 * GetLogger()
                 */

        self::$Logger = $this;

    }

    function CloneLogger() {
                /*
                 * возвращает указатель на клон текущего экземпляра класса
                 */

        return clone $this;

    }

    function DTime() {
		/*
		 * возвращает текущее время в секундах с точностью до 4 знака
                 * после запятой
		 */

        list($usec, $sec) = explode(' ', microtime() );
        $time=(float) $usec + (float) $sec;
        return $time;

    }

    private function _time_interval() {
		/*
		 * возвращает интервал между временем логгера и текущим
		 */	

        return (float) round($this->DTime() - (float) $this->DTime, 4);

    }

    private function _time_startinterval() {
		/*
		 * возвращает интервал между временем старта логгера и текущим
		 */	

        return (float) round($this->DTime() - (float) $this->DTimeStart, 4);

    }

    private function _set_time() {

        $this->DLastTime = $this->DTime;
        $this->DTime = $this->DTime();

    }

    private function _setUserKeys($section='', $module='',  $block='') {
                /*
                 * Устанавливает пользовательские ключи
                 * При вызове без параметров - сбрасывает их
                 */

         $this->UserKeys['Section'] = $section;
         $this->UserKeys['Module'] = $module;
         $this->UserKeys['Block'] = $block;

    }


    private function _transport_setup() {
		/*
		 * привязка различных транспортов к миссагам разного типа
		 * настройка берется из define'ов'
		 * 
		 * TransportMsgBind['MsgType'] - тип сообщения, для которого актуален транспорт
		 * TransportMsgBind['MsgType'][N]['Transport'] - ссылка на объект-транспорт нужного типа
		 * TransportMsgBind['MsgType'][N]['Destination'] - массив с параметрами доставки для этого типа транспорта
                 * где N - порядковый номер транспорта для сообщений MsgType
		 */

        $this->TransportMsgBind = array();


    }

    function SetDefaultMsgType($MsgType) {
                /*
                 * Устанавливает общее значение типа сообщения по умолчанию
                 */

        $this->DefaultMsgType = $MsgType;

    }

    function TransportInit($MsgType) {
                /*
                 * Создает описатель транспорта для сообщений типа $MsgType.
                 * Если описатель уже существует, он будет очищен.
                 */

         $this->TransportMsgBind[$MsgType] = array();

    }

    function TransportBind($MessageType, $TransportName, $Parameters=NULL) {
		/*
		 * Определяет нужный транспорт к нужному типу сообщений.
                 * Возможно множественное определение различных или однотипных
                 * транспортов для разных типов сообщений.
                 *
		 * $MessageType - тип сообщения. Может быть типа integer и
                 * соответствовать предопределенному типу сообщения. Или может
                 * быть строкой в которой математическое условия предваряет
                 * тектовое название константы или соответствующий ей цифровой
                 * код сообщения.
                 * В первом случае транспорт будет вызываться по обычному
                 * стандарту, т.е. будут обработаны все сообщения, чей тип меньше
                 * или равен заданному $MessageType. Это стандартный способ обработки
                 * сообщений для многих систем.
                 * Во втором случае транспорт будет вызываться для всех сообщений,
                 * код (тип) которых удовлетворяет условию. В качестве примера,
                 * рассмотрим вызов TransportBind с параметром $MessageType
                 * нескольких видов:
                 * '< LOG_ERR'    - будут передавать транспорту сообщения
                 * типов от LOG_EMERG до LOG_ERR включительно.
                 * 'LOG_ERR'      - альтернативный синтаксис предыдущего
                 * 3              - альтернативный синтаксис предыдущего
                 * '>LOG_DEBUG'   - все сообщения типов больше LOG_DEBUG
                 * '==LOG_CRIT'    - все сообщения типа LOG_CRIT и только
                 * '==2'           - альтернативный синтаксис предыдущего
                 * '>=LOG_NOTICE' - все сообщения типов LOG_NOTICE, LOG_INFO,
                 * LOG_DEBUG и все остальные с типом больше LOG_NOTICE
                 *
                 * Обработка транспортов для сообщения идет по порядку определения
                 * транспортов с помощью вызова TransportBind. Т.е. для каждого
                 * сообщения просматривается весь список всех определенных в
                 * конкретном экземпляре logger транспортов и проверяется условие
                 * обработки сообщения каждым транспортом. Обработка сообщения
                 * завершается при достижении конца списка всех определенных
                 * транспортов. Таким образом, порядок определения транспортов не
                 * имеет значения.
                 *
		 *
                 * $TransportName (string) - название транспорта, класс
                 *      транспорта ищется в файле следующего названия:
                 *      'logger_tr_' . $TransportName . '.inc'
                 *
                 * $Parameters - массив с необходимыми для работы транспорта
                 *      параметрами. Формат каждого массива индивидуален для
                 *      каждого транспорта. Обычно там содержится такие данные
                 *      как название и путь к файлам для записи туда сообщений,
                 *      DSN и название таблиц в СУБД и пр.
		 * 
		 * есть выбор: хранить настройки транспорта (шаблоны вывода, используемые поля logger_msg,
		 * точки назначения: названия файла, таблицы и пр.) непосредственно внутри объекта-транспорта,
		 * или хранить внутри logger и передавать транспорту как параметр?
		 * в первом случае даже для однотипного транспорта придется хранить по одному экземпляру для
		 * каждого типа сообщения
		 * во втором случае - сложнее и менее универсальная система вызова транпорта для доставки
		 * сообщения
		 * 
		 * третий вариант. заставить транспорт формировать для себя блок параметров доставки и хранить
		 * в logger эти блоки для каждого типа сообщений, передавая при вызове.
		 */

        if ( is_int($MessageType) ) {
            $MsgType = '<=' . intval($MessageType);
        } elseif( defined($MessageType) ) {
            $MsgType = '<=' . constant($MessageType);
        } else {
            if ( eval('$CheckResult = 3 ' . $MessageType . ';') === FALSE ) {
                $arr = error_get_last();
                trigger_error('Parse error MsgType: ' . $arr['message'] . ". Unable bind transport '$TransportName' to MsgType='$MessageType' ", E_USER_ERROR);
            } else {
                $MsgType = $MessageType;
            }
        }

        $RealTransportName = "logger_tr_$TransportName";

        if ( ! class_exists($RealTransportName) )
            require_once $this->SelfPath . $RealTransportName . '.inc';

        if (
            ! class_exists($RealTransportName)
            or ! in_array('logger_transport', class_implements($RealTransportName) )
        )
            trigger_error("Class not exists or wrong type. Unable bind transport '$TransportName' to MsgType='$MessageType' ", E_USER_ERROR);

        if (
            ! isset($this->TransportMsgBind[$MsgType]) or
            ! is_array($this->TransportMsgBind[$MsgType])
        ) {
            $this->TransportInit($MsgType);
        }
        $BindTransportArr = array();

        try {
            if ( eval('$BindTransportArr[\'Transport\'] = ' . $RealTransportName . '::GetTransport();') === FALSE )
                throw new Exception('Executing error for ' . $RealTransportName . '::GetTransport();');
            $BindTransportArr['Destination'] = $BindTransportArr['Transport']->MakeDestination($this, $Parameters);
        } catch (Exception $E) {
            trigger_error($E->getMessage() . " Unable bind transport '$TransportName' to MsgType='$MessageType' ", E_USER_ERROR);
        }

        if (
            is_string($MessageType)
            and preg_match('/^.*([_0-9a-z]+)$/iU', $MessageType, $mtext)
            and defined($mtext[1])
            and constant($mtext[1]) > 7
        )
            $BindTransportArr['Transport']->CustomMsgType[constant($mtext[1])] = $mtext[1];

        $this->TransportMsgBind[$MsgType][] = $BindTransportArr;

    }

    private function _createMessage() {
		/*
		 * создадим объект тут, чтобы иметь возможность управлять стеком объектов, если понадобится
		 */

//        unset($this->LogMsg);
		/*
		 * надо потестировать и определить что быстрее и экономнее:
		 * 1. уничтожать старый объект и создавать новый
		 * 2. создавать новый, бросив старый
		 * 3. чистить существующий объект, уничтожая его внутренние поля
		 * 4. чистить существующий объект, замещая NULL его поля
		 * по идее, первый будет самый правильный
		 */

        eval('$this->LogMsg = new ' . Logger_MSG_Class . ';');
        return $this->LogMsg;

    }

    private function _discardMessage() {
                /*
                 * уничтожает объект-сообщение
                 */

        $this->LogMsg->Destroy();
        $this->LogMsg = NULL;

    }

    private function _getCurrentMessage() {

        return $this->LogMsg;

    }

    function msg($msg, $type=NULL, $level=Logger_MSG_TraceLevel) {
                /*
                 * короткий вариант вызова метода message
                 * без указания кода сообщения
                 */

        return $this->message($msg, NULL, $type);

    }

    function message($msg, $code=NULL, $type=NULL, $level=Logger_MSG_TraceLevel) {
		/*
		 * создает объект типа logger_msg содержащий параметры сообщения:
		 * текст сообщения $msg, тип $type:
		 * LOG_EMERG - серьезная авария, крах системы, ппц
		 * LOG_ALERT - серьезная ошибка, сбой
		 * LOG_CRIT - ошибка, не позволяющая продолжать работу
		 * LOG_ERR - ошибка
		 * LOG_WARNING - предупреждение
		 * LOG_NOTICE - нотация, сообщение о необходимости произвести корректировку
		 * LOG_INFO - информационное сообщение
		 * LOG_DEBUG - отладочная информация
		 * 
		 * $level указывает кол-во элементов цепочки backtrace, которое
                 * передается сообщению
		 * 
		 */

        if ( $type === NULL ) {
            $type = $this->DefaultMsgType;
        }

        $msg_transports = array();

        foreach ( $this->TransportMsgBind as $msg_type => $transport ) {
            $CheckResult = FALSE;
            if ( @eval('$CheckResult = ' . $type . $msg_type . ';') === FALSE ) {
                $arr = error_get_last();
                trigger_error('Parse error MsgType: ' . $arr['message'] . ". Unable define transport for MsgType='$msg_type' ", E_USER_ERROR);
            }
            if ( $CheckResult ) {
                foreach($transport as $tr_arr)
                    $msg_transports[] = @$tr_arr;
            }
        }

        if ( // если нет транспорта - ничего не делаем
            sizeof($msg_transports) == 0
        )
            return;

        $this->_set_time();

        $MSG = $this->_createMessage();

        $BTrace = debug_backtrace();

        while ( $BTrace[0]['file'] == __FILE__ )
            array_shift($BTrace);

        $level_0 = $BTrace[0];

        foreach ( $BTrace as $key => $val ) {
            if ( --$level < 0 )
                break;
            $MSG->PutField('MTrace', $key, $val);
        }

        if (
            $code === NULL
            and Logger_MSG_AutofillCode
        ) {
            $code = "0_" . $level_0['line'];
        }

        $MSG->Init($this->SystemKeys, $this->UserKeys);

        $MSG->PutField('MFile', 'Filename', $level_0['file']);
        $MSG->PutField('MFile', 'Line', $level_0['line']);

        $MSG->PutField('MText', 'Message', $msg);
        $MSG->PutField('MText', 'Code', $code);
        $MSG->PutField('MText', 'Type', $type);

        $MSG->PutField('MTime', 'Start_time', $this->DTimeStart);
        $MSG->PutField('MTime', 'Last_time', $this->DLastTime);
        $MSG->PutField('MTime', 'Cur_time', $this->DTime);
//        $MSG->PutField('MTime', 'Shift_time', $this->_time_interval());

        foreach ( $msg_transports as $transport )
            $this->_transportMessage($transport);

        $this->_discardMessage();

    }

    private function _transportMessage($transport) {
		/*
		 * отправляет сообщение для транспорта $transport
		 */

        $MSG = $this->_getCurrentMessage();

        $transport['Transport']->TransferMessage($MSG, $this->LoggerID, $transport['Destination']);

    }

}

?>
