<?php
/*
 * Created on 20.09.2008
 *
 * транспорт для логера, отсылает сообщения на eMail
 */
 require_once 'logger_transport.inc';
 require_once 'logger_transport_class.inc';

 class logger_tr_mail extends logger_transport_class implements logger_transport {

 /*
  * этот транспорт отсылает сообщения на eMail, указанный при привязке транспорта
  * к типу сообщений. Для отправки используется встроенная php-function mail
  */

	private static $Transport = null;

 	static function GetTransport() {

		if ( self::$Transport == null ) {
			self::$Transport = new logger_tr_mail;
		}

		return self::$Transport;

 	}

	function TransferMessage($MSG, $LoggerID, $Destination) {

                $mail = '';
                foreach ( $Destination['SysKeys'] as $key => $val ) {
                    if ( empty($val) )
                        continue;
                    $mail .= "${key}: ${val}\n";
                }
                $mail .= "LoggerID: ${LoggerID}\n";
		$mail .= str_repeat('-', 10) . "\n";
		$mail .= "Logger{$Destination['UserKey']} message:\n";
                $mail .= sprintf(
                    "%s at %0.4f+%0.4f: [%s] %s in %s:%d\n",
                    $this->GetTextMsgType($MSG->MText['Type']),
                    $MSG->MTime['Start_time'],
                    $MSG->MTime['Cur_time'] - $MSG->MTime['Start_time'],
                    $MSG->MText['Code'],
                    $MSG->MText['Message'],
                    $MSG->MFile['Filename'],
                    $MSG->MFile['Line']
                );
                foreach ( $MSG->MTrace as $level => $MT ) {
                    $mail .= "backtrace $level:\n";
                    $mail .= $MSG->toStringTrace($MT);
                }

                mail(
                        $Destination['eMail'],
                        'Logger ' . $this->GetTextMsgType($MSG->MText['Type']) . ' message from ' . $Destination['SysKeys']['ServerName'],
                        $mail
                );

//                echo '<hr><pre>';
//                echo $MSG->toString();

	}

	function MakeDestination($Logger, $param) {
		/*
                 * $Logger - указатель на текущий экземпляр logger'а
		 * $param - массив параметров для создания Destination
		 * 
                 * формат массива $param:
		 * eMail - адрес получателя сообщений
		 *
		 */

		$ret = array();

                $ret['eMail'] = $param['eMail'];
                $ret['SysKeys'] = $Logger->SystemKeys;
                $ret['UserKey'] = $this->toStringUserKeys($Logger->UserKeys);
                if ( ! empty($ret['UserKey']) )
                    $ret['UserKey'] = "[${ret['UserKey']}]";

		return $ret;

	}


 }
 ?>
