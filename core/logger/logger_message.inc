<?php
/*
 * Created on 20.09.2008
 *
 * базовый интерфейс для сообщений logger'а
 * 
 */

interface logger_message {
    /*
     * интерфейс для создания сообщений
     */

        function Init($SystemKeys, $UserKeys);
            /*
             * этот метод вызывает logger после создания экземпляра сообщения
             * и передает основные ключи: SystemKeys и UserKeys
             */

         function Destroy();
             /*
              * вызывается при уничтожении сообщения
              */

	function GetField($group, $field);
            /*
             * возвращает информационную ячейку сообщения
             */

	function PutField($group, $field, $value);
            /*
             * сохраняет информационную ячейку сообщения
             */

	function GetAll();
            /*
             * возвращает все информационные ячейки сообщения в виде массива.
             * Вид массива к примеру:
             * array(
             *    FieldName = FieldValue;
             * )
             */

	function toString();
            /*
             * возвращает текстовый блок со всеми ячейками
             */

}

?>