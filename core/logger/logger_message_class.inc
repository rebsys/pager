<?php
/*
 * Created on 20.09.2008
 *
 * базовый класс для сообщений logger'а
 * с общими константами и методами для сообщений
 * 
 */

class logger_message_class {
    /*
     * базовый класс для создания расширений сообщений
     */

        function GetField($group, $field) {

		if (
                    ! isset($this->$group)
                    or ! is_array($this->$group)
                )
                    return NULL;

                $obj = & $this->$group;

                if ( ! isset($obj[$field]) )
                    return NULL;

		return $obj[$field];

	}

	function PutField($group, $field, $value) {

		if (
                    ! isset($this->$group)
                    or ! is_array($this->$group)
                )
                    $this->$group = array();

		$obj = & $this->$group;
		$obj[$field] = $value;

	}

    	function __toString() {

		return $this->toString();

	}

        function toStringTrace($TraceItem) {
                /*
                 * возвращает элемент backtrace в текстовом виде с
                 * форматированием стиля text/plain
                 */

                $ret = '';

                if (is_object($TraceItem['object']))
                    $TraceItem['object'] = 'object_of_class_' . get_class($TraceItem['object']);
                if ( sizeof($TraceItem['args']) > 0 )
                    $TraceItem['args'] = str_replace("\n", "\n\t", var_export($TraceItem['args'], TRUE));
                else
                    $TraceItem['args'] = '';
                $ret .= "\tfile: ${TraceItem['file']}:${TraceItem['line']}\n";
                $ret .= "\tclass ${TraceItem['class']}: ${TraceItem['object']}${TraceItem['type']}${TraceItem['function']}(${TraceItem['args']})\n";

                return $ret;

        }

	function toString() {

		$ret = '';
		foreach ( $this->GetAll() as $group => $arr ) {

			$ret .= "$group = {\n";
			if ( $group == 'MTrace' )
                            foreach ( $arr as $key => $val ) {
                                $ret .= "$key:\n";
                                $ret .= $this->toStringTrace($val);
                            }
			else
                            foreach ( $arr as $key => $val ) {
                                if ( is_array($val) )
                                    $ret .= "\t$key = " . str_replace("\n", "\n\t", var_export($val, 1)) . "\n";
                                else
                                    $ret .= "\t$key = $val\n";
                            }
			$ret .= "}\n";

		}
		return $ret;

	}

}

?>