<?php
/*
 * Created on 20.09.2008
 *
 * транспорт для логера, пишет в stdout
 */

 require_once 'logger_transport.inc';
 require_once 'logger_transport_class.inc';

 class logger_tr_stdout extends logger_transport_class implements logger_transport {
 
 /*
  * этот транспорт выводит сообщения на stdout, т.е. непосредственно в ответ
  * http-сервера на страницу браузера
  */

	private static $Transport = null;

 	static function GetTransport() {

		if ( self::$Transport == null ) {
			self::$Transport = new logger_tr_stdout;
		}
		
		return self::$Transport;
 
 	}
 
	function TransferMessage($MSG, $LoggerID, $Destination) {

		echo "<hr>Logger{$Destination['UserKey']} message:<br>";
                printf(
                    '%s at %0.4f+%0.4f: [%s] <strong>%s</strong> in %s:%d<br>',
                    $this->GetTextMsgType($MSG->MText['Type']),
                    $MSG->MTime['Start_time'],
                    $MSG->MTime['Cur_time'] - $MSG->MTime['Start_time'],
                    $MSG->MText['Code'],
                    $MSG->MText['Message'],
                    $MSG->MFile['Filename'],
                    $MSG->MFile['Line']
                );
                foreach ( $MSG->MTrace as $level => $MT ) {
                    echo "backtrace $level:<br>";
                    echo str_replace("\t", '&nbsp; &nbsp;', nl2br($MSG->toStringTrace($MT)));
                }
//                echo '<hr><pre>';
//                echo $MSG->toString();

	}

	function MakeDestination($Logger, $param) {
		/*
                 * $Logger - указатель на текущий экземпляр logger'а
		 * $param - массив параметров для создания Destination
		 * 
		 * 
		 */

		$ret = array();

                $ret['UserKey'] = $this->toStringUserKeys($Logger->UserKeys);
                if ( ! empty($ret['UserKey']) )
                    $ret['UserKey'] = "[${ret['UserKey']}]";

		return $ret;

	}
 
 
 }
?>
