<?php
/*
 * Created on 14.09.2008
 *
 * auto configurator
 */
 
if ( ! defined('UsersConfigurationFile') )
    define('UsersConfigurationFile', '../.user_config.inc');

if ( ! defined('MainSystemDefines') ) {

	$SystemDefaults_parameters = array(

        /*
         * Константы могут определяться в результате различных манипуляций
         * с данными. Для этого в качестве переметра определения константы
         * используется массив, первый элемент которого указывает способ
         * определения константы, а второй - данные для него.
         * Можно использовать следующие определения:
         * VarDef1 = array('M', "a_function($AnyVar + $OtherVar)"),
         * Константа VarDef1 будет определена как результат выполнения кода,
         * стоящего вторым элементом массива.
         * VarDef2 = array('C', 'OtherConstant'),
         * Константа VarDef2 будет определена так же, как и константа, чье имя
         * указано во втором элементе массива. Обычно используется для дублиру-
         * ющих констант, которые не определяются в основном файле конфигурации.
         */

        'ERROR_MAIL_SENDER_ADDRESS'                             =>	'mic@rebsys.ru',
        'ERROR_MAIL_SENDER'					=>	'Mr. Postman',
        'ERROR_LOG'						=>	'error.log',
        'ERROR_LOG_CHECK'					=>	FALSE,
        'ERROR_PATH'						=>	'MAIL',
        'ERROR_MAIL'						=>	'mic@rebsys.ru',
        'ERROR_MAIL_CHECK'					=>	FALSE,
        'ERROR_LEVEL'						=>	'9',

        'PHP_INI_ALL'                       =>  '7',
        'SQL_Debug'                         =>  FALSE,
        'Logger_MSG_TraceLevel'             =>  0,
        'MediaFiles'			    =>	'user_files/media',
        'FPath'                             =>  array('M', "constant('FRoot') . constant('MediaFiles') . '/'"),
        'WPath'                             =>  array('M', "constant('WRoot') . constant('MediaFiles')  .'/'"),
        'SESSION_AuthorizationScript'       =>  array('M', "constant('WRoot') . 'auth_failed.php'"),


        'TimeZone'			    =>  'Europe/Moscow',// текущая временная зона
        'StoreEngineClass'                  =>  'store_core',   // стандартный движок хранилища
        'Pager_BaseClass'                   =>  'pager_class',  // стандартный класс для Pager
        'ModMan_BaseClass'                  =>  'modman_class', // стандартный класс для ModMan
        'StoreLocation'                     =>  'engine',       // каталог для файлов движка хранилища
        'PM_PagerBadRequest'                =>  '/PageBadRequest', // страница для ошибки 400
        'PM_PagerUnauthorized'              =>  '/PageUnauthorized', // страница для ошибки 401
        'PM_PagerForbidden'                 =>  '/PageForbidden', // страница для ошибки 403
        'PM_PagerNotFound'                  =>  '/PageNotFound',// страница для ошибки 404
        'PM_PagerError'                     =>  '/PageError',   // страница для ошибки 500
        'FileAutoContent_Prefix'	          =>  '',		// общий префикс всех файлов контента для file_auto
        'FileAutoContent_Ext'		            =>  '.html',	// дефолтное расширение файлов контента для file_auto
//        'FileAutoCSS_Prefix'	              =>  'css/',		// общий префикс всех файлов стилей для file_auto
//        'FileAutoCSS_Ext'		                =>  '.min.css',	// дефолтное расширение файлов стилей для file_auto
        'FileAutoXML_Ext'		                =>  '.xml',		// дефолтное расширение файлов XML для file_auto
        'FileAutoXSL_Ext'		                =>  '.xsl',		// дефолтное расширение файлов XSL для file_auto
        'PageDefaultHeader'                 =>  '/top.html',	// штатный заголовок страницы, верхняя часть статического контента
        'PageDefaultFooter'                 =>  '/bottom.html',	// штатный футер страницы, нижняя часть статического контента
        'PageDefaultDTD'                    =>  '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
                                                                // Default DTD для тех страниц, у которых он не задан в Properties
        'PM_PageIndex'                      =>  'ROOT',         // алиас для корневой страницы, используется при запросе в корень директории
        'PM_PageCheckIndex'                 =>  TRUE,           // при отсутствии страницы, проверять наличие одноименной директории
                                                                // и делать редирект на нее при наличии

     // настройки безопасности
        'PM_SEC_CheckValid_Path'            =>  '|^/[A-z0-9~_/.-]*$|', // правильный REGEXP, применяемый к запрошенному
                                                                // идентификатору страницу, т.е. к урлу страницы без GET. Если запрошенный URL
                                                                // не соответствует данному REGEXP, выдается соответствующее сообщение
                                                                // и выполнение запроса останавливается. Если REGEXP определен пустой
                                                                // строкой, проверка не выполняется.

    // общие настройки store_core для всех типов хранилищ 
        'StoreEngine_CurrentDir_is_Dot'	    =>  TRUE,		// если TRUE, то текущий каталог обозначается в
                                                                // пути объекта как unix-путях: "./"
                                                                // если FALSE, то любой путь, который начинается не от
                                                                // корня "/", считается относительным текущего каталога

        'StoreEngine_ModuleRelPath'	    =>  '/',		// если путь к модулю начинается на от корня "/", то в начало
                                                                // пути добавляется префикс StoreEngine_ModuleRelPath
        'StoreEngine_RevisionDelimiter'	    =>  '@@@',		// разделитель для указание версии после имени объекта
        'StoreEngine_RevisionHEADasObject'  => TRUE,		// задает обязательную проверку на наличие версии HEAD для
                                                                // объекта, указанного без версии.
        'StoreEngine_RevisionHEADasModule'  => FALSE,		// при установленной StoreEngine_RevisionHEADasObject задает еще обязательную
                                                                // проверку на наличие версии HEAD для модуля, указанного без версии.
        'StoreEngine_FileAccessXML'	    => array('M', "constant('FRoot') . '.estore_access.xml'"),	// задает местонахождение файла с описанием
                                                                // прав доступа к различным объектам для авторизованных пользователей

    /***********************************************************/
    /***** настройки различных типов хранилищ с драйверами *****/
    /***********************************************************/
    // формат задания имен: StorageType_<driver> => driver_name
    // при нахождении ссылки типа "driver:" в пути к объекту, StoreEngine будет пытаться подключить файл с именем
    // из константы StoreType_<driver> с расширением ".inc" и проинициализировать находящийся там драйвер хранилища
    // с именем класса, совпадающим с именем файла без расширения. Класс драйвера должен применять интерфейс store_interface.
        'StorageType_file'		    =>  'store_file',	// файловое хранилище
        'StorageType_db'		    =>  'store_db',	// хранилище в БД типа MySQL
#	'StorageType_s3'		    =>  'store_s3',	// хранилище в AWS S3
        'StorageType_ddb'		    =>  'store_aws_ddb',// AWS DynamoDB

    // если драйвер объекта явно не указан, используется хранилище по умолчанию для данного типа объектов
    // настройка типов хранилищ по умолчанию для разных типов объектов
        'PageStorageDefault'		    =>  'file',		// для объектов типа PageObject
        'ModuleStorageDefault'		    =>  'file',		// ModuleObject
        'FileStorageDefault'		    =>  'file',		// FileObject

     // настройки для движка store_file
        'PagerFileSource'                   =>  'user_files/pages', // каталог для описателей страниц
        'OtherFiles'                        =>  'user_files/files', // каталог для прочих файлов
        'ModuleFileSource'                  =>  'core/modules', // каталог для файлов-классов модулей
        'UsersModuleFileSource'             =>  'user_files/modules', // каталог для файлов с юзерскими модулями,
                                                                // они лежат в виртуальном каталоге users
                                                                // пример: <module name='users/my_news' />
                                                                // в этом случае префикс pager_module_ в названии
                                                                // модуля не требуется.
                                                                // файл my_news.inc: class my_news;
            // end of config array
	);

        // подключим юзерский конфиг, если есть
        if ( file_exists(UsersConfigurationFile) )
            include_once UsersConfigurationFile;

        // определим корневые директории
        if ( ! defined('LFRoot') ) {
            if ( __FILE__ > $_SERVER['DOCUMENT_ROOT'] )
                define('LFRoot', str_replace($_SERVER['DOCUMENT_ROOT'], DIRECTORY_SEPARATOR, str_replace('core' . DIRECTORY_SEPARATOR .'config.inc', DIRECTORY_SEPARATOR, __FILE__)));
            else
                define('LFRoot', '');
        }
        if ( ! defined('LWRoot') )
            define('LWRoot', LFRoot);
        define('FRoot', preg_replace('|' . DIRECTORY_SEPARATOR .'{2,}|', DIRECTORY_SEPARATOR, $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . LFRoot . DIRECTORY_SEPARATOR));
        define('WRoot', preg_replace('|/{2,}|', '/',  '/' . LWRoot . '/'));

        // добавим корневой и текущий каталоги в include_path
        $ipath_arr = explode(PATH_SEPARATOR, ini_get('include_path'));
        if ( ! in_array(dirname(__FILE__), $ipath_arr))
            array_push($ipath_arr, dirname(__FILE__));
        if ( ! in_array(FRoot, $ipath_arr) )
            array_push($ipath_arr, FRoot);
        if ( ini_set('include_path', implode(PATH_SEPARATOR, $ipath_arr)) === FALSE ) {
            trigger_error('Unable add my path to include_path', E_USER_ERROR);
            die('Unable add my path to include_path');
        }

	foreach ( $SystemDefaults_parameters as $DefVar => $DefVal) {
            if ( ! defined($DefVar) ) {
                if ( is_array($DefVal) )
                    switch($DefVal[0]) {
                        case 'M' :
                            eval("define('$DefVar', $DefVal[1]);");
                            break;
                        case 'C' :
                            define($DefVar, constant($DefVal[1]));
                            break;
                        default :
                            define($DefVar, $DefVal[1]);
                    }
                else
                    define($DefVar, $DefVal);
            }
	}

	unset($SystemDefaults_parameters);
	define( 'MainSystemDefines', TRUE );

}

?>
