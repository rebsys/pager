<?php
/**
 * интерфейс для modman_class
 */
interface modman_interface {

    /**
     * метод подключает нужный StoreEngine и создает его экземпляр
     */
    function Init();

    /**
     * извлекаются настройки страницы и создается деревья свойств и модулей
     */
    function PageSetup($pager);

    /**
     * запускаются последовательно модули страницы
     */
    function PageRun($pager);
}

?>
