<?php
/**
 * класс modman_class для реализации объекта ModMan
 *
 * включает в себя свойства и методы для работы со стеком модулей, получение
 * самих модулей из хранилища, отслеживает зависимости, очередность исполнения,
 * параметры вызова и пр.
 */

// определение типов объектов
define('PageObject', 1);
define('ModuleObject', 2);
define('FileObject', 3);
// определение стандартных префиксов для path к разным типам объектов, обязаны заканчиваться на '/'
define('PagePrefix', 'pager/');
define('ModulePrefix', 'module/');
define('FilePrefix', 'file/');

include_once 'mm_object.inc';

class modman_class implements modman_interface {

    public  $StoreEngine = NULL;
    protected $Log;
    protected $PaC;
    public  $ModCount;
    protected $Modules;

    function  __construct() {
        $this->Log = logger::GetLogger();
        $this->PaC = PagerConfig::GetPagerConfig();

    }

    function Init() {
        /*
         * TODO: сделать еще один движок хранения контента, назвать его
         * store_helper, подключать при необходимости.
         * К нему иметь отдельные опции конфига, задающие тип используемого хранилища
         * от типа контента, маски названия источника контента и любые другие
         * варианты. Все инициированные хранилища подключаются с созданием уникальных
         * для хранилища алиасов, которые могут потом использоваться. Таким образом
         * возможно иметь несколько однотипных хранилищ с разными настройками и,
         * как следствие, с разными физическими характеристиками хранилища: разные
         * таблицы и СУДБ, каталоги и пр. Для этого надо разделить настройки хранилищ
         * по алиасам, при использовании хелпера.
         * При вызове strore_helper определяет нужный тип хранилища для этого
         * типа контента и лезет за ним туда, используя штатные движки.
         * Еще надо бы сделать возможность указывать дополнительным атрибутом
         * нужное хранилище через его алиас. Т.е. если модуль поддерживает store_helper,
         * он проверяет аттрибут sh_name в нодах типа file_src и использует его
         * значение для получения названия хранилища. Если не поддерживает - 
         * просто игнорирует этот аттрибут.
         * 
         * Вот как-то так. красиво! :)
         *
         * УСТАРЕЛО, но смысл тот же
         * предполагается сделать единый интерфейс получения контента, который
         * будет в зависимости от настроек выдавать контент из нужного хранилища
         * с помощью соответствующего движка. Планируется сделать несколько вариантов
         * указания на тип хранилища, по увеличению приоритета:
         * 1. базовая настройка по типу контенту: file, page, module
         * 2. настройка по типу контента плюс вилдкард/регексп
         * 3. явное указание при вызове на нужное хранилище
         * Единый движок будет подключать помере необходимости нужные движки хранилищ
         * и использовать их. В ModMan'е останется только ссылка на этот единый движок,
         * или метод для ее получения - что еще лучше.
         *
         * Еще один момент в тему хэлпера для StoreEngine - статус и права на доступ
         * объекта. Нужен запрос статуса, в котором можно было бы получить права
         * к объекту, доступные версии и таги объекта. Вот и вопрос: надо ли это
         * делать хелпер, или проверку прав доступа к объекту жестко вшить в двигло?
         * Попутно, видимо, не помешает сделать некий hook for StoreEngine, чтобы
         * всякие эдакие штуки делать. Уже много всего на него нависло в итоге, надо
         * как следует подумать...
         *
         *
         */

        if ( ! class_exists('StoreEngine') )
            require_once StoreLocation . DIRECTORY_SEPARATOR . StoreEngineClass . '.inc';

        $this->StoreEngine = new StoreEngine();

    }

    /**
     * Рекурсивный метод, формирующий Properties и Modules станицы $page из
     * заданных на этой же странице шаблонов.
     *
     * @package pager
     * @param DOMDocument $page
     * @return null
     *
     * <p>Все Properties шаблонов добавляются в конец текущих Properties страницы,
     * т.е. после последнего элемента Properties.
     *
     * <p>В случае совпадения элементов в различных Properties шаблонах и самой
     * страницы, эти элементы будут добавляться в конец Properties страницы по
     * мере вычитывая их из шаблонов. Далее в методе PageApplyTemplate описана
     * возможность управлять добавлением совпадающих свойств: выше, ниже, вместо.
     * Различные модули, которые в дальнейшем
     * будут обрабатывать Properties этой страницы, должны будут самостоятельно
     * определять, как использовать свойства с одинаковыми именами: использовать
     * только первое из них, только последнее или еще как-то.
     *
     * <p>Рекомендуется использовать атрибут <b>important</b> со значением отличным
     * от '<i>0</i>' или '<i>no</i>', для того чтобы дать возможность модулям
     * определять приоритеты конфликтующих свойств.
     *
     * <p>Modules шаблона являются базовыми и редко замещаются, хотя любой шаблон
     * или страница имеет возможность убрать любой или все модули из шаблона,
     * добавить произвольный модуль в нужное место, заменить любой модуль на другой.
     *
     * <p>Модули задаются элементами module, в которые включаются необходимые
     * модулю параметры. Формат параметров может быть индивидуален для каждого
     * модуля.<br>
     * Сами таги module могут иметь набор опциональных атрибутов, указывающих
     * на действие (удалить, добавить или заменить) производимое над модулем, и
     * на место применения этого действия, т.е. координаты модуля. Место применения
     * указывается с помощью стандатных способов
     * <a href="http://www.w3schools.com/XPath/xpath_syntax.asp">XPath</a>,
     * задаваемых относительно корневого элемента Modules. Обычно для указания
     * модуля используется атрибут name, задающий имя используемого модуля, но
     * если используются несколько одинаковых модулей в разных местах, рекомендуется
     * использовать дополнительный идентификаторы на усмотрение пользователя.
     * Кроме того, ряд атрибутов может быть использован для управления работой
     * самого модуля. В любом случае, не должны быть использованы ни один из ниже
     * перечисленых атрибутов, имеющих префикс mm_, и используемых для различных
     * манипуляций с модулями в элементе Modules самой страницы.
     * 
     * <p>Для манипуляции модулями поддерживает следующие атрибуты:<br>
     * <ul>
     *
     * <li><b>mm_query</b> - задает параметр поиска места применения module с помощью
     * <a href="http://www.w3schools.com/XPath/xpath_syntax.asp">XPath</a><br>
     * Пример: &lt;module mm_query="module[@name='debug_printXML_Page']"><br>
     *
     * <li><b>mm_action</b> - задает действие по отношению к модулю, заданному с помощью
     * атрибута mm_qury. Может принимать параметры:
     * <ul>
     * <li><i>add</i> - добавить модуль
     * <li><i>del</i> - удалить модуль
     * </ul>
     *
     * <li><b>mm_location</b> - задает позицию применения, относительно модуля, заданного
     * с помощью атрибута mm_qury. Может принимать параметры:
     * <ul>
     * <li><i>above</i> - перед модулем
     * <li><i>below</i> - после модуля
     * <li><i>self</i> - сам модуль(-и), указанный с помощью атрибута <b>query</b>
     * </ul>
     *
     * <li><b>mm_query_fault</b> - задает действие при неудачном поиске модуля, заданного
     * с помощью атрибута mm_qury. Может принимать параметры:
     * <ul>
     * <li><i>error</i> - сгенерировать ошибку и пропустить этот модуль
     * <li><i>begin</i> - добавить/убрать первый модуль
     * <li><i>end</i> - добавить/убрать последний модуль
     * <li><i>default</i> - выполнить действия как при незаданном query
     * </ul>
     *</ul>
     * 
     * <p>
     * Все атрибуты mm_* являются опциональными, но их значение по умолчанию
     * определяется по разному в каждом конкретном случае. Эта схема призвана
     * показать эти значения в каждом конкретном случае:<br>
     * <ul>
     * <li><b>mm_query_fault</b> - всегда имеет значение <i>default</i>, если не
     * задано<br>
     * <li><b>mm_action</b> - всегда имеет значение <i>add</i>, если не
     * задано<br>
     * Для атрибута <b>mm_action</b>="<i>add</i>" остальные атрибуты принимают
     * следующие значения по умолчанию:<br>
     * <ul>
     * <li><b>mm_location</b> - имеет значение <i>self</i>, если атрибут
     * <b>mm_query</b> задан и не пустой<br>
     * <li><b>mm_location</b> - имеет значение <i>below</i>, если атрибут
     * <b>mm_query</b> не указан<br>
     * <li><b>mm_query</b> - имеет значение <i>module[position()=1]</i> для указания
     * первого модуля в списке, если атрибут <b>mm_location</b> задан значением
     * <i>above</i><br>
     * <li><b>mm_query</b> - имеет значение <i>module[last()]</i> для указания
     * последнего модуля в списке, если атрибут <b>mm_location</b> задан значением
     * <i>below</i> или не указан<br>
     * <li><b>mm_location</b> заданный со значением <i>self</i> обязательно требует
     * не пустое значение атрибута <b>mm_query</b>. В противном случае генерируется
     * сообщение об ошибке.<br>
     * </ul>
     * Для атрибута <b>mm_action</b>="<i>del</i>" остальные атрибуты принимают
     * следующие значения по умолчанию:<br>
     * <ul>
     * <li><b>mm_location</b> - всегда имеет значение <i>self</i>, если пустой
     * или не задан<br>
     * <li><b>mm_query</b> - имеет значение <i>module[position()=1]</i> для указания
     * первого модуля в списке, если атрибут <b>mm_location</b> задан значением
     * <i>above</i><br>
     * <li><b>mm_query</b> - имеет значение <i>module[last()]</i> для указания
     * последнего модуля в списке, если атрибут <b>mm_location</b> задан значением
     * <i>below</i><br>
     * <li><b>mm_query</b> - имеет значение <i>module</i> для указания всех
     * модулей в списке, если атрибут <b>mm_location</b> задан значением
     * <i>self</i> или не указан<br>
     * </ul>
     * </ul>
     *</ul>
     *
     */
    function PageApplyTemplate(DOMDocument &$page, $template = null, $page_src = null) {
        /*
         * Свойства строятся так: берем страницу, применяем ее свойства, ищем
         * шаблоны и уходим на их обработку в рекурсию. Получаем свойства сверху
         * вниз, по мере подключения шаблонов.
         * Модули берутся наоборот: берем страницу, ищем шаблон и уходим на его
         * обработку в рекурсию. На выходе применяем текущие модули. Получаем
         * дерево модулей соответствующее дереву шаблонов. Самый нижний шаблон
         * дает основные модули, все последующие вносят свои коррективы.
         *
         * Для свойств есть возможность указать объединение с одноименными элементами,
         * задается опцией InheritMode с вариантами значений: replace, above, below.
         * При использование этой опции, свойство из присоединяемого шаблона будет
         * полностью замещать одноименное свойство из текущего состояния страницы,
         * или будет объединено с самым первым или последним из них.
         *
         * TODO: Для модулей нужно при подключении проверять наличие xml-описания модуля,
         * где могут быть указаны зависимости, параметры и прочее. А для этого
         * нужно добавить еще один тип хранилища. Физически планирую размещать
         * описание модулей там же где и сами модули, но это будет опционально
         * в рамках настроек движков хранилищ.
         *
         */

        static $LOG, $TemplateNamesArray, $XPage_Properties, $XPage_Modules, $xp;

        if ( $template == null ) {
            // обработка самой страницы

            $TemplateNamesArray = array();
            $template = $page_src = 'NULL';
            $Xtemplate = clone $page;

            //очистим все нужные фолдеры
            $xp = new DOMXPath($page);
            foreach ( $xp->query('Properties') as $prop_node )
                $page->documentElement->removeChild($prop_node);
            $XPage_Properties = $page->documentElement->appendChild($page->createElement('Properties'));
            foreach ( $xp->query('Modules') as $mod_node )
                $page->documentElement->removeChild($mod_node);
            $XPage_Modules = $page->documentElement->appendChild($page->createElement('Modules'));

        } elseif ( ! in_array($template, $TemplateNamesArray) ) {
            // обработка шаблонов страницы

            $this->Log->msg("try add $template to queue for processing from page '$page_src'", LOG_DEBUG3);
            try {
                $Xtemplate = $this->StoreEngine->GetObject(PagePrefix . $template);
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg('template "' . $template . '" for page ' . $page_src . '" not found and will not be use', LOG_WARNING);
                return;
            }
            $TemplateNamesArray[] = $template;

        } else {

            return;

        }

        $xt = new DOMXPath($Xtemplate);
        foreach ( $xt->query('Properties/*') as $propt ) {
        	if ( $propt->attributes->getNamedItem('InheritMode') !== NULL ) {
		        $match_nodes = $xp->query($propt->nodeName, $XPage_Properties);
	        	switch ($propt->attributes->getNamedItem('InheritMode')->nodeValue) {
    	    		case 'replace': // заменить все существующие
	    	    		foreach ( $match_nodes as $match_node ) {
    	    	        		$XPage_Properties->removeChild($match_node);
    	    	        	}
				$XPage_Properties->appendChild($page->importNode($propt, TRUE));
        			break;
    	    		case 'above': // вложить в первую найденную
    	    			// если ноды не существует - создать ее !!!
    	    			if ( $match_nodes->length == 0 )
    	    				$target_node = $XPage_Properties->appendChild($page->createElement($propt->nodeName));
    	    			else
	    	    			$target_node = $match_nodes->item(0);
	    	    		foreach ( $propt->childNodes as $added_node ) {
    	    	        		// вложить в самое начало списка потомков
    	    	        		$target_node->insertBefore($page->importNode($added_node, TRUE), $target_node->firstChild);
				}
        				break;
    	    		case 'below': // вложить в последнюю найденную
    	    			// если ноды не существует - создать ее !!!
    	    			if ( $match_nodes->length == 0 )
    	    				$target_node = $XPage_Properties->appendChild($page->createElement($propt->nodeName));
    	    			else
	    	    			$target_node = $match_nodes->item( $match_nodes->length - 1 );
	    	    		foreach ( $propt->childNodes as $added_node ) {
    	    	        		$target_node->appendChild($page->importNode($added_node, TRUE));
				}
        				break;
			default:
				throw new Exception('Unknown value of "InheritMode" in template ' . $template);
        		}
        	} else {
			    $XPage_Properties->appendChild($page->importNode($propt, TRUE));
			}
		}

        foreach ( $xt->query('UseTemplatePage') as $TemplatePage )
            $this->PageApplyTemplate($page, $TemplatePage->nodeValue, $template);

        foreach ( $xt->query('Modules/module') as $module ) {

            $MMO = new mm_object(array('action', 'location', 'query', 'query_fault'));
            $MMO->InitVar('action', $module->getAttribute('mm_action'));
            $MMO->InitVar('location', $module->getAttribute('mm_location'));
            $MMO->InitVar('query', $module->getAttribute('mm_query'));
            $MMO->InitVar('query_fault', $module->getAttribute('mm_query_fault'));
            /*
             * TODO: сделать еще опциональный параметр full_query, который без всяких там
             * корректировок искался бы по всех XML странице.
             * Нужно будет проверять его наличие, и если его нет, формировать его из обычного
             * query, а если он задан - использовать именно его. Но это на будущее, для
             * большей гибкости.
             */

            $this->Log->msg('Parse module with option: mm_action='. $MMO->GetVar('action') . ', mm_location=' . $MMO->GetVar('location') . ', mm_query=' . $MMO->GetVar('query'), LOG_DEBUG1);

            // сбросим поиск для первого модуля
            if (
                $MMO->CheckEmpty($module->getAttribute('mm_query')) and
                $XPage_Modules->childNodes->length == 0
            )
                $MMO->InitVar('query', '.');

            // найдем заданный через query модули
            $query_nodelist = $xp->query('Modules/' . $MMO->GetVar('query'));

            if ( $query_nodelist->length == 0 ) {
                // не найден заданный query модуль

                switch ($MMO->GetVar('query_fault')) {
                    case 'begin':
                        $MMO->InitVar('query', 'module[position()=0]');
                        break;

                    case 'end':
                        $MMO->InitVar('query', 'module[last()]');
                        break;

                    case 'default':
                        $MMO->InitVar('query', '');
                        break;

                    case 'error':
                        throw new Exception('Unable find a module by mm_query: ' . $MMO->GetVar('query'));

                    default:
                        throw new Exception('Unknown value of mm_query_fault: ' . $MMO->GetVar('query_fault'));
                }

                $query_nodelist = $xp->query('Modules/' . $MMO->GetVar('query'));

                if ( $query_nodelist->length == 0 )
                    throw new Exception('Unable find a module by mm_query after use mm_query_fault. mm_query: ' . $MMO->GetVar('query'));

            }

            if ( $MMO->GetVar('action') == 'del' ) {
                // обработаем запросы на удаление

                foreach ( $query_nodelist as $rm_module ) {

                    if ( $MMO->GetVar('location') == 'self' )
                            $module_to_remove = $rm_module;

                    elseif ( $MMO->GetVar('location') == 'above' )
                            $module_to_remove = $rm_module->previousSibling;
                            
                    elseif ( $MMO->GetVar('location') == 'below' )
                            $module_to_remove = $rm_module->nextSibling;

                    $this->Log->msg('module name=' . $module_to_remove->getAttribute('name') . ' will be remove' , LOG_DEBUG);
                    $XPage_Modules->removeChild($module_to_remove);

                }

            } else {
                // обработаем запросы на добавление/замену

                $add_module = $query_nodelist->item(0);
                $this->Log->msg('module name=' . $module->getAttribute('name') . ' will be add to ' . $MMO->GetVar('location') . ' of ' . $add_module->getAttribute('name'), LOG_DEBUG1);

                if (
                    $MMO->GetVar('location') == 'self'
                ) {
                        $XPage_Modules->replaceChild($page->importNode($module, TRUE), $add_module);

                } elseif (
                    $MMO->GetVar('location') == 'above'
                ) {
                        $XPage_Modules->insertBefore($page->importNode($module, TRUE), $add_module);

                } elseif (
                    $MMO->GetVar('location') == 'below' and
                    $add_module->nextSibling == NULL
                ) {
                        $XPage_Modules->insertBefore($page->importNode($module, TRUE));

                } elseif (
                    $MMO->GetVar('location') == 'below' and
                    $add_module->nextSibling != NULL
                ) {
                        $XPage_Modules->insertBefore($page->importNode($module, TRUE), $add_module->nextSibling);

                }

            }

//echo '--- page --- <pre>', htmlspecialchars($page->saveXML()), '</pre>------------<br>';
        /*
         * action - add
         * action=add, location - above
         * action=add, location=below, query - в самое начало стиска
         * action=add, location=above, query - в самый конец стиска
         * action=add, location=self, query=address - заменяет текущий модуль или ошибка
         *      если модуль не задан
         *
         * action=remove, location - self
         * action=remove, location=below, query - самый первый
         * action=remove, location=above, query - самый последний
         * action=remove, location=self, query - все модули
         * action=remove, location=any, query=address - удаляет указанный модуль
         */

        }

//        echo '--- page --- <pre>', htmlspecialchars($page->saveXML()), '</pre>------------<br>';

    }

    function PageCheckAccess($page) {
        /*
         * проверяем настройки доступности страницы для показа
         */

        $xp = new DOMXPath($page);

        if ( $xp->query("Access/Deny")->length > 0 ) {

            return  FALSE;

        }

        return TRUE;

    }

    /**
     * Пытается загрузить страницу, соответствующую коду ошибки. Если не
     * получается, выполняет базовый вывод ошибки и останавливает дальнейшее
     * выполненение запроса.
     *
     * @param int $code
     * @param array $ExtParams
     * @return DOMDocument
     *
     * Дополнительно передаются параметры в массиве $ExtParams:
     * PageID - идентификатор страницы, обработка которой вызвала ошибку
     * Location - адрес редиректа для ошибки 301
     *
     */
    protected function _PageError($code, array $ExtParams) {

        $PageXML = new DOMDocument;

        switch ($code) {

            case 301:   // document moved permanently
                header($_SERVER["SERVER_PROTOCOL"] . ' 301 Moved Permanently');
                header('Location: ' . $ExtParams['Location']);
                printf (
                    "<html><body><p>The document has moved <a href=\"%s\">%s</a></p></body></html>\n",
                    $ExtParams['Location'],
                    $ExtParams['Location']
                );
                exit;

            case 307:   // document temporary moved
                header($_SERVER["SERVER_PROTOCOL"] . ' 307 Temporary Redirect');
                header('Location: ' . $ExtParams['Location']);
                printf (
                    "<html><body><p>The document has temporary redirected <a href=\"%s\">%s</a></p></body></html>\n",
                    $ExtParams['Location'],
                    $ExtParams['Location']
                );
                exit;

            case 400:   // bad request

                try {
                    $PageXML = $this->StoreEngine->GetObject(PagePrefix . PM_PagerBadRequest);
                } catch (Exception $ee) {
                    $this->Log->message('StoreEngine error: ' . $ee->getMessage(), $ee->getCode(), LOG_ERR);
                    $this->Log->message('Bad request: "' . $ExtParams['PageID'], $code, LOG_NOTICE);
                    header($_SERVER["SERVER_PROTOCOL"] . ' 400 Bad Request');
                    echo "Bad request\n";
                    exit;
                }
                break;

            case 401:   // page is Unauthorized

                try {
                    $PageXML = $this->StoreEngine->GetObject(PagePrefix . PM_PagerUnauthorized);
                } catch (Exception $ee) {
                    $this->Log->message('StoreEngine error: ' . $ee->getMessage(), $ee->getCode(), LOG_ERR);
                    $this->Log->message('Page is Unauthorized: "' . $ExtParams['PageID'], $code, LOG_NOTICE);
                    header($_SERVER["SERVER_PROTOCOL"] . ' 401 Unauthorized');
                    echo "Unauthorized\n";
                    exit;
                }
                break;

            case 404:   // page not found

                try {
                    $PageXML = $this->StoreEngine->GetObject(PagePrefix . PM_PagerNotFound);
                } catch (Exception $ee) {
                    $this->Log->message('StoreEngine error: ' . $ee->getMessage(), $ee->getCode(), LOG_ERR);
                    $this->Log->message('Page "' . $ExtParams['PageID'] . '" not found', $code, LOG_NOTICE);
                    header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
                    echo "Page not found\n";
                    exit;
                }
                break;

            case 403:   // page forbidden

                try {
                    $PageXML = $this->StoreEngine->GetObject(PagePrefix . PM_PagerForbidden);
                } catch (Exception $ee) {
                    $this->Log->message('StoreEngine error: ' . $ee->getMessage(), $ee->getCode(), LOG_ERR);
                    $this->Log->message('Page "' . $ExtParams['PageID'] . '" is forbidden', $code, LOG_WARNING);
                    header($_SERVER["SERVER_PROTOCOL"] . ' 403 Forbidden');
                    echo "Forbidden\n";
                    exit;
                }
                break;

            default:

                $this->Log->message('Page "' . $ExtParams['PageID'] . '" has unknown error', $code, LOG_CRIT);
                
            case 500:

                try {
                    $PageXML = $this->StoreEngine->GetObject(PagePrefix . PM_PagerError);
                } catch (Exception $ee) {
                    $this->Log->message('StoreEngine error: ' . $ee->getMessage(), $ee->getCode(), LOG_ERR);
                    $this->Log->message('Page "' . $ExtParams['PageID'] . '" has a error', $code, LOG_ERR);
                    header($_SERVER["SERVER_PROTOCOL"] . ' 500 Error');
                    echo "Page has a error\n";
                    exit;
                }
                break;
        }

        $PE_Node = $PageXML->createElement('PageError');
        foreach ( $ExtParams as $var => $val )
            $PE_Node->appendChild($PageXML->createElement($var, htmlentities($val, ENT_QUOTES)));
        $PageXML->documentElement->appendChild($PE_Node);
        return $PageXML;

    }

    /**
     * Проверяет параметр $URL на соответствие шаблону безопасности
     *
     * @param string $URL
     */
    protected function _CheckPath ($Path) {

        if (
          defined('PM_SEC_CheckValid_Path') and
          PM_SEC_CheckValid_Path != ''
      ) {

          if ( ! preg_match(PM_SEC_CheckValid_Path, $Path) )
                  return FALSE;

      }

      return TRUE;

    }

    /**
     * Возвращает XML-описание страницы с идентификатором PageID из хранилища
     * 
     * @param PageID
     * @return DOMDocument
     * 
     * При неудачном поиске страницы, если идентификатор не
     * заканчивается на слэш и установлена PM_PageCheckIndex, пробует получить
     * одноименный странице каталог. Если успешно, генерирует редирект на него.
     * При ошибках вызывает _PageError для генерации сообщения об ошибке.
     */
    protected function _PageLoad($PageID) {

        try {

            // загружаем запрошенную сраницу
            return $this->StoreEngine->GetObject(PagePrefix . $PageID);

        } catch (Exception $e) {

            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            $this->Log->msg('object page has a error ' . $e->getCode(), LOG_DEBUG);
            /*
             * Если запрашиваемый файл существует в файловой системе с этим путем,
             * возможно, его просто забыли убрать из под обработки wrapper.php
             * в файле .htaccess - выдадим совет в эту тему.
             */
            if (
                $e->getCode() == 404 and
                file_exists(FRoot . $PageID)
            )
                $this->Log->msg('remove "' . $PageID . '" from wrapper.php in .htaccess', LOG_HINT);

            // если это не каталог, попробуем получить каталог
            if (
                $e->getCode() == 404 and
                substr($PageID, -1) != '/' and
                PM_PageCheckIndex
            ) {
                $this->Log->msg("check dir for request $PageID", LOG_DEBUG);
                    try {
                        $this->StoreEngine->GetObject(PagePrefix . $PageID . '/');
                        $this->Log->msg("go to redirect from $PageID to $PageID/", LOG_DEBUG);
                        $this->_PageError(301, array('Location' => str_replace('//', '/', WRoot . $PageID . '/')) );
                    } catch(Exception $ee) {
                        $this->Log->message('StoreEngine error: ' . $ee->getMessage(), $ee->getCode(), LOG_ERR);
                    }
            }

            // загружаем страницу для соответствующей ошибки
            $this->Log->msg("load error-page for request $PageID", LOG_DEBUG);
            return $this->_PageError($e->getCode(), array('PageID' => $PageID) );
            
        } // end catch $e

    }

    /**
     * Настраивает текущую страницу
     *
     * @param pager_class $pager
     * @return void
     *
     * Выполняет загрузку из хранилища XML-описания страницы, обработку шаблонов,
     * проверку доступа и пр.
     */
    function PageSetup($pager) {

        if ( $this->_CheckPath($pager->GetPageID()) ) {

            $PageID = $pager->GetPageID();
            if ( isset($GLOBALS['PageAliases']) and is_array($GLOBALS['PageAliases'])  and sizeof($GLOBALS['PageAliases']) > 0 ) {
                foreach ( $GLOBALS['PageAliases'] as $PageAlias ) {
                    if ( preg_match($PageAlias[0], $pager->GetPageID(), $pager->AliasMatchedArray) ) {
                        $PageID = $pager->ReductionPath($PageAlias[1]);
                        break;
                    }
                }
            }
            $this->PaC->SetVar('PageID', $PageID, ReadOnly_Section);
            $PageXML = $this->_PageLoad($PageID);

        } else {

            $this->Log->msg('bad request', LOG_WARNING);
            $PageXML = $this->_PageError(400, array('PageID'=>$pager->GetPageID()));

        }
        $this->Log->msg('object page was loaded', LOG_DEBUG);

        $xp = new DOMXPath($PageXML);

        // проверим есть ли ограничения доступа и доступна ли страница для просмотра
        if ( $xp->query("Access")->length > 0 ) {

            if ( ! $this->PageCheckAccess($PageXML) )
                $PageXML = $this->_PageError(403, array('PageID'=>$pager->GetPageID()));

        }

        // проверим нет ли шаблона и применим его, если он есть
        try {
            $this->PageApplyTemplate($PageXML);
        } catch ( Exception $e) {
            $this->Log->message(
                sprintf('Pages template "%s" has an error: %s',
                    $PageXML->baseURI,
                    $e->getMessage()
                ),
                $e->getCode(),
                LOG_ERR
            );
            $this->Log->message('Trace: <pre>' . $e . '</pre>', $e->getCode(), LOG_DEBUG);
        }

        $pager->PageXML = $PageXML;

        $pager->SrcXML = new DOMDocument('1.0', 'utf-8');
        $pager->OutXSL = new DOMDocument('1.0', 'utf-8');

        $this->ModCount = 0;
        $this->Modules = $xp->query("Modules/module");

    }

    function PageRun($Pager) {

        while ( $this->ModCount < $this->Modules->length ) {

            $ModuleName = $this->Modules->item($this->ModCount)->attributes->getNamedItem('name')->nodeValue;

            $this->Log->msg("get module $ModuleName", LOG_DEBUG1);

            try {

                $module = $this->StoreEngine->GetObject(ModulePrefix . "$ModuleName");

            } catch (Exception $e) {

                    $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                    $this->Log->msg("module '$ModuleName' has a error, skipped", LOG_WARNING);
                    $this->ModCount++;
                    continue;

            }

            if (
                ! method_exists($module, 'Init') or
                ! method_exists($module, 'Run')
            ) {
                $this->Log->msg("module '$ModuleName' does not contain mandatory methods: Init, Run", LOG_ERR);
                $this->Log->msg("module '$ModuleName' has a error, skipped", LOG_WARNING);
                $this->ModCount++;
                continue;
            }

            $this->Log->msg("run module $ModuleName", LOG_DEBUG);

            try {

                $module->Init($this);
                $module->Run($Pager, $this->Modules->item($this->ModCount));

            } catch (Exception $e) {

                $this->Log->message(
                    sprintf("module <u>'%s'</u> has exception: <em>'%s'</em> at %s:%s",
                        $ModuleName,
                        $e->getMessage(),
                        $e->getFile(),
                        $e->getLine()
                    ), $e->getCode(), LOG_ERR);

                switch ($e->getCode()) {
                    
                    default:
                    case 10:
                        $this->Log->msg("module '$ModuleName' has a error, skipped", LOG_WARNING);
                        break;
                    case 20:
                    case 30:
                        // редирект на страницу с 500 ошибкой
                        $this->Log->msg("module '$ModuleName' has a error, skipped", LOG_WARNING);
                        $this->_PageError(500, array('PageID' => $Pager->PageID));
                        break;
                    case 50:
                        // временный редирект на URL из $e->Message
                        $this->_PageError(307, array('Location' => $e->getMessage()));
                        break;
                    case 60:
                        // редирект на страницу с требованием авторизации - 401
                        $this->_PageError($Pager->TransitArray['ExceptionData']['Code'], array('PageID' => $Pager->PageID));
                        break;

                }

            }
            $this->Log->msg("destroy module $ModuleName", LOG_DEBUG1);
            unset($module);
            $this->ModCount++;

        }

    }

}



?>
