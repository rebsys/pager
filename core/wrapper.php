<?php
/* 
 * Основной скрипт
 *
 * 1. формируем объект Page со всеми свойствами
 * 2. подключаем нужный менеджер модулей (взависимости от конфигов), который
 * будет отследивать зависимости модулей и обеспечивать последовательное их
 * выполнение
 *
 * TODO: можно подключать свои реализации Pager and/or ModMan. Для этого классы их
 * реализующие нужно подключать заранее в конфиге или задавать в defines и
 * размещать файлы в переделах include_path. В этом случае названия файлов
 * должны совпадать с названиями классов и иметь расширение '.inc'. Классы
 * должны реализовывать соответствующие интерфейсы pager_interface
 * и modman_interface. Defines:
 * Pager_CustomClass - название класса для объекта Pager.
 * ModMan_CustomClass - название класса для объекта ModMan.
 *
 *
 * Модули.
 * Модули представляют собой законченные классы реализующие интерфейс
 * module_interface и для ограничение пространства имен имеющие название
 * соответствующее шаблону: pager_module_<название модуля>
 * где <название модуля> и есть имя модуля в том виде, в котором оно включается
 * в описание страницы в секцию Modules
 *
*/

/*** defines ************/
define('SQL_Debug', TRUE);
define('SQL_CheckIP_Stop', TRUE);
define('LOG_DEBUG1', LOG_DEBUG + 1);
define('LOG_DEBUG2', LOG_DEBUG + 2);
define('LOG_DEBUG3', LOG_DEBUG + 3);
define('LOG_HINT', 1666);
//define('Logger_MSG_TraceLevel', 3);
/************************/

error_reporting(E_ALL);
ini_set('display_errors','on');

function dd() {
    call_user_func_array('d', func_get_args());
    exit;
}

function d() {
    foreach (func_get_args() as $arg) {
        var_dump($arg);
    }
}

require 'config.inc';
require 'PagerConfig.inc';
require_once 'logger/logger.inc';

if ( defined('TimeZone') )
  date_default_timezone_set(TimeZone);

$Log = logger::GetLogger();
//$Log->TransportBind(LOG_DEBUG, 'stdout');
//$Log->TransportBind(LOG_DEBUG1, 'stdout');
//$Log->TransportBind(LOG_DEBUG2, 'stdout');
//$Log->TransportBind(LOG_DEBUG3, 'stdout');
$Log->TransportBind(LOG_WARNING, 'stdout');
$Log->TransportBind('==LOG_HINT', 'stdout');

$Log->msg('Logger launched', LOG_DEBUG);

require_once 'pager_core.inc';
require_once 'pager_interface.inc';
require_once 'modman_interface.inc';
require_once 'estore_interface.inc';
require_once 'module_interface.inc';
//TODO: вот для некоторых модулей полагаю сделать xml-описание, в котором, в том числе,
// буду указывать требуемые для модуля классы и интерфейсы. и перед подключением модуля,
// нужно будет проверять - подкдючены ли эти средства?
require_once 'module_core.inc';

/*
 * это все надо бы делать один раз при настройке и потом уже ничего не проверять,
 * а подключать сразу нужное
 * TODO: будет скрипт setup.php, который будет проверять все включенные файлы,
 * классы, каталоги и пр. и др. и по результатам проверки ставить отметку в
 * конфиге.
 *
 */

if ( defined('Pager_CustomClass') )
    $PagerClassName = Pager_CustomClass;
else
    $PagerClassName = Pager_BaseClass;

if ( defined('ModMan_CustomClass') )
    $ModmanClassName = ModMan_CustomClass;
else
    $ModmanClassName = ModMan_BaseClass;

if ( ! class_exists($PagerClassName) ) {
    require_once $PagerClassName . '.inc';
}

if ( ! class_exists($ModmanClassName) ) {
    require_once $ModmanClassName . '.inc';
}

if (
    ! class_exists($PagerClassName)
    or ! in_array('pager_interface', class_implements($PagerClassName) )
) {
    $Log->msg("Class " . $PagerClassName . " not exists or wrong type", LOG_CRIT);
    exit;
}

if (
    ! class_exists($ModmanClassName)
    or ! in_array('modman_interface', class_implements($ModmanClassName) )
) {
    $Log->msg("Class " . $ModmanClassName . " not exists or wrong type", LOG_CRIT);
    exit;
}
/***
 * 
 */

$Log->msg('all classes were included', LOG_DEBUG);

eval('$Pager = new ' . $PagerClassName . ';');

$Pager->Init();

/*
 * TODO: удалить это все потом
 *
 * инициируем ModMan: подключаем StoreEngine
 *      StoreEngine служит для получения _всех_ данных. думаю, сделать запрос по пути path
 * конфигурируем Page: MonMan->PageSetup($Page), при этом устанавливаем пропертисы
 *      Page и дерево модулей в виде XML, проверяются зависимости модулей
 * если страница не определяется, подключаем модули проверки статики и модуль выдачи 404 в которых:
 *      если страница не найдена, проверить наличие статического файла на случай неправильного
 *      .htaccess и выдать его если он есть. записать в лог и в хинты изменить .htaccess
 *      если нет и статики - выдаем 404
 *
 * уже пора сделать и проверять зависимости. к примеру, html_header_build and html_header_include
 * будут работать вместе плохо, создавая два заголовка.
 * модуль cache будет сам создавать кэши, или же это будет делать только админ с помощью специального
 * инструмента?
 *
 *   (первые модули: проверять в каталоге cache наличие кэша страницы и выдавать его если он есть
 *    чтобы сбросить кэш - просто удаляем его)
 * выполняем последовательно все модули с помощью ModMan. данные везде получаем исключительно через StoreEngine
 *
 * ****************
 * TODO: надо сделать на базе этой версии усеченный вариант, который по общему шаблону шарашит все страницы
 * т.е. сделать таким образом, чтобы верстальщику нужно было лишь складывать контент страниц в некий
 * специальный каталог и все. в редком случае для некоторых страниц выкладывая полное xml-описание
 * в pages
 *
 * ******************
 * TODO: надо сделать страницу проверки целостности и безопасности текущих настроек. Для этого нужно
 * определять хост, порт и виртуальный хост при обращении к этой странице, чтобы потом самим опрашивать
 * нужные компоненты Pager'а с этими параметрами веб-сервера.
 * ******************
 * TODO: нужно написать список модулей и что они делают. вкратце, без описания, просто чтобы не создавать дубликатов
 * как вариант, можно в ModMan (все равно разбираться с зависимостями) сделать вызов, который для каждого файла из
 * modules будет показывать название и первый встреченный комментарий - это как раз описание
 * потом прицепить это к общему Setup системы
 * TODO: сделать шаблоны страниц, которые могут использоваться другими страницами. свойства страницы, конечно, переопределяют
 * свойства шаблона. но что делать с модулями? надо или вводить нумерацию модулей, или как-то по-другому сделать возможность
 * подключать эксклюзивные модули данной страницы _между_ модулями шаблона
 * TODO: сделать страницу с ошибкой, которую будем подгружат при возникновении каких-либо ошиок при выводе других страниц:
 * This page has got an error and can not be displayed
 */
eval('$ModMan = new ' . $ModmanClassName . ';');

$ModMan->Init();
$ModMan->PageSetup($Pager);

$Log->msg('all objects were created', LOG_DEBUG);

$ModMan->PageRun($Pager);

//echo '<pre>';
//var_dump($Pager);
//echo '</pre>';




?>
