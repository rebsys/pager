CKEDITOR.plugins.add( 'img_block', {
    requires: 'widget',

    icons: 'img_block',

    init: function( editor ) {
	editor.widgets.add( 'img_block', {
	    button: 'Создать блок с картинкой',
	    template:
	       '<div class="img_block">'+
	       '<div class="img_block_img"><img src="/ckeditor/plugins/img_block/icons/info.png"></div>'+
	       '<div class="img_block_text">Текст</div>'+
	       '</div>',
            editables: {
                images: {
                    selector: '.img_block_image'
                },
                content: {
                    selector: '.img_block_text'
                }
            },
            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'img_block' );
            }
        } );
    }
} );
