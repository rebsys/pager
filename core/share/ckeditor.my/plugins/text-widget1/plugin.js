CKEDITOR.plugins.add( 'text-widget1', {
    requires: 'widget',

    icons: 'text-widget1',

    init: function( editor ) {
	editor.widgets.add( 'text-widget1', {
	    button: 'Create a text-widget1 box',
	    template:
	       '<div class="text-widget1">' +
                '<h2 class="text-widget1-title">Title</h2>' +
                '<div class="text-widget1-content"><p>Content...</p></div>' +
                '</div>',
            editables: {
                title: {
                    selector: '.text-widget1-title'
                },
                content: {
                    selector: '.text-widget1-content'
                }
            },
            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'text-widget1' );
            }
        } );
    }
} );
