CKEDITOR.plugins.add( 'simple_block', {
    requires: 'widget',

    icons: 'simple_block',

    init: function( editor ) {
	editor.widgets.add( 'simple_block', {
	    button: 'Создать простой текстовый блок',
	    template:
	       '<div class="simple_block">Текст</div>',
            editables: {
                content: {
                    selector: '.simple_block'
                }
            },
            upcast: function( element ) {
                return element.name == 'div' && element.hasClass( 'simple_block' );
            }
        } );
    }
} );
