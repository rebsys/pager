<?php

/**
 * имя рутовой ноды XML-страниц
 */
define('PageXML_RootNodeName', 'root');

/**
 * префикс для корневых нод PageXML
 */
define('PageXML_RootPrefix', '/' . PageXML_RootNodeName);
/**
 * базовый класс с общими для большинства вариантов Pager методами
 */
class pager_core {

    /**
     * идентификатор запроса, URI
     * @var string
     */
    public $PageID;

    /**
     * массив для sup-patterns, выделенных из строки запроса текущей страницы
     * добавляются в порядке использования в шаблоне, начиная с номера 1.
     * (используется функция preg_match для формирования)
     * т.е. шаблон типа this_uri_(n2)_(p3), создаст массив вида: 1 => 'n2', 2=> 'p3'
     * @var array
     */
    public $AliasMatchedArray = array();

    /**
     * массив для транзитных данных страницы используется для передачи любых
     * транзитных данных от модуля к модулю
     * @var array
     */
    public $TransitArray = array();

    /**
     * XML-описания страницы
     * @var DOMDocument
     */
    public $PageXML;

    /**
     * XML-документ, хранящий различные данные для обработки модулями.
     * @var DOMDocument
     */
    public $SrcXML;

    /**
     * XSL шаблон, используемый для XSLT трансляции
     * @var DOMDocument
     */
    public $OutXSL;

    /**
     * Результирующая text/html строка, содержащая непосредственно HTML код
     * запрашиваемой страницы. Используется как альтернатива XSLT
     * @var string
     */
    public $OutText;

    /**
     * устанавливает идентификатор страницы относительно корня сайта WRoot
     * попутно исправляет огрехи при задании WRoot с или без финального слэша
     *
     * @param string $PagePath
     *
     */
    function SetPageID($PagePath) {

        if (
                defined('WRoot')
                and preg_match('|' . WRoot . '(.*)|i', $PagePath, $path_bit)
        ) {

            $this->PageID = (substr($path_bit[1], 0, 1) == '/' ? '' : '/') . $path_bit[1];

        } else {

            $this->PageID = $PagePath;

        }

    }

    /**
     * получить идентификатор страницы
     *
     * @return string $PagerID
     */
    function GetPageID() {

        return $this->PageID;

    }

    /**
     * Приводит заданный путь $path в соответствие с идентификатором страницы.
     *
     * @param string $path
     * @return string
     *
     * Проверяет $path на лидирующий слэш '/'. Если путь начинается со слэша,
     * считает путь абсолютным и возвращает как есть. В противном случае дополняет
     * $path путем страницы.
     */
    function ReductionPath($path) {

        if ( $path[0] == '/' )
            return $path;
        else
            return dirname($this->GetPageID()) . '/' . $path;

    }

}

?>
