<?php
/*
 * Created on 13.05.2007
 *
 * В этой библиотеке собраны различные готовые HTML конструкции
 *
 */
 
 class HTML_Date extends HTML {
 	/*
 	 * формирует span из трех select для формировании даты
 	 */

	var $sel_year;
	var $sel_month;
	var $sel_day;
 
 	function HTML_Date($prefix, $month_macro='n', $year_from='', $year_to='') {
		/*
		 * конструктор строит нужный span-HTML
		 */
		 
		 if ($year_from == '')
		 	$year_from = date('Y', time()) - 10;
		 if ($year_to == '')
		 	$year_to = date('Y', time());
		 $this->htext_container('span');
		 $this->sel_day = & $this->addItemContent(new htext_container('select'));
		 $this->sel_day->setName($prefix . 'day');
		 for ($i = 1; $i < 32; $i++) {
		 	$opt = & $this->sel_day->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container($i));
  	 		$opt->addOption('value', $i);
		 }
		 $this->sel_month = & $this->addItemContent(new htext_container('select'));
		 $this->sel_month->setName($prefix . 'month');
		 for ($i = 1; $i < 13; $i++) {
		 	$opt = & $this->sel_month->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container(date($month_macro, mktime ( 0, 0, 0, $i))));
  	 		$opt->addOption('value', $i);
		 }
		 $this->sel_year = & $this->addItemContent(new htext_container('select'));
		 $this->sel_year->setName($prefix . 'year');
		 for ($i = $year_from; $i <= $year_to; $i++) {
		 	$opt = & $this->sel_year->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container($i));
  	 		$opt->addOption('value', $i);
		 }
	}

	function SetupContent($date) {
		/*
		 * устанавливает дату, заданную в unixtime
		 * или строкой для strtotime
		 */
		 
		if ( is_string($date) )
			$date = strtotime($date);
		$dates = getdate($date);
  	 	foreach($this->sel_day->Content as $SelOption) {
  	 		$SelOption->rmOption('selected');
  	 		if ($SelOption->Options['value'] == $dates['mday'])
  	 			$SelOption->addOption('selected');
		}
  	 	foreach($this->sel_month->Content as $SelOption) {
  	 		$SelOption->rmOption('selected');
  	 		if ($SelOption->Options['value'] == $dates['mon'])
  	 			$SelOption->addOption('selected');
		}
  	 	foreach($this->sel_year->Content as $SelOption) {
  	 		$SelOption->rmOption('selected');
  	 		if ($SelOption->Options['value'] == $dates['year'])
  	 			$SelOption->addOption('selected');
		}
		 
	}

 }
 

 class HTML_Time extends HTML {
 	/*
 	 * формирует span из трех select для формировании даты
 	 */

	var $sel_hour;
	var $sel_min;
	var $sel_sec;
 
 	function HTML_Time($prefix) {
		/*
		 * конструктор строит нужный span-HTML
		 */
		 
		 $this->htext_container('span');
		 $this->sel_hour = & $this->addItemContent(new htext_container('select'));
		 $this->sel_hour->setName($prefix . 'hour');
		 for ($i = 0; $i < 25; $i++) {
		 	$opt = & $this->sel_hour->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container($i));
  	 		$opt->addOption('value', $i);
		 }
		 $this->sel_min = & $this->addItemContent(new htext_container('select'));
		 $this->sel_min->setName($prefix . 'min');
		 for ($i = 0; $i < 61; $i++) {
		 	$opt = & $this->sel_min->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container($i));
  	 		$opt->addOption('value', $i);
		 }
		 $this->sel_sec = & $this->addItemContent(new htext_container('select'));
		 $this->sel_sec->setName($prefix . 'sec');
		 for ($i = 0; $i < 61; $i++) {
		 	$opt = & $this->sel_sec->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container($i));
  	 		$opt->addOption('value', $i);
		 }
	}

	function SetupContent($date) {
		/*
		 * устанавливает дату, заданную в unixtime
		 * или строкой для strtotime
		 */
		 
		if ( is_string($date) )
			$date = strtotime($date);
		$dates = getdate($date);
  	 	foreach($this->sel_hour->Content as $SelOption) {
  	 		$SelOption->rmOption('selected');
  	 		if ($SelOption->Options['value'] == $dates['hours'])
  	 			$SelOption->addOption('selected');
		}
  	 	foreach($this->sel_min->Content as $SelOption) {
  	 		$SelOption->rmOption('selected');
  	 		if ($SelOption->Options['value'] == $dates['minutes'])
  	 			$SelOption->addOption('selected');
		}
  	 	foreach($this->sel_sec->Content as $SelOption) {
  	 		$SelOption->rmOption('selected');
  	 		if ($SelOption->Options['value'] == $dates['seconds'])
  	 			$SelOption->addOption('selected');
		}
		 
	}

 }

class HTML_File {
	
	function HTML_File() {
		 $this->htext_container('input');
		 $this->sel_day = & $this->addItemContent(new htext_container('select'));
		 $this->sel_day->setName($prefix . 'day');
		 for ($i = 1; $i < 32; $i++) {
		 	$opt = & $this->sel_day->addItemContent(new htext_container('option'));
  	 		$opt->addItemContent(new ptext_container($i));
  	 		$opt->addOption('value', $i);
		 }
	}
	
}

?>
