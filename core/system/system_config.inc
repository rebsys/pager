<?php
/*
 * Created on 28.09.2008
 *
 * system-module configurator
 */

if ( ! defined('MainSystemModuleDefines') ):

define('MainSystemModuleDefines', TRUE);
define('MainSystemModulePath', dirname(__FILE__));

// добавим некоторые нужные каталоги в include_path
$ipath_arr = explode(PATH_SEPARATOR, ini_get('include_path'));

foreach (
    array (
        MainSystemModulePath,
        $_SERVER['DOCUMENT_ROOT'],
        dirname($_SERVER['SCRIPT_FILENAME'])
    )
    as $dir
) {

    if ( ! in_array($dir, $ipath_arr))
        array_push($ipath_arr, $dir);

}

if ( ini_set('include_path', implode(PATH_SEPARATOR, $ipath_arr)) === FALSE ) {
    trigger_error('Unable add my path to include_path', E_USER_ERROR);
    die('Unable add my path to include_path');
}

/*
 * TODO: проверить наличие файла с классом вот так:
 *
// works
if ((include 'vars.php') == 'OK') {
    echo 'OK';
}
 * и от этого пробовать разные варианты. если не пойдет, пробовать пути из include_path
 * в цикле, по типу примера выше: $ipath_arr = explode(PATH_SEPARATOR, ini_get('include_path'));
 */

function __autoload_SystemPath($path) {
    if ( ! file_exists($path) )
        return FALSE;
    if ( ! preg_match('*(^|' . PATH_SEPARATOR . ')' . $path . '($|' . PATH_SEPARATOR . ')*', ini_get('include_path')) )
        ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . $path);
    return TRUE;
}

function __autoload_system_class($class_name, $path) {

	$ExtArr = array('.class', '.inc', '.php');

    foreach ( $ExtArr as $ext ) {

        if ( file_exists($path . $class_name . $ext) ) {

            include_once $path . $class_name . $ext;
            if ( ! defined('FullSystemClassesPath') )
                define('FullSystemClassesPath', $path);
            return TRUE;

        }
    }

    return FALSE;

}

function __autoload_system($class_name) {

//    if ( defined('FullSystemClassesPath') )
//        return __autoload_system_class($class_name, FullSystemClassesPath);

    $DirArr = explode(DIRECTORY_SEPARATOR, dirname($_SERVER['PHP_SELF']));
    $Path = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR;

    if ( __autoload_system_class($class_name, MainSystemModulePath . DIRECTORY_SEPARATOR) ) {
        return TRUE;
    }

    foreach ( $DirArr as $Dir) {

        if ( ! empty($Dir) )
            $Path .= $Dir . DIRECTORY_SEPARATOR;
        $DirName = $Path . 'system' . DIRECTORY_SEPARATOR;


        if ( ! file_exists($DirName) )
            continue;

        if ( __autoload_system_class($class_name, $DirName) ) {
            if ( ! preg_match('*(^|' . PATH_SEPARATOR . ')' . $Path . '($|' . PATH_SEPARATOR . ')*', ini_get('include_path')) )
                ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . $Path);
            return TRUE;
        }

    }

	return FALSE;

}
 
 function __autoload($class_name) {

	if ( ! __autoload_system(strtolower($class_name)) )
		__autoload_system($class_name);

 }

 endif;

?>
