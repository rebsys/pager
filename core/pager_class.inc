<?php
/**
 * класс pager_class для реализации объекта Pager
 *
 * включает в себя свойства и методы для работы с параметрами запроса, контента,
 * параметрами ответа и пр.
 */
class pager_class extends pager_core implements pager_interface {

    function Init() {

        $PaC = PagerConfig::GetPagerConfig();
        $URI = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
        $this->SetPageID( $URI );
        $PaC->SetVar('RequestID', $URI);

    }

}

?>
