<?php
/**
 * интерфейс для драйвера хранилища StoreEngine
 * 
 * 1. Поддержку версий мы изживем, скорее всего.
 * 2. Перед выполнением запроса (Get, Put, Del) возможно установить/сбросить
 * некоторые поддерживаемые драйвером фичи использованием запроса 
 * OkFeature(int $action = SEF_ActionSET | SEF_ActionDEL | SEF_ActionRES, 
 *	     int $feature = SEF_setExists | SEF_setList | SEF_setRevision | SEF_setOwner | [...] [, option array with other parameters...]
 * )
 * которые будут выполнены в момент выполнения запроса.
 * 
 * Полный список фич:
 *
 * SEF_setList - возвращает список названия объектов в виде массива
 *    Область применения: GetObject
 *    Возможность сочетания: SEF_setRevision
 *    Описание: Воспринимает $object_path в запросе как путь к каталогу или
 *		путь с вилдкадами * или ?. Может возвращать пустой массив, если совпадений не найдено.
 *		В сочетании с SEF_setRevision возвращает список всех доступных версий объекта, использование
 *		вилдкардов в таком запрос запрещено.
 *    Возвращает: Нарушает работу обычного GetObject и возвращает массив объектов, соответствующих запросу. Формат массива
 *              имеет следующий вид:
 *		   <object name> - имя объекта
 *		      {	// все харакетиристики объекта опциональны и могут быть опущены при ответе
 *			size => <size of object in bytes>
 *			time_c => <time of last change object>
 *			author_id => <identificator of author of this object>
 *			....
 *		      }
 * 
 * SEF_setRevision - задает ревизию объекта для действия
 *    Область применения: GetObject, PutObject, DelObject
 *    Возможность сочетания: SEF_setExists, SEF_setList, SEF_setOwner
 *    Описание: Воспринимает $object_path в запросе как путь к объекту
 *
 * SEF_setExists - проверяет наличие объекта
 *    Область применения: GetObject
 *    Возможность сочетания: SEF_setRevision
 *    Описание: Воспринимает $object_path в запросе как путь к каталогу или объекту
 * 
 * SEF_setOwner - устанавливает идентификатор владельца объекта при сохранении объекта
 *    Область применения: PutObject
 *    Возможность сочетания: SEF_setRevision
 *    Описание: Воспринимает $object_path в запросе как путь к объекту
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */

// ACTION of feature
define('SEF_ActionSET', 1);
define('SEF_ActionDEL', 2);
define('SEF_ActionRES', 3);

// FEATURE by name
define('SEF_setExists',   1);
define('SEF_setList',     2);
define('SEF_setRevision', 3);
define('SEF_setOwner', 	  4);

interface store_interface {

    /**
     * возвращает список поддерживаемых фич
     */
    function GetFeatures();

    /**
     * возвращает текущую версию драйвера или фичи
     */
    function GetVersion( $feature=null );


    /**
     * возвращает объект типа $object_type с путем $object_path
     */
    function GetObject( $object_type, $object_path );

    /**
     * помещает объект $obj типа $object_type в путь $object_path
     */
    function PutObject( $object_type, $object_path , $obj);

    /**
     * удаляет объект типа $object_type с путем $object_path
     */
    function DelObject( $object_type, $object_path );

    /**
     * 
     */
    function OkFeature($action, $feature=null, $params=array());

}

?>
