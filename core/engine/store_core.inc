<?php
/**
 * StoreEngine класс для манипуляции данными, хранимыми в различных
 * системах хранения - Storages.
 *
 * все объекты разложены на несколько виртуальных каталогов
 * pager - XML объекты для страниц
 * module - объекты-указатели на экземпляры классов модулей
 * file - строчные объекты произвольного содержимого
 *
 * в любом пути может префикс вида <storage_type>:/
 * где storage_type один из известных типов хранения данных:
 * file  - локальное хранилище в файловой системе
 * s3    - удаленное хранилище на aws s3
 * db	 -хранилище в БД mysql
 * ddb	 - AWS DynamoDB
 *
 *
 * private StorageDrivers - массив для хранения указателей на
 * драйвера различных хранилищ со списком поддерживаемых фич
 * storage_type => array {
 *   Driver - указатель на драйвер
 *   Version - версия драйвера
 *   Futures - список фич
 * }
 *
 * Служебные константы:
 * string StoreEngine_ModuleRelPath - задает безусловную замену текущего каталога в путях
 * к исполняемым классам модулей. Обычно в названиях модулей путь не указывается, поэтому
 * проще сразу переделать его от корня.
 * boolean StoreEngine_CurrentDir_is_Dot - указывает, что текущий каталог в пути к любому
 * объекту указывается конструкцией './', или просто отсутствием ведущего слэша в пути
 *
 * Драйверы подключаются по мере необходимости. Когда StoreEngine встречает объект,
 * он пытается определить для объекта нужный драйвер и использовать его. Если дравер
 * не инициализирован, он будет пытаться его инициализировать в момент первого использования.
 * Таим образом мы избегает лишний мороки с неиспользуемыми драйверами и авторизацией хранилищ.
 *
 */

/*

Версионность объектов. !!!!!!!!!!!!!!!!!!!! все описание перенести в интерфейс !!!!!!!!!!!!!!!!
В нынешней модели хранения объектов, у любого объекта может быть версии трех разных видов:
 1. HEAD - головная, основная версия, синоним полнорго имени без указания версий;
 2. DRAFT - рабочая версия, временная версия для текущего редактирования;
 3. REV[0-9]{14} - номер ревизии для объекта;
В этой модели только версии HEAD и DRAFT являются перезаписываемыми. Версии, представляющие собой
номер ревизии, могут быть только однократны сохранены: созданы и записаны. Но все виды версий,
разумеется, являются читаемыми.
Каждая версия может хранить размер, указание автора и времени создания или последнего сохранения
для HEAD & DRAFT - зависит от возможностей драйвера хранилища.
Номер ревизии объекта - это 14 цифр, содержащих время создания этой ревизии в формате unixtime и 
4 случайных числа для исключения дудлирования: unixtime+rand(1000,9999).
Каждый объект может задаваться обычным путем, это синоним версии вида HEAD, или прямым указанием
вида версии, в котором версия от пути отделяется тремя символами '@'. Вот пример указания обекта
корневой страницы, со временем создания на момент написания этих строк:
    ROOT.xml@@@REV1393615075
Когда мы обращаемся к объекту без указания версии, движок хранилища для начала спрашивает версию
HEAD объекта, а если ее не находит, тогда спрашивает тот УРЛ объекта, который явно указан. Это
позволяет нам использовать обычные пути к объектам, без указания версий, которые будут одинаково
хорошо работать для хранилищ с поддержкой версий, так и для хранилищ без такой поддержки.

Сохранение.
Когда мы запрашиваем сохранение объекта, мы всегда сохраняем его в DRAFT. Когда мы запрашиваем
сохранение объекта в HEAD, мы генерируем новую ревизию и сохраняем ее копию в HEAD. Кроме того,
можно принудительно заставить сохранить объект в новую ревизию. Этот функционал должен реализовываться
или engine_core или редактором. И, скорее всего, именно редактором. А еще вернее, модулем put_object.

Фичи, поддерживаемые драйвером хранилища.
1. revision - поддержка ревизий объекта.
    Понимает параметры:
    * list - возвращает полный список доступных версий этого объекта, работает для урла любого вида,
    т.е. для урла с указанием версии, версия будет убрана и выданы все доступные версии именно для
    самого объекта.
-2. list - запрос списка объектов
-3. wlist - запрос списка объектов по маске wildcards
-4. plist - запрос списка объектов по regexp
-5. exists - запрос наличия объекта и доступности для пользователя User

Расширенный запрос.
 Выполняется добавлением к обычному GetObject, SaveObject, DestroyObject специального параметра
 Ext, который представляет собой массив специального формата:
  Auth => array
     User - string, идентификатор пользователя, инициирующего запрос
     Cred - string, Credentials этого пользователя
 Feature => array
     Name - string, название запрашиавемой фичи
     Params - array, произвольный список параметров, требуемых запрошенной фичи
*/


/*

--1. Задать типы хранилища по умолчанию для всех типов ресурсов: страницы, модули, файлы
2. Задать массив доступных типов хранилищ, и их драйверов
3. Определить интерфейс драйвера хранилища: методы и что-то еще?

StorageDefaultPage = file
StorageDefaultMudule = file
StorageDefaultFile = file

StorageType_file = store_file
StorageType_db = store_db
StorageType_s3 = store_s3

*/

// поддерживаемые типы объектов через запятую
define('ObjectNameList', 'Page,Module,File');
// настройки доступа
define('StorageAccessRoot', '/EstoreAccess/');
define('StorageAccessUserGroupNode', 'UserGroups');
define('StorageAccessRuleSetNode', 'RuleSet');

#require_once 'store_acl.inc';

class StoreEngine implements estore_interface {

    private $Log;
    private $StorageDrivers;
    private $ObjectPath = '';		// объект с путем
    private $ObjectType = '';		// тип объекта - модуль, файл, страница
    private $ObjectTypeName = '';	// текстовое название типа объекта
    private $ObjectDrive = '';		// драйвер для работы с объектом
    private $ObjectRevision = '';	// идентификатор ревизии объекта
    private $AccessXML;			// правила доступа
    private $AccessXP;
    private $UserGroupsXML;		// группы пользователей
    private $UserGroupsXP;

    function  __construct() {
        $this->Log = logger::GetLogger();
        $this->StorageDrivers = array();
        if ( ! defined('CurrentDir_is_Dot') )
            define('CurrentDir_is_Dot', true);
        $tmpXML = new DOMDocument('1.0', 'UTF-8');
        $this->AccessXML = new DOMDocument('1.0', 'UTF-8');
        $this->UserGroupsXML = new DOMDocument('1.0', 'UTF-8');
        if ( file_exists(StoreEngine_FileAccessXML) ) {
            $tmpXML->load(StoreEngine_FileAccessXML);
        }
        $tmpXP = new DOMXPath($tmpXML);
        $tmpNL = $tmpXP->query(StorageAccessRoot . StorageAccessUserGroupNode);
        if ( $tmpNL->length > 0 ) {
            $this->UserGroupsXML->appendChild($this->UserGroupsXML->importNode($tmpNL->item(0), true));
        } else {
            $this->UserGroupsXML->appendChild($this->UserGroupsXML->createElement(StorageAccessUserGroupNode));
        }
        $tmpNL = $tmpXP->query(StorageAccessRoot . StorageAccessRuleSetNode);
        if ( $tmpNL->length > 0 ) {
            $this->AccessXML->appendChild($this->AccessXML->importNode($tmpNL->item(0), true));
        } else {
            $this->AccessXML->appendChild($this->AccessXML->createElement(StorageAccessRuleSetNode));
        }
        $this->AccessXP = new DOMXPath($this->AccessXML);
        $this->UserGroupsXP = new DOMXPath($this->UserGroupsXML);

    }

    function __init() {
        $this->ObjectPath = '';
        $this->ObjectType = '';
        $this->ObjectDrive = '';
        $this->ObjectRevision = '';
    }

    /**
     * разбирает существующие определения хранилища типа $type и запускает инит драйвера,
     * формирует StorageDrivers
     * $type - один из видов хранилища: file, db, s3 или другие
     */
    function __prepare_storages($type=NULL) {

        if ( $type == NULL )
            $type = $this->ObjectDrive;
    
        if ( ! array_key_exists($type, $this->StorageDrivers) ) {

            $this->StorageDrivers[$type] = array(
                'Driver' => $this->__init_drive($type)
            );
            $this->StorageDrivers[$type]['Version'] = $this->StorageDrivers[$type]['Driver']->GetVersion();
            $this->StorageDrivers[$type]['Feature'] = $this->StorageDrivers[$type]['Driver']->GetFeatures();

        }
        $this->StorageDrivers[$type]['Driver']->OkFeature(SEF_ActionRES);

    }

    /**
     * подключает и инициализирует драйвер хранилища $drive
     * 
     * возвращает ссылку на драйвер
     */
    function __init_drive($drive) {

        if ( ! defined('StorageType_' . $drive) )
            throw new Exception("driver for $drive not defined", 501);

        $StoreTypeClass = constant('StorageType_' . $drive);
        
        if ( ! class_exists($StoreTypeClass) ) {
            //require_once StoreLocation . DIRECTORY_SEPARATOR . $StoreTypeClass . '.inc';
            require_once $StoreTypeClass . '.inc';
        }

        return new $StoreTypeClass;

    }


    /**
     * Формирует ObjectPath, ObjectDrive and ObjectType по запрошеному пути
     * Если в пути есть указание ревизии, формирует и ObjectRevision
     */
    function __parse_path($path) {
        // the path is: <object_type>/[drive_type:/]<object_path>[<revision>]
        // <revision> = StoreEngine_RevisionDelimiter(REV[0-9]{14}|HEAD|DRAFT)

        if ( preg_match('%^(.*)' . StoreEngine_RevisionDelimiter . '(REV[0-9]{14}|HEAD|DRAFT)$%', $path, $arr) ) {
            // удалим ревизию из $path и запишем ее в ObjectRevision
            $path = $arr[1];
            $this->ObjectRevision = $arr[2];
        }

        if ( preg_match('%/?(\w+/)((\w+):)?(/?.*)%', $path, $arr) ) {
            // $arr[1] - object type, 3 - driver on empty, 4 - path

            $this->Log->msg("parse path '$path' => '${arr[1]}', '${arr[2]}', '${arr[4]}'", LOG_DEBUG2);

            foreach ( explode (',', ObjectNameList) as $ObjectTypeName ) {

                if ( $arr[1] == constant($ObjectTypeName . 'Prefix') ) {

                    $this->ObjectType = constant($ObjectTypeName . 'Object'); // set type
                    $this->ObjectTypeName = $ObjectTypeName . 'Object';
                    if ( empty($arr[3]) ) // set this driver or default
                        $this->ObjectDrive = constant($ObjectTypeName . 'StorageDefault');
                    else
                        $this->ObjectDrive = $arr[3];
                    $this->ObjectPath = $arr[4]; // set path

                }
            }

            if ( empty($this->ObjectType) )
                throw new Exception("unknown object type: ' . $arr[1] . '", 504);

            // заменим относительные пути текущим каталогом страницы
            $this->Log->msg(sprintf("path was parsed for object type of %s, from %s:%s", $this->ObjectType, $this->ObjectDrive, $this->ObjectPath), LOG_DEBUG2);
            try {
                $PaC = PagerConfig::GetPagerConfig();
                $RelativePath = dirname($PaC->GetVar('PageID', ReadOnly_Section));

                // для модулей есть возможность заблокировать подстановку текущего пути,
                // установкой константы StoreEngine_ModuleRelPath
                if ( $this->ObjectType == ModuleObject and
                     defined('StoreEngine_ModuleRelPath')
                ) {
                    $RelativePath = StoreEngine_ModuleRelPath;
                }

                if ( StoreEngine_CurrentDir_is_Dot ) {
                    $this->ObjectPath = preg_replace('%^./%', $RelativePath . '/', $this->ObjectPath, 1);
                    $this->Log->msg(sprintf("change ./ to '%s' in path to current dir: '%s'", $RelativePath, $this->ObjectPath), LOG_DEBUG3);
                } else {
                    if ( empty($this->ObjectPath) or $this->ObjectPath[0] != '/' )
                        $this->ObjectPath = $RelativePath . $this->ObjectPath;
                    $this->Log->msg(sprintf("change relative path to '%s' in current dir: '%s'", $RelativePath, $this->ObjectPath), LOG_DEBUG3);
                }
            } catch (Exception $e) {
                    $this->Log->msg('Not PageID in PagerConfig at this moment', LOG_DEBUG3);
            }
            

        } else {

            throw new Exception("invalid path '$path' for object", 503);

        }
        
    }

    /**
     * проверяет поддержку фичи у драйвера
     */
    function __check_feature($feature) {
        if ( ! in_array($feature, $this->StorageDrivers[$this->ObjectDrive]['Feature']) ) {
            // этой фичи нет
            $this->Log->msg(sprintf('Driver for "%s" does not support feature id="%d"', $this->ObjectPath, $feature), LOG_WARNING);
            return FALSE;
        }
        return TRUE;
    }

    /**
     * настраивает владельца объекта в драйвере
     */
    function __prepare_auth($Ext) {
        
        if ( ! $this->__check_feature(SEF_setOwner) )
            throw new Exception(sprintf('Driver for "%s" does not support feature "SEF_setOwner"', $this->ObjectPath), 505);
        $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setOwner, $Ext['Auth']['User']);
        
    }

    /**
     * прверяет и устанавливает нужные фичи драйвера
     */
    function __prepare_feature($Ext) {

        switch ( $Ext['Feature']['Name'] ) {
        
            case SEF_RevisionList :
            case 'RevisionList' :	// feature's alias
                if ( ! $this->__check_feature(SEF_setList) or ! $this->__check_feature(SEF_setRevision) )
                    return;
                if ( $Ext['Action'] == 'SaveObject' or $Ext['Action'] == 'DestroyObject' ) {
                    throw new Exception("feature 'RevisionList' is not compatible with 'SaveObject' or 'DestroyObject' methods", 506);
                }
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setList);
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setRevision);
                break;
            case SEF_setRevision :
            case 'Revision' :	// feature's alias
                if ( ! $this->__check_feature(SEF_setRevision) )
                    return;
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setRevision, $Ext['Feature']['Params']);
                break;
            case SEF_Exists :
            case 'Exists' :	// feature's alias
                if ( ! $this->__check_feature(SEF_setExists) )
                    return;
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setExists);
                break;
            case SEF_List :
            case 'List' :	// feature's alias
                if ( ! $this->__check_feature(SEF_setList) )
                    return;
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setList);
                break;
            default :
                throw new Exception("unknown feature '" . $Ext['Feature']['Name'] . "' for object", 505);
        }
    }


    /**
     * проверяет нахождение юзера (или группы, в которую он входит) в списке разрешений доступа
     * возвращает: 
     * TRUE - если список пользователей/групп пуст - истинно для любого пользователя
     * TRUE - если список не пуст и в списке есть пользователь $user явно или в составе группы
     * FALSE - если список определен, но пользователя в нем нет
     * FALSE - если пользователь не определен, но список разрешений не пустой
     * param: $user
     * param: DOMElement $node
     * return: bool
     */
    function __check_user_access($user, DOMElement $node) {

        if ( ! $node->hasChildNodes() ) {
            $this->Log->msg('CheckAccess: список ограничений для этого правила пустой - правило применяется для всех', LOG_DEBUG3);
            return TRUE; // список пуст
        }
        if ( empty($user) ) {
            $this->Log->msg('CheckAccess: пользователь/логин для этого запроса не определен, но список ограничений не пустой - правило игнорируется', LOG_DEBUG3);
            return FALSE; // список не пуст, но пользователь не определен
        }
        if ( $this->AccessXP->query("User[text()='$user']", $node)->length > 0 ) {
            $this->Log->msg("CheckAccess: правило применяется для пользователя $user", LOG_DEBUG3);
            return TRUE; // юзер явно указан в списке
        }
        foreach ( $this->UserGroupsXP->query("Group[User/text()='$user']") as $group_node ) {
            // получаем список всех групп, в которые входит юзер
            if ( $this->AccessXP->query(
                    "Group[text()='" . $this->UserGroupsXP->query('Name', $group_node)->item(0)->nodeValue . "']",
                    $node
                 )->length > 0 
            ) {
                $this->Log->msg("CheckAccess: правило применяется для пользователя $user в группе " . $this->UserGroupsXP->query('Name', $group_node)->item(0)->nodeValue, LOG_DEBUG3);
                return TRUE; // группа явно указана в списке разрешений
            }
        }
        
        $this->Log->msg('CheckAccess: способ применения правила неопределен - правило игнорируется', LOG_DEBUG3);
        return FALSE;
        
    }

    /**
     * задает драйверу хранилища нужную версию объекта
     */
    function __prepare_revision() {
    
        if ( ! empty($this->ObjectRevision) ) {
            $this->__prepare_feature(
                array('Feature'  => array(
                        'Name'   => SEF_setRevision,
                        'Params' => array(
                            'RevID' => $this->ObjectRevision,
                        )
                    )
                )
            );
        }
    
    }

    /**
     * проверяет доступность объекта $path для операции $Ext['Action']
     */
    function CheckPermission($action, $path, $Ext) {

        $RuleNL = $this->AccessXP->query('Rule[ObjectType="' . $this->ObjectTypeName . '" and ' . $action . '/*]');
        if ( $RuleNL->length == 0 ) {
            $this->Log->msg('CheckPermission: нет подходящих для запроса правил', LOG_DEBUG2);
            return;
        }
        
        foreach ( $RuleNL as $RuleNode ) {
            // проверим соответствие пути объекта
            $AddressFlag = TRUE;
            foreach ( $this->AccessXP->query('Address/Pattern', $RuleNode) as $PatternNode ) {
                // проверим путь и пропустим, если путь указан и не совпадает
                // будем обрабатывать, если путь опущен - правило для всех объектов с любым путем
                $MatchTypeNL = $this->AccessXP->query('Address/MatchType', $RuleNode);
                if ( $MatchTypeNL->length > 0 )
                    $MatchType = $MatchTypeNL->item(0)->nodeValue;
                else
                    $MatchType = 'wildcard';
                switch ( $MatchType ) {
                    case 'wildcard':
                    default:
                        $PatternRE = '^' . str_replace('*', '.*', $PatternNode->nodeValue) . '$';
                        break;
                    case 'regexp':
                        $PatternRE = $PatternNode->nodeValue;
                }
                $AddressFlag = preg_match("|$PatternRE|", $this->ObjectPath);
                if ( $AddressFlag ) {
                    $this->Log->msg('CheckPermission: для объекта: ' . $this->ObjectPath . " определн шаблон $PatternRE", LOG_DEBUG3);
                    break;
                }
            }
            if ( ! $AddressFlag )
                continue;
            // объект попадает в зону действия текущего правила
            // теперь проверим содержимое типа доступа
            $this->Log->msg('CheckPermission: найдено правило для: ' . $this->ObjectPath, LOG_DEBUG2);
            foreach ( $this->AccessXP->query("$action/*", $RuleNode) as $AccessNode ) {
                $user_access = $this->__check_user_access($Ext['Auth']['User'], $AccessNode);
                switch ( $AccessNode->nodeName ) {
                    // проверяем не пустой ли список, если пустой - сразу применяем значение $AccessNode->nodeName: return or throw exception
                    // если не пустой - получаем список пользователей и групп и проверяем наличие текущего пользователя
                    // из $Ext['Auth'] в каком-либо списке вида доступа $AccessNode->nodeName
                    // если находим соответствие - применяем: continue or throw exception
                    case 'Allow':
                        $this->Log->msg('CheckPermission: совпадений Allow для пользователя ' . $Ext['Auth']['User'] . '=' . var_export($user_access, true), LOG_DEBUG2);
                        if ( $user_access )
                            return; // доступ разрешен
                        break; // нет совпадений, смотрим следующее правило
                    case 'Deny':
                        $this->Log->msg('CheckPermission: совпадений Deny для пользователя ' . $Ext['Auth']['User'] . '=' . var_export($user_access, true), LOG_DEBUG2);
                        if ( $user_access )
                            throw new Exception('Access deny for object ' . $this->ObjectPath, 403); // доступ запрещен
                        break; // нет совпадений, смотрим следующее правило
                    default:
                        throw new Exception('"' . $AccessNode->nodeName . '" is unknown access-method for object ' . $this->ObjectPath, 405);
                }
            }
        }
        
    }

    /**
     * подготавливает объект $obj для манипуляций: разбирает путь, готовит драйвер и пр.
     */
    function PrepareObject($path) {

        $this->__init(); // init saved object
        $this->__parse_path($path);
        $this->__prepare_storages();
        $this->__prepare_revision();

    }

    /**
     * анализирует параметр расширенного запроса $Ext
     * и проверку доступа, если она активна
     */
    function GoExt($action, $path, $Ext) {

        if ( ! is_array($Ext) )
            return;

        $Ext['Action'] = $action;

        if ( array_key_exists('Feature', $Ext) )
            $this->__prepare_feature($Ext);
        
        if ( array_key_exists('Auth', $Ext) )
            $this->__prepare_auth($Ext);

    }
    

    /**
     * возвращает объект с путем $path в виде строки
     */
    function GetObject($path, array $Ext=null) {


        $this->Log->msg("get object $path", LOG_DEBUG);

        $this->PrepareObject($path);
        $this->CheckPermission(__FUNCTION__, $path, $Ext);

        $this->GoExt(__FUNCTION__, $path, $Ext);
        
        // проверим StoreEngine_RevisionHEADasObject, если true - при запросах без указания ревизии поищем HEAD для этого объекта,
        // и попытаемся вернуть именно его. Если false - ищем точно то, что запрашивали.
        if (
            empty( $this->ObjectRevision )
            and StoreEngine_RevisionHEADasObject
            and (
                $Ext == NULL or
                ! array_key_exists('Feature', $Ext)
            )
            and ( $this->ObjectType != ModuleObject or StoreEngine_RevisionHEADasModule )
            and $this->__check_feature(SEF_setRevision)
            and $this->__check_feature(SEF_setExists)
        ) {
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setRevision, array('RevID'=>'HEAD'));
                $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setExists);
                $check_head = $this->_getObject();
                $this->GoExt(__FUNCTION__, $path, $Ext);
                if ( $check_head )
                    $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionSET, SEF_setRevision, array('RevID'=>'HEAD'));
        }
        return $this->_getObject();

    }

    /**
     * внутренний метод для получения объекта
     * возвращает объект с путем $this->ObjectPath в виде строки
     */
    function _getObject() {

        try {
            return $this->StorageDrivers[$this->ObjectDrive]['Driver']
                    ->GetObject(
                        $this->ObjectType,
                        $this->ObjectPath
                    );
        } catch (Exception $e) {
            $this->Log->msg('get object ' . $this->ObjectPath .' failed', LOG_DEBUG);
            $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionRES);
            throw $e;
        }
    }

    /**
     * сохраняет объект $data_obj с путем $path
     */
    function SaveObject($path, $data_obj, array $Ext=null) {

        $this->Log->msg("write object $path", LOG_DEBUG);

        $this->PrepareObject($path);
        $this->CheckPermission(__FUNCTION__, $path, $Ext);

        $this->GoExt(__FUNCTION__, $path, $Ext);
        
        try {
            return $this->StorageDrivers[$this->ObjectDrive]['Driver']
                        ->PutObject(
                            $this->ObjectType,
                            $this->ObjectPath,
                            $data_obj
                        );
        } catch (Exception $e) {
            $this->Log->msg("write object $path failed", LOG_DEBUG);
            $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionRES);
            throw $e;
        }

    }

    /**
     * удаляет объект с путем $path
     */
    function DestroyObject($path, array $Ext=null) {

        $this->Log->msg("delete object $path", LOG_DEBUG);

        $this->PrepareObject($path);
        $this->CheckPermission(__FUNCTION__, $path, $Ext);
        $this->GoExt(__FUNCTION__, $path, $Ext);
        
        try {
            return $this->StorageDrivers[$this->ObjectDrive]['Driver']
                        ->DelObject(
                            $this->ObjectType,
                            $this->ObjectPath
                        );
        } catch (Exception $e) {
            $this->StorageDrivers[$this->ObjectDrive]['Driver']->OkFeature(SEF_ActionRES);
            throw $e;
        }


    }

}

?>
