<?php
/**
 * вариант StoreEngine класса для работы с файловой системой
 *
 * PreDefined
 * SF_RootHTML - regex-шаблон для замены корневых струтур типа "/.html"
 * на более понятные в стандартной файловой системе "PM_PageIndex . '.html'",
 * где PM_PageIndex - предопределенный синоним для "/"
 * SF_CheckRealObjectPath - определяет необходимость проверять истинный путь объекта и попадание его в указанный CHROOT хранилища
 *
 * все объекты распределены по типам:
 * PageObject - XML объекты для страниц
 * ModuleObject - объекты-указатели на экземпляры классов модулей
 * FileObject - строчные объекты произвольного содержимого
 *
 */

require_once 'store_interface.inc';

if ( ! defined('SF_CheckRealObjectPath') ) define('SF_CheckRealObjectPath', TRUE);
define ('SF_DefaultFileOwner', 'nobody'); // дефолтный владелец новых файлов
define ('SF_RootHTML', '#/.(html|xml|xsl)#');
define ('SF_TrueRootHTML', DIRECTORY_SEPARATOR . PM_PageIndex . '.\1');
define ('SF_doFeatureAction_ReturnResult', 1); // указывает на необходимость вернуть Data как результат выполнения запроса
define ('SF_doFeatureAction_ChangeObjPath', 2); // указывает на необходимость изменения пути объекта, старый и новый путь в Data: OldObjPath, NewObjPath
define ('SF_doFeatureAction_RemoveObjPath', 3); // указывает на необходимость удаления объекта, путь к объекту в Data: RemoveObjPath
define ('SF_doFeatureAction_CreateObjPath', 4); // указывает на необходимость создания объекта, путь к объекту в Data: CreateObjPath
define ('SF_doFeatureAction_ReplaceObjPath', 5); // указывает на необходимость замены объекта - удаление старого и работы с новым объектом, старый и новый пути в Data: OldObjPath, NewObjPath

class store_file implements store_interface {

    private $Log;
    private $Features;

    function  __construct() {
        $this->Log = logger::GetLogger();
        $this->_resetFeatures();
    }

    /*
     * обнуляет все утсановленные фичи
     */
    private function _resetFeatures() {
        $this->Features = array();
    }


    /*
     * возвращает массив файлов, подходящих под заданные параметры
     * поиска: $path - путь к объекту, $rev - ревизия, $owner - владелец.
     * последние два параметра опциональны.
     * возвращаемый массив содержит имена файлов как ключи для полученных
     * характеристик файлов: размер, время последней модификации и пр.
     */
     private function _dirPath($path, $rev='REV[0-9]{14}|HEAD|DRAFT', $owner='\w+') {
        $res_arr = array();
        $dpath = dirname($path);
        $bpath = basename($path);
        if ( ! is_dir($dpath) )
            return $res_arr;
        foreach ( scandir($dpath) as $filename ) {
            $fpath = $dpath . '/' . $filename;
            // преобразуем вилдкарды в регекс
            $regex = str_replace('\*', '.*', preg_quote($bpath, '%'));
            $regex = str_replace('\?', '.', $regex);
            $regex = str_replace('\[\^', '[^', $regex);
            $regex = str_replace('\[', '[', $regex);
            $regex = str_replace('\]', ']', $regex);
            if ( ! empty($rev) )
                $regex .= StoreEngine_RevisionDelimiter . '(' . $rev . ')';
            if ( ! empty($owner) )
                $regex .= '@(' . $owner . ')$';
            if ( preg_match('%^' . $regex . '$%', $filename)
            ) {
                if ( ! is_readable($fpath) )
                    continue;
                preg_match('%^(.+)(' . StoreEngine_RevisionDelimiter . '(REV[0-9]{14}|HEAD|DRAFT))?(@(\w+))?$%U', $filename, $arr);
                if ( sizeof($arr) >= 3 )
                    $fkey = $arr[1] . $arr[2];
                else
                    $fkey = $filename;
                /****/
                $farr = stat($fpath);
                $res_arr[$fkey] = array (
                    'size'	=> $farr['size'],
                    'time_c'	=> $farr['ctime'],
                    'real_file'	=> $fpath,
                );
                if ( isset($arr[3]) )
                    $res_arr[$fkey]['RevID'] = $arr[3];
                if ( isset($arr[5]) )
                    $res_arr[$fkey]['author_id'] = $arr[5];
            }
        }
        return $res_arr;
     }

    /*
     * Выполняет или подготавливает все установленные фичи
     * Может возвращать значение, отличное от NULL, которое представляет собой
     * массив следующего вида:
     * 'Action' => SF_doFeatureAction_... - одно из предопределенных действий
     * 'Data'	=> массив данных, связанных с этими действиями
     */
    private function _doFeatures($type, $obj) {

        if ( sizeof($this->Features) == 0 )
            return;

        if ( array_key_exists(SEF_setExists, $this->Features) ) {
            $this->Log->msg("request 'SEF_setExists' for object: $obj", LOG_DEBUG);
            if ( array_key_exists(SEF_setRevision, $this->Features) ) {
                $this->Log->msg("request 'SEF_setRevision' for object: $obj", LOG_DEBUG);
                $rev_files = $this->_dirPath($obj, $this->Features[SEF_setRevision]['RevID']);
                // BEGIN of $fet_res = sizeof($rev_files) > 0 and is_readable(reset($rev_files)['real_file']);
                $fet_res = reset($rev_files);
                if ( $fet_res !== FALSE )
                    $fet_res = $fet_res['real_file'];
                // END
                return array(
                    'Action' => SF_doFeatureAction_ReturnResult,
                    'Data' => $fet_res,
                );
            }
            return array(
                'Action' => SF_doFeatureAction_ReturnResult,
                'Data' => is_readable($obj),
            );
        }

        if ( array_key_exists(SEF_setList, $this->Features) ) {
            $this->Log->msg("request 'SEF_setList' for object: $obj", LOG_DEBUG);
            if ( array_key_exists(SEF_setRevision, $this->Features) ) {
                $this->Log->msg("request 'SEF_setRevision' for object: $obj", LOG_DEBUG);
                return array(
                    'Action' => SF_doFeatureAction_ReturnResult,
                    'Data' => $this->_dirPath($obj),
                );
            }
            return array(
                'Action' => SF_doFeatureAction_ReturnResult,
                'Data' => $this->_dirPath($obj, '', ''),
            );
        }
        
        if ( array_key_exists(SEF_setRevision, $this->Features) ) {
            $this->Log->msg("request 'SEF_setRevision' for object: $obj", LOG_DEBUG);
            $RevList = $this->_dirPath($obj, $this->Features[SEF_setRevision]['RevID']);
            $NewOwner = SF_DefaultFileOwner;
            if ( array_key_exists(SEF_setOwner, $this->Features) ) {
                $NewOwner = $this->Features[SEF_setOwner];
            }
            /*
             * владелец файла при перезаписи не меняется
             */
            if ( sizeof($RevList) == 0 ) {
                return array(
                    'Action' => SF_doFeatureAction_CreateObjPath,
                    'Data' => array(
                        'CreateObjPath' => $obj . StoreEngine_RevisionDelimiter . $this->Features[SEF_setRevision]['RevID'] . '@' . $NewOwner,
                    ),
                );
            } else {
                $RevListCur = current($RevList);
                return array(
                    'Action' => SF_doFeatureAction_ChangeObjPath,
                    'Data' => array(
                        'OldObjPath' => $obj,
                        'NewObjPath' => $RevListCur['real_file'],
                    ),
                );
            }
        }

    }

    /*
     * возвращает версию драйвера или фичи - изживаем?
     */
    function GetVersion($feature=null) {
        
        switch ($feature) {

            default:
                return 1;
        
        }
        
    }
    
    /*
     * возвращает массив со списком поддерживаемых драйвером фич
     */
    function GetFeatures() {

        return array(SEF_setRevision, SEF_setList, SEF_setExists, SEF_setOwner);

    }

    /**
     * выполнение различных фич
     */
    function OkFeature($action, $feature=null, $params=array()) {

        switch ( $action ) {
            case SEF_ActionSET :
                $this->Features[$feature] = $params;
                break;
            case SEF_ActionDEL :
                if ( isset($this->Features[$feature]) )
                    unset($this->Features[$feature]);
                break;
            case SEF_ActionRES :
                $this->_resetFeatures();
                break;
        }
    
    }
    
    /**
     * возвращает объект с путем $path в сиде строки
     */
    function GetObject($type, $path) {

        $this->Log->msg("get object type $type from $path", LOG_DEBUG);

        $obj = $this->_translatePath($type, $path);

        $this->Log->msg("check object '$type' from '$obj'", LOG_DEBUG3);

        $FRes = $this->_doFeatures($type, $obj);
        $this->_resetFeatures();
        if ( is_array($FRes) ) {	// вернуть альтернативный ответ: dir, exists etc...
            switch ( $FRes['Action'] ) {
                case SF_doFeatureAction_ReturnResult :
                    if ( $type == PageObject and is_array($FRes['Data']) ) {
                        foreach ( $FRes['Data'] as $file => $arr ) {
                            unset($FRes['Data'][$file]);
                            $FRes['Data'][preg_replace("|(^.*)\.xml(" . StoreEngine_RevisionDelimiter . ".*$)?|", '\1\2', $file)] = $arr;
                        }
                    }
                    return $FRes['Data'];
                case SF_doFeatureAction_ChangeObjPath :
                    $obj = $FRes['Data']['NewObjPath'];
                    break;
                case SF_doFeatureAction_CreateObjPath :
                    $obj = '';
                    break;
            }
        }

        if ( 
            realpath($obj) === FALSE or
            ! is_readable($obj)
        )
            throw new Exception("object '$path' not found or not readable", 404);

        switch ($type) {
            case PageObject:
                $xml = new DOMDocument('1.0', 'utf-8');
                if ( $xml->load($obj, LIBXML_NOBLANKS) === FALSE )
                    throw new Exception("objects 'pager' is not XML", 500);
                return $xml;
            case ModuleObject:
                if ( preg_match('|^/users/(.+)$|', $path, $obj_add) )
                    $class_name = basename($obj_add[1]);
                else
                    $class_name = 'pager_module_' . basename($path);
                if ( ! class_exists($class_name) )
                    require_once $obj;
                eval('$module = new ' . $class_name . ';');
                return $module;
            case FileObject:
                return file_get_contents($obj);
            default:
                throw new Exception("objects type '$type' is unknown", 503);
        }

    }

    /**
     * сохраняет объект $data_obj с путем $path
     */
    function PutObject($type, $path, $data_obj) {

        $this->Log->msg("write object $path", LOG_DEBUG);

        $obj = $this->_translatePath($type, $path);

        $this->Log->msg("check object '$type' from '$obj'", LOG_DEBUG3);

        $FRes = $this->_doFeatures($type, $obj);
        $this->_resetFeatures();
        if ( is_array($FRes) ) {	// вернуть альтернативный ответ: dir, exists etc...
            switch ( $FRes['Action'] ) {
                case SF_doFeatureAction_ChangeObjPath :
                    $obj = $FRes['Data']['NewObjPath'];
                    break;
                case SF_doFeatureAction_CreateObjPath :
                    $obj = $FRes['Data']['CreateObjPath'];
                    break;
            }
        }

        switch ($type) {
            case PageObject:
                //var_dump($obj, $data_obj);
                if ( ! is_a($data_obj, 'DOMDocument') )
                    throw new Exception("type of this object is not DOMDocument", 500);
                // опция для форматированного сохранения документа
                $data_obj->formatOutput = TRUE;
                if ( $data_obj->save($obj) === FALSE )
                    throw new Exception("unable write the object: $path", 500);
                break;
            case ModuleObject:
                throw new Exception("this object was not be writed", 403);
            case FileObject:
                if ( file_put_contents($obj, $data_obj) === FALSE )
                    throw new Exception("unable write the object: $path", 500);
                break;
            default:
                throw new Exception("objects type '$type' is unknown", 503);
        }
    }

    /**
     * удаляет объект с путем $path
     */
    function DelObject($type, $path) {

        $this->Log->msg("Destroy object $path", LOG_DEBUG);

        $obj = $this->_translatePath($type, $path);

        $this->Log->msg("delete object '$type' at '$obj'", LOG_DEBUG3);

        $FRes = $this->_doFeatures($type, $obj);
        $this->_resetFeatures();
        if ( is_array($FRes) ) {	// вернуть альтернативный ответ: dir, exists etc...
            switch ( $FRes['Action'] ) {
                case SF_doFeatureAction_ChangeObjPath :
                    $obj = $FRes['Data']['NewObjPath'];
                    break;
            }
        }

        switch ($type) {
            case PageObject:
                if ( ! unlink($obj) )
                    throw new Exception("unable delete the object: $path", 500);
                break;
            case ModuleObject:
                throw new Exception("this object was not be deleted", 403);
            case FileObject:
                if ( ! unlink($obj) )
                    throw new Exception("unable delete the object: $path", 500);
                break;
            default:
                throw new Exception("objects type '$type' is unknown", 503);
        }

    }


    /**
     * определяет тип, название и реальный путь к объекту
     */
    private function _translatePath($type, $path) {

        /* для корневой страницы нельзя сделать файл с именем "/", поэтому
         * используем PageRootAlias
         */
        if ( substr($path, -1) == '/' )
            $path .= PM_PageIndex;

        $path = str_replace('/', DIRECTORY_SEPARATOR, $path);

        $this->Log->msg("change the path to: $path", LOG_DEBUG3);

        switch ($type) {
            case PageObject:
                $root = FRoot . PagerFileSource;
                $file = $root . $path . '.xml';
                break;
            case ModuleObject:
                if ( preg_match('|^' . DIRECTORY_SEPARATOR . 'users(/.+)$|', $path, $parr_opt) ) {
                    $root = FRoot . UsersModuleFileSource;
                    $file = $root . $parr_opt[1] . '.inc';
                } else {
                    $root = FRoot . ModuleFileSource;
                    $file = $root . $path . '.inc';
                }
                break;
            case FileObject:
                $root = FRoot . OtherFiles;
                $path = preg_replace(SF_RootHTML, SF_TrueRootHTML, $path);
                $file = $root . $path;
                break;
            default:
                throw new Exception("objects type '$type' is unknown", 503);
        }

        $this->Log->msg("will check '$file' and '$root' for chroot", LOG_DEBUG3);
        
        if ( 
            SF_CheckRealObjectPath
            and realpath(dirname($file)) !== FALSE
            and preg_match('@^' . preg_quote(realpath($root)) . '.*@', realpath(dirname($file))) == 0
        ) {
            throw new Exception("object $path is not in security location", 503);
        }

        return $file;

    }

}

?>
