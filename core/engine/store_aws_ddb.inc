<?php
/**
 * вариант StoreEngine класса для работы с AWS DynamoDB
 *
 * все объекты распределены по типам:
 * PageObject - XML объекты для страниц
 * ModuleObject - объекты-указатели на экземпляры классов модулей
 * FileObject - строчные объекты произвольного содержимого
 *
 */


if ( !defined('AWS_CONFIG_FILE') )
    define('AWS_CONFIG_FILE', FRoot . '.aws_config.php');

if ( !defined('AWS_DDB_FILES_TABLE') )
    define('AWS_DDB_FILES_TABLE', 'files');		// таблица для файлов

require_once 'store_interface.inc';
use Aws\Common\Aws;
require_once 'core/share/vendor/autoload.php';

class store_aws_ddb implements store_interface {

    private $Log;
    private $AWS;
    private $Client;

    function  __construct() {
        $this->Log = logger::GetLogger();
        $this->AWS = Aws::factory(AWS_CONFIG_FILE);
        $this->Client = $this->AWS->get('DynamoDb');
    }


    function GetVersion($feature=null) {

        return 1;

    }
    
    function GetFeatures() {

        return array();

    }

    function OkFeature($feature) {
    }

    /**
     * возвращает объект с путем $path в сиде строки
     */
    function GetObject($type, $path) {

        $this->Log->msg("get object type $type from $path", LOG_DEBUG);

        switch ($type) {
            case PageObject:
                $xml = new DOMDocument('1.0', 'utf-8');
                $xml_root = $xml->appendChild($xml->createElement('root'));
                $xml_root->appendChild($xml->createElement('UseTemplatePage', 'file:/DefaultPage'));
                $xml_PP = $xml_root->appendChild($xml->createElement('Properties'));
/*                $this->SQL->query(sprintf('select Title, DLastMod from articles
                    where Status = \'opened\' 
                    and Link = \'%s\'',
                    $this->SQL->escape_string(substr($path, 1))), __file__, __line__);
                if ( $this->SQL->rows != 1 )
                    throw new Exception("objects '$type/$path' not found", 404);
                if ( ! $this->SQL->fetch() )
                    throw new Exception("objects '$type/$path' unable to read", 401);
*/
                $xml_HtmlHeader = $xml_PP->appendChild($xml->createElement('HTML_Header'));
//                $xml_HtmlHeader->appendChild($xml->createCDATASection(sprintf('<title>%s</title>', $HTML->DeTranslate($this->SQL->row['Title']))));
                return $xml;
            case ModuleObject:
            case FileObject:
/*                $this->SQL->query(sprintf('select Content from articles
                    where Status = \'opened\' 
                    and Link = \'%s\'',
                    $this->SQL->escape_string(substr($path, 1))), __file__, __line__);
                if ( $this->SQL->rows != 1 )
                    throw new Exception("objects '$type/$path' not found", 404);
                if ( ! $this->SQL->fetch() )
                    throw new Exception("objects '$type/$path' unable to read", 401);
                return $HTML->DeTranslate($this->SQL->row['Content']);
*/
                try {
                    
                    $res = $this->Client->getItem(array(
                        'TableName'	=> AWS_DDB_FILES_TABLE,
                        'Key'		=> array(
                            'path' => array(
                                'S'	=> $path
                            ),
                        )
                    ));
                } catch (Exception $e) {
//                    $this->Log->msg("object $type/$path has error: " . $e->getMessage(), LOG_ERR);
                    $this->Log->msg("object $type/$path has error: " . $e->getRequest(), LOG_ERR);
                    throw new Exception("objects '$type/$path' has error", 502);
                }

                if ( $res->count() == 0 )
                    throw new Exception("objects '$type/$path' not found", 404);
                
                return $res->get('Item')['content']['S'];
            default:
                throw new Exception("objects '$type/$path' is unknown", 503);
        }

    }

    /**
     * сохраняет объект $data_obj с путем $path
     */
    function PutObject($type, $path, $data_obj) {

        $this->Log->msg("write object $path", LOG_DEBUG);

        switch ($type) {
            case PagerObject:
            case ModuleObject:
                throw new Exception("this object was not be writed", 403);
            case FileObject:
                throw new Exception("unable write the object", 500);
            default:
                throw new Exception("objects type '$type' is unknown", 503);
        }
    }

    /**
     * удаляет объект с путем $path
     */
    function DelObject($type, $path) {
    }

}

?>
