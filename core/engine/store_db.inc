<?php
/**
 * вариант StoreEngine класса для работы с СУБД MySQL
 *
 * используется коннектор к базе из system/SQL, поэтому ключи для авторизации
 * автоматически ищутся в корне в файле .connect.php
 * пример есть в _connect.php
 *
 * все объекты распределены по типам:
 * PageObject - XML объекты для страниц
 * ModuleObject - объекты-указатели на экземпляры классов модулей
 * FileObject - строчные объекты произвольного содержимого
 * 
 * Для pages запрос должен возвращать следующие поля:
 * 	Template	- указание на шаблон для страницы. Полный путь с источником, типа 'file:/DefaultPage'
 * 	Title		- HTML Title страницы
 *	Keywords	- HTML keywords
 *	Description	- HTML description
 *	DateLastMod	- дата последней модификации
 *	DateExpires	- дата предполагаемого истечения срока актуальности
 * При необходимости можно задать свой шаблон sql-запроса через константу STORE_DB_PAGES_SQL_QUERY. В этом
 * шаблоне должен присутствовать аргумент %s для подстановки пути страницы в sql-запрос. Пример:
 * "select `Template`, `Title`, null as `Keywords`, curdate as `DateLastMod` from `pages` where `Path` = '%s' limit 1"
 * 
 * Для files запрос должен возвращать следующие поля:
 * 	Content - текстовое поле с содержимым файла
 * При неодходимости можно задать свой шаблон sql-запроса через константу STORE_DB_FILES_SQL_QUERY. Вид
 * шаблона аналогичен STORE_DB_PAGES_SQL_QUERY.
 *
 */

if ( !defined('STORE_DB_PAGES_TABLE') )
    define('STORE_DB_PAGES_TABLE', 'pages');		// таблица для pages
if ( !defined('STORE_DB_MODULES_TABLE') )
    define('STORE_DB_MODULES_TABLE', 'modules');	// таблица для modules
if ( !defined('STORE_DB_FILES_TABLE') )	
    define('STORE_DB_FILES_TABLE', 'files');		// таблица для files

require_once 'store_interface.inc';
require_once 'core/system/system_config.inc';

class store_db implements store_interface {

    private $Log;
    private $SQL;

    function  __construct() {
        $this->Log = logger::GetLogger();
        $this->SQL = new SQL;
    }


    function GetVersion($feature=null) {

        return 1;

    }
    
    function GetFeatures() {

        return array();

    }

    function OkFeature($action, $feature=null, $params=array()) {
    }

    /**
     * возвращает объект с путем $path в сиде строки
     */
    function GetObject($type, $path) {

        $HTML = new htmlchars;
        $this->Log->msg("get object type $type from $path", LOG_DEBUG);
        $s_path = $this->SQL->escape_string($path);

        switch ($type) {
            case PageObject:
                if ( defined('STORE_DB_PAGES_SQL_QUERY') ) {
                    $SQuery = sprintf(STORE_DB_PAGES_SQL_QUERY, $s_path);
                } else {
                    $SQuery = sprintf("SELECT `Template`, `Title`, `Keywords`, `Description` ".
                        "`DateExpires`, `DateLastMod` ".
                        "FROM `%s` ".
                        "WHERE `Path` = '%s' "
                        , STORE_DB_PAGES_TABLE, $s_path
                    );
                }
                $this->SQL->query($SQuery, __file__, __line__);
                if ( $this->SQL->rows != 1 )
                    throw new Exception("objects '$type/$path' not found", 404);
                if ( ! $this->SQL->fetch() )
                    throw new Exception("objects '$type/$path' unable to read", 401);
                $xml = new DOMDocument('1.0', 'utf-8');
                $xml_root = $xml->appendChild($xml->createElement('root'));
                if ( ! empty($this->SQL->row['Template']) )
                    $xml_root->appendChild($xml->createElement('UseTemplatePage', $this->SQL->row['Template']));
                $xml_PP = $xml_root->appendChild($xml->createElement('Properties'));

                $xml_HtmlHeader = $xml->createElement('HTML_Header');
                if ( ! empty($this->SQL->row['Title']) )
                    $xml_HtmlHeader->appendChild($xml->createCDATASection(sprintf('<title>%s</title>', $HTML->DeTranslate($this->SQL->row['Title']))));
                if ( ! empty($this->SQL->row['Description']) )
                    $xml_HtmlHeader->appendChild($xml->createCDATASection(sprintf('<meta name="description" content="%s">', $HTML->DeTranslate($this->SQL->row['Description']))));
                if ( ! empty($this->SQL->row['Keywords']) )
                    $xml_HtmlHeader->appendChild($xml->createCDATASection(sprintf('<meta name="keywords" content="%s">', $HTML->DeTranslate($this->SQL->row['Keywords']))));

                $xml_HttpHeader = $xml->createElement('HTTP_Header');
                if ( ! empty($this->SQL->row['DateLastMod']) )
                    $xml_HttpHeader->appendChild($xml->createElement('string', $HTML->DeTranslate($this->SQL->row['DateLastMod'])));
                if ( ! empty($this->SQL->row['DateExpires']) )
                    $xml_HttpHeader->appendChild($xml->createElement('string', $HTML->DeTranslate($this->SQL->row['DateExpires'])));


                if ( $xml_HtmlHeader->hasChildNodes() )
                    $xml_PP->appendChild($xml_HtmlHeader);
                if ( $xml_HttpHeader->hasChildNodes() )
                    $xml_PP->appendChild($xml_HttpHeader);

                return $xml;
            case ModuleObject:
            case FileObject:
                if ( defined('STORE_DB_FILES_SQL_QUERY') ) {
                    $SQuery = sprintf(STORE_DB_FILES_SQL_QUERY, $s_path);
                } else {
                    $SQuery = sprintf("SELECT `Content` FROM `%s` ".
                        "WHERE `Path` = '%s' "
                        , STORE_DB_FILES_TABLE, $s_path
                    );
                }
                $this->SQL->query($SQuery, __file__, __line__);
                if ( $this->SQL->rows != 1 )
                    throw new Exception("objects '$type/$path' not found", 404);
                if ( ! $this->SQL->fetch() )
                    throw new Exception("objects '$type/$path' unable to read", 401);
                return $HTML->DeTranslate($this->SQL->row['Content']);

            default:
                throw new Exception("objects '$type/$path' is unknown", 503);
        }

    }

    /**
     * сохраняет объект $data_obj с путем $path
     */
    function PutObject($type, $path, $data_obj) {

        $this->Log->msg("write object $path", LOG_DEBUG);

        switch ($type) {
            case PagerObject:
            case ModuleObject:
                throw new Exception("this object was not be writed", 403);
            case FileObject:
                throw new Exception("unable write the object", 500);
            default:
                throw new Exception("objects type '$type' is unknown", 503);
        }
    }

    /**
     * удаляет объект с путем $path
     */
    function DelObject($type, $path) {
    }

}

?>
