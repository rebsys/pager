<?php
/**
 * класс PagerConfig
 *
 * Предназначен для хранения, модификации и выдачи конфигурационных
 * статических параметров и текущих динамических.
 * Динамические параметры могут сохраняться в секциях, определение
 * секций задаются константами.
 *
 * ReadOnly_Section - секция, в которой сохраняются параметры без возможности
 * их последующего изменения.
 */

define('ReadOnly_Section', 1);
 
class PagerConfig {

  static private $PagerConfig = null;
  private $ConfigVar;
  private $RuntimeVar;
  private $Log;

  function __construct() {

    $this->ConfigVar = array();
    $this->RuntimeRootVar = array();
    $this->RuntimeVar = array();
    $this->RuntimeVar[ReadOnly_Section] = array();
    $this->Log = logger::GetLogger();

  }

  /*
   * Статический метод для получения текущего
   * экземпляра PagerConfig
   */
  static function GetPagerConfig() {

    if ( self::$PagerConfig == null ) {

      self::$PagerConfig = new PagerConfig();

    }
    
    return self::$PagerConfig;

  }

  /*
   * Инициирует конфигурационный параметр. Если параметр уже установлен,
   * генерирует исключение с кодом 10.
   */
  function InitConfig($var, $val) {
  
    $this->Log->msg("Init config_var '$var' to value '$val'", LOG_DEBUG2);

    if ( array_key_exists($var, $this->ConfigVar) )
      throw new Exception("Parameter '$var' already exists", 10);
  
    $this->ConfigVar[$var] = $val;

  }

  /*
   * Возвращает значение конфигурационного параметра.
   * Если параметр не определен, генерирует исключение с кодом 20
   */
  function GetConfig($var) {

    $this->Log->msg("Request config_var '$var' with value: " . $this->ConfigVar[$var], LOG_DEBUG3);

    if ( ! array_key_exists($var, $this->ConfigVar) )
      throw new Exception("Parameter '$var' not found", 20);
  
    return $this->ConfigVar[$var];

  }

  /*
   * Сохраняет значение параметра $var со значением $val в опционально-
   * задаваемой секции $section.
   *
   * Предопределенные секции:
   *
   * ReadOnly_Section - секция, в которой храняться неизменяемые параметры.
   * При попытке изменения значения существующего в секции ReadOnly параметра,
   * генерируется исключение с кодом 30.
   * 
   */
  function SetVar($var, $val, $section=null) {

    $this->Log->msg("Save PagerConfig var '$var' section '$section' to value '$val'", LOG_DEBUG2);
  
    switch ($section) {
    
      case ReadOnly_Section:
      
        if ( array_key_exists($var, $this->RuntimeVar[ReadOnly_Section]) )
          throw new Exception("Parameter '$var' already exists", 30);
        $this->RuntimeVar[ReadOnly_Section][$var] = $val;
        break;
        
      case null:
        $this->RuntimeRootVar[$var] = $val;
        break;
      
      default:
        $this->RuntimeVar[$section][$var] = $val;
        break;
    
    }
  
  }

  /*
   * Возвращает значение параметра $var из опционально-задаваемой секции $section.
   * Если параметр отсутствует, генерируется исключение с кодом 40.
   */
  function GetVar($var, $section=null) {

    $this->Log->msg("Read PagerConfig var '$var' from section '$section'", LOG_DEBUG2);

    switch ($section) {
    
      case null:
        if ( ! array_key_exists($var, $this->RuntimeRootVar) )
          throw new Exception("Parameter '$var' not found", 40);
        return $this->RuntimeRootVar[$var];
      
      default:
        if ( ! array_key_exists($var, $this->RuntimeVar[$section]) )
          throw new Exception("Parameter '$var' not found", 40);
        return $this->RuntimeVar[$section][$var];
    
    }


  }

}
