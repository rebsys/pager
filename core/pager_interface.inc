<?php

/**
 * интерфейс для создания объектов-страниц
 */
interface pager_interface {

    /**
     * вызывается для инициализации страницы
     *
     * @param void
     * @return void
     *
     */
    function Init();

    /**
     * возвращает уникальный текстовый идентификатор страницы, т.е. ее линк на сайте
     * 
     * @param void
     * @return string
     * 
     */
    function GetPageID();
    
    /**
     * устанавливает уникальный текстовый идентификатор страницы, т.е. ее линк на сайте
     * 
     * @param string $PagePath
     * @return void
     * 
     */
    function SetPageID( $PagePath );


}

?>
