<?php
/**
 * Формирует в поле OutText HTML заголовок страницы.
 *
 * Создает указание на DTD документа из параметра Pager/Properties/Doctype
 * Используйте <![CDATA[....]]> для этого свойства, чтобы избежать проблем со
 * структурой XML документа.
 *
 * Создает корневой таг <html>, добавляя атрибуты непосредственно как строку из
 * параметра страницы Pager/Properties/Html_str
 *
 * Создает корневой таг контента страницы <body>, добавляя атрибуты
 * непосредственно как строку из параметра страницы Pager/Properties/Body_str
 *
 * Формирует таги заголовка <head> HTML-страницы из параметра
 * Pager/Properties/HTML_Header. Будут обработаны все экземпляры свойства
 * Properties/HTML_Header, поэтому возможно указывать их в разных местах,
 * включая шаблоны.
 * Добавляет в заголовок <head> описания CSS, которые берет из файлов, указанных
 * с помощью параметров Properties/HTML_CSS. Добавляет их в порядке указания,
 * но всегда ДО обработки параметра HTML_Header, т.е. с более низким приоритетом.
 *
 * TODO: надо сделать темплэйт XML-описания страницы со всеми параметрами и подробным
 * описанием каждого из них. А еще лучше демо-страницу, которая загрузит все
 * имеющиеся в наличии модули и выведет их название и описания в удобном формате.
 *
 */

class pager_module_html_header_build extends module_core implements module_interface {

  function Run($pager, $module) {
    $xp = new DOMXPath($pager->PageXML);
    $Html_str = $Body_str = '';

    $nodelist = $xp->query("Properties/Doctype");
    if ( $nodelist->length > 0 )
      $DocType = $nodelist->item(0)->nodeValue;
    else
      $DocType = PageDefaultDTD;

    $pager->OutText .= trim($DocType);

    $nodelist = $xp->query("Properties/Html_str");
    if (
      $nodelist->length > 0
      and ! empty( $nodelist->item(0)->nodeValue )
    )
      $Html_str = ' ' . $nodelist->item(0)->nodeValue;

    $pager->OutText .= "<html${Html_str}>";

    $HTML_Header = '';
    $nodelist = $xp->query("Properties/HTML_CSS/*");
    if ($nodelist->length > 0) {
      require_once('core/share/cssmin.php');
      foreach ( $nodelist as $node ) {
        switch ($node->nodeName) {
//          case 'auto_file':
//            $FileSrc = 'auto';
//            break;
          case 'file_src':
            $FileSrc = $node->nodeValue;
            break;
          default:
            throw new Exception('Unknown type of required content: ' . $node->nodeName, 16);
        }
        try {
          $StrFile = $this->se->GetObject('file/' . $node->nodeValue);
        } catch (Exception $e) {
          $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
          throw new Exception('failed to get an object from file_src', 12);
        }
        $HTML_Header .= "<style type='text/css'>" . CssMin::minify($StrFile) . "</style>";
      }
    }

    $nodelist = $xp->query("Properties/HTML_Header");
    if (
      $nodelist->length > 0
      and ! empty( $nodelist->item(0)->nodeValue )
    ) {

      foreach ( $nodelist as $node )
        $HTML_Header .= trim($node->nodeValue);

    }

    $pager->OutText .= '<head>' . $HTML_Header . '</head>';

    $nodelist = $xp->query("Properties/Body_str");
    if (
      $nodelist->length > 0
      and ! empty( $nodelist->item(0)->nodeValue )
    )
      $Body_str = ' ' . $nodelist->item(0)->nodeValue;

    $pager->OutText .= "<body${Body_str}>";

  }

}
?>