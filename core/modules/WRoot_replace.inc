<?php
/*
 * Ищет и изменяет текст уже сформированной части страницы в поле OutText.
 *
 * По всему уже готовому OutText ищем макро @@WRoot@@ и меняет на содержимое
 * константы WRoot, если она определена. Если не определена - выдает лишь
 * предупреждение.
 */

class pager_module_WRoot_replace implements module_interface {

    function Init($mm) {
    }

    function Run($pager, $module) {
        

        if ( ! defined('WRoot') ) {
            $Log = logger::GetLogger();
            $Log->msg("Constant 'WRoot' is not defined", LOG_WARNING);
        }

        $pager->OutText = str_replace('@@WRoot@@', WRoot, $pager->OutText);

    }


}

?>
