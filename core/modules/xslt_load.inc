<?php
/*
 * Загружает и затем импортирует в хвост объекта OutXSL все ноды объекта DOMDocument
 * из содержимого файла с именем из параметра file_src. Если file_src не указан,
 * пытается самостоятельно найти содержимое OutXSL в объекте типа file, c путем совпадающим
 * с текущим PageID и имеющего суфикс .xsl, если определено свойство
 * страницы Properties/Page_Content/auto_file
 *
 * Опциональный параметр ClearOutXSL очищает существующий OutXSL перед загрузкой
 */

class pager_module_xslt_load extends module_core implements module_interface {

    function Run($pager, $module) {

        $this->_init_PageXP($pager);
        if ( $this->PageXP->query("ClearOutXSL", $module)->length > 0 ) {
            $pager->OutXSL = new DOMDocument('1.0', 'UTF-8');
        }

        try {
        
            $XML = $this->GetFileXML($pager, $module, FileAutoXSL_Ext);
            if ( $pager->OutXSL->hasChildNodes() ) 
                foreach ( $XML->documentElement->childNodes as $Node )
                    $pager->OutXSL->documentElement->appendChild($pager->OutXSL->importNode($Node, TRUE));
            else
                $pager->OutXSL->appendChild($pager->OutXSL->importNode($XML->documentElement, TRUE));

        } catch(Exception $e) {
        
            $this->Log->msg("module 'xslt_load' cant load the content, skipped", LOG_WARNING);

        }

    }

}
?>
