<?php
/*
 * формирует в поле OutText верхнюю часть статического шаблона страницы HTML
 *
 * контент может быть задан как (в порядке убывания приоритета):
 * - параметр модуля file_src
 * - параметр страницы Properties/Page_Header/file_src
 * - константа PageDefaultHeader
 */

class pager_module_top_include extends module_core implements module_interface {

    function Run($pager, $module) {

        if ( $StrFile = $this->_load_file_src($pager, $module) ) {

                $pager->OutText .= $StrFile;
                unset($StrFile);

        } elseif ( $StrFile = $this->_load_path_file($pager, "Properties/Page_Header/file_src") ) {

                $pager->OutText .= $StrFile;
                unset($StrFile);

        } elseif ( defined('PageDefaultHeader') ) {

            try {
                $pager->OutText .= $this->se->GetObject('file/' . PageDefaultHeader);
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg("module 'top_include' has a error, skipped", LOG_WARNING);
            }

        } else {

            $this->Log->msg("module 'top_include' cant find the content, skipped", LOG_WARNING);

        }
    }


}
?>
