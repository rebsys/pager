<?php
/*
 * Модуль для поддержки редактора объектов. Модуль предполагается использовать после модуля request_parser,
 * в котором происходит первичная обработка и проверка входных данных. Все данные для этого модуля object4edit
 * рекомендуется брать из результирующей структуры request_parser, обычно это RequestResult, в SrcXML.
 * Все интегрируемые в SrcXML данные импортируются не напрямую в задаваемую параметром ноду, а в ноду-потомок
 * с именем Data. Также, модуль проверяет доступность запрашиваемых объектов, и создает ноду Error с указанием
 * в ней сути проблемы.
 * Если заданная параметром нода в SrcXML, предназначенная для импорта данных из хранилища (или списка ревизий,
 * или другого импорта), не существует - она будет создана модулем, вместе со всем требуемой цепочкой.
 *
 * Модуль может обрабатывать объекты типа XML.
 * Режим включается автоматически, если модуль имеет параметр XML. В этом случае, входные данные для режима WRITE
 * берутся не непосредственно из параметра, указанного параметром object_data_src, а из структуры, соответствующей
 * предопределенной структуре RequestResult модуля request_parser.  Путь к этой структуре указывается в object_data_src.
 * У параметра XML должны быть обязательные потомки: file_xml, file_xsl_in, file_xsl_out.
 * file_xml - указывает xml-файл, содержащий шаблон для создания нового XML объекта.  Этот шаблон используется, когда
 * для редактирования открывается несуществующий (новый) объект в хранилище, и XSLT-трансляция невозможна, в силу
 * отсутствия исходных данных, но структура документа должна быть определена.  По сути, это может быть копия
 * существующего объекта без данных, или просто xml-документ с необходимыми для редактора полями для редактирования.
 * file_xsl_in - указывает xml-файл, содержащий XSL-шаблон для трансляции XML объекта, загруженного из хранилища для
 * редактирования.  Эта трансляция необходима для того, чтобы выделить из редактируемого xml-объекта лишь те данные,
 * которые могут быть подвергнуты редактированию.  Также, эта трансляция используется для разворачивания xml-структур,
 * упакованных внутри xml-объекта в секции CDATA. Результат трансляции помещается как потомок в узел, адрес которого
 * задается параметром object_data_dst режимов READ и EDIT.
 * file_xsl_out - указывает xml-файл, содержащий XSL-шаблон для трансляции входных данных, обычно используется
 * RequestResult, в результате которой получается структура-шаблон для создания или модификации существующего
 * xml-объекта из хранилища.  Таким образом производится сборка необходимых входных данных и упаковка их в нужном виде
 * и формате, для последующей записи в xml-объект.
 * В обоих случаях использования xslt-трансляции должен быть получен DOMElement, содержащий один элемент Data, в
 * котором содержится произвольное количество элементов Node, каждый из которых описывает редактируемый элемент
 * xml-объекта.  Каждый элемент Node содержит: Name, XPath и Value.  Name должен содержать произвольное имя переменной,
 * имеет информационно-указательное значение.  XPath содержит полный путь к узлу xml-документа, соответствующий
 * текущему элементу Node.  Value содержит текущее значение редактируемое элемента xml-документа.  Если узел должен
 * иметь потомков типа CDATA, в результате трансляции такой потомок доллжен быть создан.  Для этого внутри xslt-шаблона
 * может быть использована такая конструкция:
 * <xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>данные для упавки<xsl:text disable-output-escaping="yes">]]&gt;</xsl:text>
 * В процессе сохранения xml-объекта все данные и потомки из Value будут записаны в указанную в XPath ноду объекта.
 * Советы. Не забываем создавая xslt обязательно указывать не только формат вывода, но и чарсет: <xsl:output method="xml" encoding="UTF-8" />
 * Примеры.
 *
 * 1. простая нода
 <Node>
   <Name>title</Name>
   <XPath>/Properties/HTML_Header</XPath>
   <Value><![CDATA[<title>заголовок страницы</title>]]></Value>
 </Node>
 * 2. нода с атрибутами
 <Node>
   <Name>keywords</Name>
   <XPath>/Properties/HTML_Header</XPath>
   <Value><![CDATA[<meta name="keywords" content="cms, content system, editor"/>]]></Value>
 </Node>
 * 
 * Для любых типов объектов модуль поддерживает режимы работы:
 * 1. EDIT - режим редактирования объекта;
 * 2. READ - режим чтения объекта;
 * 3. WRITE - режим записи объекта;
 * 4. DELETE - режим удаления объекта.
 *
 * Каждый режим конфигурируется как одноименная с названием режима нода-потомок для модуля в конфиге страницы.
 * Если режим не сконфигурирован и опущен, значит этот режим не активизируется никогда.
 * Описание каждого режима содержима обязательный параметр
 * validator, содержащий один из возможных валидаторов для request_parser. Наличие этого вадидатора в транзитном
 * массиве страницы $pager->TransitArray['RequestValidator'] является необходимым условием для старта соответствующего
 * валидатору режима работы модуля.
 *
 * EDIT. В режиме редактирования модуль извлекает из заданной параметром object_path_src ноды SrcXML путь к объекту,
 * извлекает из хранилища ревизию DRAFT этого объекта и импортирует ее в заданную параметром object_data_dst
 * ноду SrcXML. Помимо этого, модуль проверяет ниличие доступных для начала редактирования ревизий заданного
 * объекта и сохраняет список ревизий в виде DOMNode с именем Revisions, импортируя его в заданную параметром
 * object_data_dst ноду SrcXML. Доступные для начала редактирования - это все ревизии типа REV[0-9]+, т.е. все
 * ревизии, кроме HEAD и DRAFT.
 * Для удобства, в указанную через object_data_dst ноду с именем ObjectPathHEAD так же импортируется полный путь
 * к HEAD ревизии объекта, который может быть использован для процедуры релиза черновика. Аналогично создается
 * нода ObjectPathDRAFT с путем к ревизии DRAFT объекта.
 * Если ревизия DRAFT объекта не существует, модуль пытается последовательно извлечь и импортировать в заданную
 * параметром object_data_dst ноду SrcXML сначала ревизию HEAD этого объекта, а потом и сам объект без указания
 * какой-либо ревизии. Предполагается, что первая же попытка сохранить изменения в редакторе объекта, приведет
 * к созданию черновика на основе использованной ревизии.
 *
 * READ. В режиме чтения модуль получает путь к объекту в хранилище из ноды в SrcXML, заданной параметром object_path_src.
 * Путь не анализируется на наличие указания ревизий, и запрашивается именно что указано. Полученные из хранилища
 * данные объекта импортируются в указанную с помощью параметра object_data_dst ноду в SrcXML.
 * Для удобства, в указанную через object_data_dst ноду с именем ObjectPathDRAFT так же импортируется полный путь
 * к DRAFT ревизии объекта, который может быть использован для процедуры сохранения ревизии в виде черновика.
 * 
 * WRITE. В режиме записи модуль получает, предпочтительно прошедшие через request_parser, данные объекта из ноды
 * SrcXML, заданной параметром object_data_src. И путь к объекту для сохраннения из ноды в SrcXML, заданной
 * параметром object_path_dst. Если путь для сохранения объекта содержит ревизию HEAD, модуль сохранит текущее
 * содержимое HEAD в виде новой ревизии типа REV, и только потом перезапишет содежимое HEAD новыми данными. Черновик,
 * т.е. ревизия с именем DRAFT, при этом будет удален.
 * Если при записи возникают проблемы с правами доступа, пишет соответствующую ошибку в SrcXML по адресу из
 * предопределенной константы ModuleErrorsSet.
 * 
 * DELETE. В режиме удаления модуль получает из задаваемой параметром object_path_src ноды SrcXML путь к объекту, отсекает
 * возможные указания ревизии, и удаляет ревизию DRAFT объекта.
 * 
 * Предполагаемый алгоритм использования:
 * 1. с помощью request_parser задается объект для редактирования, путь к объекту передается модулю (валидатор EDIT),
 * который получает или формирует ревизию DRAFT выбранного объекта и создает форму для редактирования объекта;
 * 2. при наличии доступных ревизий, в отдельной форме создается их список: альтернативные версии
 *    объекта - валидатор READ;
 * 3. в форме редактирования создается кнопка для сохранения черновика - валидатор WRITE, или подключается AJAX;
 * 4. в форме редактирования создается кнопка для сохранения черновика вместо головной ревизии HEAD объекта
 *    - валидатор WRITE;
 * 5. в отдельной форме с путем к объекту создается кнопка для удаления черновика - валидатор DELETE;
 * 6. в форме просмотра ревизий объекта, созданной с использованием валидатора READ, для сохранения выбранной ревизии
 *    как черновика используется валидатор типа WRITE, но крайне рекомендуется выдавать предупреждение о замене существующей
 *    ревизии черновика.
 * 
 */

include_once '_share_object_core.inc';

class pager_module_get_object4edit extends _share_object_core implements module_interface {

  private $XML_XSLT_IN = FALSE;
  private $XML_XSLT_OUT = FALSE;

  /*
   * пытается получить объект $object_path из хранилища
   * возвращает содержимое объекта в виде секции CDATA ноды Data от SrcXML,
   * или NULL в случае неудачи
   * если опциональный параметр $simple равен TRUE, просто возвращает полученный объект
   */
  protected function _get_object($object_path, $simple=FALSE) {
        try {
            $ObjData = $this->se->GetObject(
              $object_path,
              array(
                'Auth' => $this->AuthArr,
              )
            );
            // если работаем с XML, но объект получен как строка - преобразуем
            if (
              $this->XML_XSLT_IN
              and ! is_a($ObjData, 'DOMDocument') 
            ) {
              $xml = new DOMDocument('1.0', 'UTF-8');
              $xml->loadXML($ObjData);
              $ObjData = $xml;
            }
            // если запросили простую выдачу - сразу выдаем объект
            if ( $simple )
              return $ObjData;
            // если работаем с XML, преобразуем объект согласно шаблона $this->XML_XSLT_IN
            if ( $this->XML_XSLT_IN ) {
              $ObjectNode = $this->pager->SrcXML->importNode(
                $this->_xml_translate($this->XML_XSLT_IN, $ObjData)->getElementsByTagName('Data')->item(0),
                TRUE
              );
            } else {
              $ObjectNode = $this->pager->SrcXML->createElement('Data');
              $ObjectNode->appendChild($this->pager->SrcXML->createCDATASection($ObjData));
            }
            return $ObjectNode;
        } catch (Exception $e) {
            if ( $e->getcode() != 404 ) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                throw new Exception('failed to get an object from ' . $object_path, 12);
            }
        }
  }

  /*
   * в режиме работы с XML, подготавливает XML-документ для сохранения
   */
  private function _xml_object_prepare($object_path, $object_data_src) {

        // получим шаблон для сохранения данных,
        // возможно, вместе со старыми данными, если они есть
        $base_obj_path = $this->_trunk_revision($object_path);
        foreach ( array(
          $object_path,
          $base_obj_path . StoreEngine_RevisionDelimiter . 'HEAD',
          $base_obj_path,
          'file/' . $this->_check_param('XML/file_xml', $this->module)
          ) as $obj_path
        ) {
          $obj_xml = $this->_get_object($obj_path, TRUE);
          if ( $obj_xml != NULL ) {
            break;
          }
        }
        $obj_xp = new DOMXPath($obj_xml);

        // получим структуру готовых для сохранения данных
        $ods = new DOMDocument('1.0', 'UTF-8');
        //$ods->loadXML($this->pager->SrcXML->saveXML($this->SrcXP->query($object_data_src)->item(0)));
        $ods->appendChild($ods->importNode($this->SrcXP->query($object_data_src)->item(0), TRUE));
        $obj_data = $this->_xml_translate($this->XML_XSLT_OUT, $ods);
        $obj_data->encoding = 'UTF-8';

        // модифицируем узлы шаблона согласно структуре данных
        foreach ( $obj_data->getElementsByTagName('Node') as $ObjItem ) {
          // определим ноду для внесения данных
          $node = $this->_create_xml_path($obj_xp, $ObjItem->getElementsByTagName('XPath')->item(0)->nodeValue);
          $ObjItemNL = $ObjItem->getElementsByTagName('Value');
          $node->nodeValue = '';
          if ( $ObjItemNL->length > 0 ) {
            if ( $ObjItemNL->item(0)->hasChildNodes() ) {
              $node->removeChild($node->firstChild);
              foreach ( $ObjItemNL->item(0)->childNodes as $cnodes )
                $node->appendChild($obj_xml->importNode($cnodes, TRUE));
            } else {
              $node->nodeValue = $ObjItemNL->item(0)->nodeValue;
            }
          }
        }

        // вернем модифицированный xml с новыми данными
        return $obj_xml;

  }
  
  /*
   * проверяет наличие опционального параметра XML, при его наличии создает в $this->XML_XSLT_IN
   * объект типа XSLTProcessor в который загружает указанный в XML/file_xsl XML-файл. Этот
   * XSLTProcessor используется для последующей трансляции XML-объектов, загружаемых из хранилища
   */
  private function _check_xml_param() {
      if (
        ! $this->XML_XSLT_IN 
        and $this->PageXP->query('XML/file_xsl_in', $this->module)->length > 0 
        and $this->PageXP->query('XML/file_xsl_out', $this->module)->length > 0 
      ) {
          $xml = new DOMDocument('1.0', 'UTF-8');
          $xml->loadXML($this->_load_path_file($this->pager, 'XML/file_xsl_in', $this->module), LIBXML_NOBLANKS);
          $this->XML_XSLT_IN = new XSLTProcessor();
          $this->XML_XSLT_IN->importStylesheet($xml);
          $xml->loadXML($this->_load_path_file($this->pager, 'XML/file_xsl_out', $this->module), LIBXML_NOBLANKS);
          $this->XML_XSLT_OUT = new XSLTProcessor();
          $this->XML_XSLT_OUT->importStylesheet($xml);
      } else {
          $this->XML_XSLT_IN = $this->XML_XSLT_OUT = FALSE;
      }
  }

  function Run($pager, $module) {

        parent::Run($pager, $module);
        
        // только для реализованных валидаторов
        foreach ( $this->pager->TransitArray['RequestValidator'] as $validator => $valdata) {
        
          if ( isset($valdata['Error']) ) // если были ошибки - пропускаем
            continue;

          // только для режимов с этим валидатором
          foreach ( $this->PageXP->query("*[validator='$validator']", $module) as $MethodNode ) {

            switch ( $MethodNode->nodeName ) {
              case 'EDIT':
/***************
***** EDIT *****
***************/
                # object_path_src, object_data_dst, object_revision_dst
                // проверим тип объекта
                $this->_check_xml_param();
                // получим путь к объекту без указания ревизии
                $ObjectPath = $this->_trunk_revision($this->_check_param('object_path_src', $MethodNode, TRUE));
                // создадим ноду для объекта
                $ObjectDataN = $this->_create_xml_path($this->SrcXP, $this->_check_param('object_data_dst', $MethodNode));
                // проверим доступность
                $ObjectCheck = $this->_check_object($ObjectPath);
                if ( $ObjectCheck != NULL ) {
                  $ObjectDataN->appendChild($ObjectCheck);
                  break;
                }
                // спросим ревизии для объекта
                $RevN = $this->_get_revision($ObjectPath);
                if ( $RevN !== NULL )
                  $ObjectDataN->appendChild($RevN);
                // создадим пути для DRAFT и HEAD ревизий объекта
                $ObjectDataN->appendChild($this->pager->SrcXML->createElement('ObjectPathHEAD', $ObjectPath . StoreEngine_RevisionDelimiter . 'HEAD'));
                $ObjectDataN->appendChild($this->pager->SrcXML->createElement('ObjectPathDRAFT', $ObjectPath . StoreEngine_RevisionDelimiter . 'DRAFT'));
                // последовательно пробуем получить черновик или кандидата на него
                foreach ( array(StoreEngine_RevisionDelimiter . 'DRAFT', StoreEngine_RevisionDelimiter . 'HEAD', '') as $ObjectPathAddon ) {
                  $ObjectData = $this->_get_object($ObjectPath . $ObjectPathAddon);
                  if ( $ObjectData != NULL ) {
                    $ObjectDataN->appendChild($ObjectData);
                    break;
                  }
                }
                break;
              case 'READ':
/***************
***** READ *****
***************/
                # object_path_src, object_data_dst
                // проверим тип объекта
                $this->_check_xml_param();
                $this->_check_param('object_path_src', $MethodNode, TRUE);
                $ObjectPath = $this->_check_param('object_path_src', $MethodNode, TRUE);
                $ObjectData = $this->_get_object($ObjectPath);
                $ObjectDataN = $this->_create_xml_path($this->SrcXP, $this->_check_param('object_data_dst', $MethodNode));
                // спросим ревизии для объекта
                $RevN = $this->_get_revision($ObjectPath);
                if ( $RevN !== NULL )
                // создадим путь для DRAFT ревизии объекта
                $ObjectDataN->appendChild($this->pager->SrcXML->createElement('ObjectPathDRAFT', $this->_trunk_revision($ObjectPath) . StoreEngine_RevisionDelimiter . 'DRAFT'));
                // получим объект
                  $ObjectDataN->appendChild($RevN);
                if ( $ObjectData != NULL )
                  $ObjectDataN->appendChild($ObjectData);
                break;
              case 'WRITE':
/****************
***** WRITE *****
****************/
                # object_data_src, object_path_dst
                // проверим тип объекта
                $this->_check_xml_param();
                $ObjectData = $this->_check_param('object_data_src', $MethodNode, TRUE);
                $ObjectPath = $this->_check_param('object_path_dst', $MethodNode, TRUE);
                // если сохраняется ревизия HEAD, текущий HEAD сохраним как ревизию REV
                if ( preg_match('%' . StoreEngine_RevisionDelimiter . 'HEAD$%', $ObjectPath) ) {
                  $err = $this->_create_revision($ObjectPath);
                  if ( $err != NULL ) {
                    $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
                    $ObjectDataN->appendChild($err);
                  }
                }
                if ( $this->XML_XSLT_OUT ) {
                  $ObjectData = $this->_xml_object_prepare($ObjectPath, $this->_check_param('object_data_src', $MethodNode));
                }
                $err = $this->_put_object($ObjectPath, $ObjectData);
                if ( $err != NULL ) {
                  $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
                  $ObjectDataN->appendChild($err);
                }
                break;
              case 'DELETE':
/*****************
***** DELETE *****
*****************/
                # object_path_src
                $this->_del_object(
                  $this->_trunk_revision(
                    $this->_check_param('object_path_src', $MethodNode, TRUE) . StoreEngine_RevisionDelimiter . 'DRAFT'
                  )
                );
                break;
              default:
/******************
***** UNKNOWN *****
******************/
                throw new Exception('unknown method: ' . $MethodNode->nodeName, 16);
            }


          }
        }
        
  }

}

?>
