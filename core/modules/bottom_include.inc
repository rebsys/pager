<?php
/*
 * формирует в поле OutText нижнюю часть статического шаблона страницы HTML
 *
 * контент может быть задан как (в порядке убывания приоритета):
 * - параметр модуля file_src
 * - параметр страницы Properties/Page_Footer/file_src
 * - константа PageDefaultFooter
 *
 */

class pager_module_bottom_include extends module_core implements module_interface {

    function Run($pager, $module) {

        if ( $StrFile = $this->_load_file_src($pager, $module) ) {

                $pager->OutText .= $StrFile;
                unset($StrFile);

        } elseif ( $StrFile = $this->_load_path_file($pager, "Properties/Page_Footer/file_src") ) {

                $pager->OutText .= $StrFile;
                unset($StrFile);

        } elseif ( defined('PageDefaultFooter') ) {

            try {
                $pager->OutText .= $this->se->GetObject('file/' . PageDefaultFooter);
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg("module 'bottom_include' has a error, skipped", LOG_WARNING);
            }

        } else {

            $this->Log->msg("module 'bottom_include' cant find the content, skipped", LOG_WARNING);

        }
    }


}
?>
