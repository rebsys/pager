<?php
/*
 * Модуль для добавления объектов типа file, из группы модулей для манипуляции объектами.
 * Требует для работы общие компоненты из _share_object_core.inc
 * 
 * Создает объект по подготовленным request_parser данным
 * При успехе создает в ModuleReportSet отчет с урлом новой страницы
 * Ошибки пишет в ModuleErrorsSet
 *
 * Параметры:
 * activator - XPath для SrcXML, должен быть валидным (существовать) для разрешения работы модуля
 * remove_xpath (опциональный) - XPath для SrcXML, удаляет указанную ноду при успешном завершении
 * object_home_path - префикс создаваемой страницы, т.е. ее домашний каталог в хранилище (можно указать и тип хранилища)
 * object_name - XPath для SrcXML, где лежит имя создаваемой страницы. object_home_path+object_name составят URL новой страницы
 */

include_once '_share_object_core.inc';

class pager_module_add_object_file extends _share_object_core implements module_interface {


  function Run($pager, $module) {

        parent::Run($pager, $module);

        // только для реализованных валидаторов
        if ( ! isset($pager->TransitArray['RequestDone']) )
            return;
        else
            $ReqResult = $pager->TransitArray['RequestDone'];

        // только для установленных активаторов
        if ( $this->SrcXP->query($this->_check_param('activator', $module))->length == 0 )
          return;

        $ObjectName = $this->_check_param('object_name', $module, TRUE);
        $ObjectPath = preg_replace('|/{2,}|', '/',  $this->_check_param('object_home_path', $module) . '/' . $ObjectName);
        $ObjectFile = 'file/' . $ObjectPath . '.html';
        $ObjectURL =  preg_replace('|^[a-z]+:/|', '/',  $ObjectPath);

        if ( $this->_check_object($ObjectFile) === NULL ) {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
          $ObjectDataN->appendChild($this->_create_error(2, "контент для страницы с именем $ObjectName уже существует"));
          return;
        }
        $err = $this->_put_object($ObjectFile, "This is $ObjectURL");
        if ( $err != NULL ) {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
          $ObjectDataN->appendChild($err);
        } else {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleReportSet);
          $Report = $ObjectDataN->appendChild($pager->SrcXML->createElement('Report'));
          $Report->appendChild($pager->SrcXML->createElement('Text', 'Пустой контент новой страницы создан успешно.'));
          try {
            $ActivatorN = $this->SrcXP->query($this->_check_param('remove_xpath', $module));
            if ( $ActivatorN->length > 0 )
              $ActivatorN->item(0)->parentNode->removeChild($ActivatorN->item(0));
          } catch(Exception $e) {}
        }
        
  }

}

?>
