<?php
/*
 * формирует в поле OutText HTML заголовок страницы
 * шаблон берет из собственного параметра file_src
 */

class pager_module_html_header_include implements module_interface {

    private $se;
    private $Log;

    function Init($mm) {
        $this->se = $mm->StoreEngine;
        $this->Log = logger::GetLogger();
    }

    function Run($pager, $module) {
        $xp = new DOMXPath($pager->PageXML);

        if ( $xp->query("Modules/module[@name='html_header_include']/file_src")->length > 0 ) {
            try {
                $pager->OutText .= $this->se->GetObject('file/' . $xp->query("Modules/module[@name='html_header_include']/file_src")->item(0)->nodeValue);
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg("module 'html_header_include' has a error, skipped", LOG_WARNING);
            }
        }
    }


}
?>
