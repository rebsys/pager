<?php
/*
 * Довавляет строку в HTML заголовок страницы в поле OutText
 * Строку берет из собственных параметрах модуля с название AddString
 *
 *  * !!! НЕ ИСПОЛЬЗОВАТЬ!!!
 * Вместо него используем html_header_change и замещаем закрывающий таг
 * html-заголовка нужными добавлениями + закрывающий таг html-заголовка.
 *
 * Пример:
 *       <module name="html_header_change">
 *           <SearchString>/head></SearchString>
 *           <ReplaceString>added_tag option="something">&lt;/head></ReplaceString>
 *       </module>
 *
 * При необходимости можно таким же образом добавлять таги не в конец заголовка,
 * а в начало или в любую другую часть.
 */

class pager_module_html_header_add implements module_interface {

    function Init($mm) {

    }

    function Run($pager, $module) {

        $SelfName = substr(__CLASS__, 13);
        $Log = logger::GetLogger();
        $Log->msg("deprecated module ${SelfName}", LOG_WARNING);

    }


}

?>
