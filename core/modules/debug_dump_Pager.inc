<?php
/**
 * debug_dump_Pager - это отладочный модуль, который
 * выводит dump объекта pager
 */
class pager_module_debug_dump_Pager implements module_interface {

    function Init($mm) {
    }

    function Run($pager, $module) {

        echo '<p style="text-align:center">', str_repeat('*', 60), '<br>';
        echo str_repeat('*', 60), '<br>';
        echo str_repeat('*', 60), '<br>';
        echo str_repeat('*', 8) , str_repeat(' !!! DEBUG !!! ', 3), str_repeat('*', 8), '<br>';
        echo str_repeat('*', 60), '</p>';
        echo '<pre>', htmlspecialchars(var_export($pager, 1)), '</pre>';
        if ( $pager->PageXML->hasChildNodes() ) {
            echo '<p style="text-align:center">', str_repeat('*', 8) , str_repeat(' !!! PageXML !!! ', 3), str_repeat('*', 8), '</p>', '<br>';
            $pager->PageXML->formatOutput = TRUE;
            echo '<pre>', htmlspecialchars($pager->PageXML->saveXML()), '</pre>';
        }
        if ( $pager->SrcXML->hasChildNodes() ) {
            echo '<p style="text-align:center">', str_repeat('*', 8) , str_repeat(' !!! SrcXML !!! ', 3), str_repeat('*', 8), '</p>', '<br>';
            $pager->SrcXML->formatOutput = TRUE;
            echo '<pre>', htmlspecialchars($pager->SrcXML->saveXML()), '</pre>';
        }
        if ( $pager->OutXSL->hasChildNodes() ) {
            echo '<p style="text-align:center">', str_repeat('*', 8) , str_repeat(' !!! OutXSL !!! ', 3), str_repeat('*', 8), '</p>', '<br>';
            $pager->OutXSL->formatOutput = TRUE;
            echo '<pre>', htmlspecialchars($pager->OutXSL->saveXML()), '</pre>';
        }

    }


}
?>
