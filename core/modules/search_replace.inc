<?php
/*
 * Ищет и изменяет текст уже сформированной части страницы в поле OutText.
 *
 * Ищет строку, соответствующую запросу в параметре модуля SearchString
 * и заменяет на строку из параметра ReplaceString
 *
 * Используйте <![CDATA[....]]> для этоих свойств, чтобы избежать проблем со
 * структурой XML документа.
 * 
 * Если SearchString имеет опцию regex со значением отличным от 0, то
 * ожидается что строка из SearchString имеет формат REGEX, и для поиска
 * совпадений используется именно REGEX. В противном случае используется полное
 * совпадение шаблона фрагменту текста.
 *
 * В случае использования REGEX возможно ограничить кол-во производимых замен.
 * Для этого можно использовать параметр Count. По умолчанию этот параметр
 * равен единице: Count = 1
 */

class pager_module_search_replace implements module_interface {

    function Init($mm) {

    }

    function Run($pager, $module) {
        
        $xp = new DOMXPath($pager->PageXML);

        $nodelist_s = $xp->query("SearchString", $module);
        if ( $nodelist_s->length > 0 ) {
            $SearchStr = $nodelist_s->item(0)->nodeValue;
        } else {
            $Log = logger::GetLogger();
            $Log->msg("mandatory parameter 'SearchString' not found in module ${SelfName}", LOG_WARNING);
        }

        $nodelist_r = $xp->query("ReplaceString", $module);
        if ( $nodelist_r->length > 0 ) {
            $ReplaceStr = $nodelist_r->item(0)->nodeValue;
        } else {
            $Log = logger::GetLogger();
            $Log->msg("mandatory parameter 'ReplaceString' not found in module ${SelfName}", LOG_WARNING);
        }

        $nodelist = $xp->query("Count", $module);
        if ( $nodelist->length > 0 ) {
            $count = intval( $nodelist->item(0)->nodeValue );
        } else {
            $count = 1;
        }

        if (
            $nodelist_s->item(0)->getAttribute('regex') != '0'
            and $nodelist_s->item(0)->getAttribute('regex') != ''
        ) {

            $pager->OutText = preg_replace($SearchStr, $ReplaceStr, $pager->OutText, $count);

        } else {

            $pager->OutText = str_replace($SearchStr, $ReplaceStr, $pager->OutText);

        }

    }


}

?>
