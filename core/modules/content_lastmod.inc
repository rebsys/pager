<?php
/*
 * Использует опциональный параметр object_path для задания полного пути к
 * объекту, по времени модификации которого задаст HTTP-заголовок
 * LastModified с помощью вызова header.
 * Если параметр не задан, проверяет наличие
 * $pager->TransitArray['content_path'] и если находит, использует его.
 */

class pager_module_content_lastmod extends module_core implements module_interface {

    function Run($pager, $module) {

        $this->_init_PageXP($pager);
        $ObjPathNL = $this->PageXP->query("object_path", $module);
        if ( $ObjPathNL->length > 0 ) {
        
            $ObjPath = $ObjPathNL->item(0)->nodeValue;

        } elseif ( isset($pager->TransitArray['content_path']) ) {
            
            $ObjPath = $pager->TransitArray['content_path'];
        }

        if ( isset($ObjPath) ) {
            
            try {
                $ObjList = $this->se->GetObject(
                  $ObjPath,
                  array(
                    'Auth' => $this->AuthArr,
                    'Feature' => array(
                      'Name' => SEF_setList,
                    )
                  )
                );
                $ObjListCur = current($ObjList);
                header('Last-Modified:' . gmdate('D, d M Y H:i:s', $ObjListCur['time_c']) . ' GMT');
            } catch (Exception $e) {
            }
        }

    }


}
?>
