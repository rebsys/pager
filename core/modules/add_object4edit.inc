<?php
/*
 * Модуль для добавления объектов в список редактирования
 * 
 * Добавляет/редактирует ссылку на объект в список объектов для редактирования. Этот список может содержать несколько
 * вложенных списков объектов, но структура у всех должна быть одинаковая:
 * Object
 *   Name - визуальное название страницы, для построения списков доступных страниц
 *   File - путь (может содержать указание типа хранилища) на объект типа file с контентом страницы
 *   Page - путь (может содержать указание типа хранилища) на объект типа pager самой страницы
 * 
 * Параметры:
 *
 * activator - XPath для SrcXML, должен быть валидным (существовать) для разрешения работы модуля
 * remove_xpath (опциональный) - XPath для SrcXML, удаляет указанную ноду при успешном завершении
 * object_page_home_path - префикс создаваемой страницы, т.е. ее домашний каталог в хранилище (можно указать и тип хранилища)
 * object_file_home_path - префикс для контента создаваемой страницы, т.е. его домашний каталог в хранилище (можно указать и тип хранилища)
 * object_name - XPath для SrcXML, где лежит имя создаваемой страницы. object_home_path+object_name составят URL новой страницы
 * activator_file - XPath для SrcXML, должен быть валидным (существовать) для внесения информации о файле в описание страницы
 * activator_page - XPath для SrcXML, должен быть валидным (существовать) для внесения информации о странице в описание страницы
 * object_list_file - путь к xml-файлу, в котором храняться записи о доступных для редактирования объектах
 * object_list_xpath - XPath в структуре object_list_file, указывающий на узел, в который будут добавляться новые записи
 * object_title - XPath для SrcXML, где лежит название создаваемой страницы для object_list_file
 * 
 */

include_once '_share_object_core.inc';

class pager_module_add_object4edit extends _share_object_core implements module_interface {


  function Run($pager, $module) {

        parent::Run($pager, $module);

        // только для реализованных валидаторов
        if ( ! isset($pager->TransitArray['RequestDone']) )
            return;
        else
            $ReqResult = $pager->TransitArray['RequestDone'];

        // только для установленных активаторов
        if ( $this->SrcXP->query($this->_check_param('activator', $module))->length == 0 )
          return;

        $ObjectName = $this->_check_param('object_name', $module, TRUE);
        $ObjectTitle = $this->_check_param('object_title', $module, TRUE);
        $ObjectFile = 'file/' . preg_replace('|/{2,}|', '/',  $this->_check_param('object_file_home_path', $module) . '/' . $ObjectName) . '.html';
        $ObjectPage = 'pager/' . preg_replace('|/{2,}|', '/',  $this->_check_param('object_page_home_path', $module) . '/' . $ObjectName);

        $ObjectListPath = 'file/' . $this->_check_param('object_list_file', $module);
        $ObjectListXML = new DOMDocument('1.0', 'UTF-8');
        $ObjectListXML->preserveWhiteSpace = FALSE;
        $ObjectListXML->loadXML($this->_load_path_file($pager, 'object_list_file', $module));
        $ObjectListXP = new DOMXPath($ObjectListXML);
        $ObjectListHome = $this->_check_param('object_list_xpath', $module);
        $ObjectListNL = $ObjectListXP->query($ObjectListHome);
        if ( $ObjectListNL->length == 0 )
                throw new Exception('XPath from "object_list_xpath" not found in XML from "object_list_file"', 12);

        $NewObjectSave = FALSE;
        $NewObjectNL = $ObjectListXP->query("Object[Name='$ObjectTitle']", $ObjectListNL->item(0));
        if ( $NewObjectNL->length > 0 )
          $NewObjectNode = $NewObjectNL->item(0);
        else
          $NewObjectNode = $ObjectListNL->item(0)->appendChild($ObjectListXML->createElement('Object'));

        $NewObjectNode->appendChild($ObjectListXML->createElement('Name', $ObjectTitle));
        if ($this->SrcXP->query($this->_check_param('activator_file', $module))->length > 0 ) {
          if ($NewObjectNode->getElementsByTagName('File')->length == 0) {
            $NewObjectNode->appendChild($ObjectListXML->createElement('File', $ObjectFile));
            $NewObjectSave = TRUE;
          } else {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
            $ObjectDataN->appendChild($this->_create_error(2, "для страницы с названием '$ObjectTitle' уже определен контент"));
          }
        }
        if ($this->SrcXP->query($this->_check_param('activator_page', $module))->length > 0 ) {
          if ( $NewObjectNode->getElementsByTagName('Page')->length == 0) {
            $NewObjectNode->appendChild($ObjectListXML->createElement('Page', $ObjectPage));
            $NewObjectSave = TRUE;
          } else {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
            $ObjectDataN->appendChild($this->_create_error(2, "для страницы с названием '$ObjectTitle' уже определено описание"));
          }
        }
        if ( $NewObjectSave ) {

          $ObjectListXML->formatOutput = TRUE;
          $err = $this->_put_object($ObjectListPath, $ObjectListXML->saveXML());
          if ( $err != NULL ) {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
            $ObjectDataN->appendChild($err);
          } else {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleReportSet);
            $Report = $ObjectDataN->appendChild($pager->SrcXML->createElement('Report'));
            $Report->appendChild($pager->SrcXML->createElement('Text', 'Данные по новой странице успешно внесены в список страниц'));
            try {
              $ActivatorN = $this->SrcXP->query($this->_check_param('remove_xpath', $module));
              if ( $ActivatorN->length > 0 )
                $ActivatorN->item(0)->parentNode->removeChild($ActivatorN->item(0));
            } catch(Exception $e) {}
          }
          
        }

  }

}

?>
