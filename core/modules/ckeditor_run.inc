<?php
/**
 * Выполняет подстановку html-код редактора ckeditor в уже сформированный
 * документ в $pager->OutText на место шаблона
 * <InsertEditorHere></InsertEditorHere>
 *
 * Документ для загрузки в редактор берет из $pager->TransitArray['Object4Edit']
 *
 * Передает результат работы редактора в POST['EditorData']
 *
 */
class pager_module_ckeditor_run implements module_interface {

    /**
     * запускается для инициализации после создания экземпляра класса
     *
     * @param modman_class $mm - ссылка на ModMan служит для доступа к
     * StoreEngine, ModCount и возможны другие общие ресурсы
     * @return void
     */
    function Init($mm) {

    }

    /**
     * запускается для обработки страницы модулем
     *
     * @param pager_class $pager - ссылка на текущую страницу
     * @param DOMNode $module - на DOMElement, описывающий текущий модуль
     * @return void
     */
    function Run($pager, $module) {

        if ( isset($pager->TransitArray['Object4Edit'])/* and
                $pager->TransitArray['Object4Edit'] != ''*/
        ) {

            $pager->OutText = str_replace(
                '<InsertEditorHere></InsertEditorHere>',
                '<script src="' . WRoot .'ckeditor/ckeditor.js"></script><textarea name="EditorData">' .
                $pager->TransitArray['Object4Edit']
                . '</textarea><script>CKEDITOR.replace("EditorData");</script>',
                $pager->OutText
            );

        }

    }

}

?>
