<?php
/*
 * загружает в поле SrcXML объект DOMDocument из содержимого файла с именем из
 * параметра file_src. Если file_src не указан, пытается самостоятельно найти
 * содержимое SrcXML в объекте типа file, c путем совпадающим
 * с текущим PageID и имеющего суфикс .xml, если определено свойство
 * страницы Properties/Page_Content/auto_file
 */

class pager_module_xmlt_load extends module_core implements module_interface {

    function Run($pager, $module) {
        
        try {
        
            $XML = $this->GetFileXML($pager, $module, FileAutoXML_Ext);
            foreach ( $XML->childNodes as $Node )
                $pager->SrcXML->appendChild($pager->SrcXML->importNode($Node, TRUE));

        } catch(Exception $e) {

            $this->Log->msg("module 'xmlt_load' cant load the content, skipped", LOG_WARNING);

        }

    }


}
?>
