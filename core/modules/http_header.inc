<?php
/*
 * Устанавливает все необходимые странице заголовки HTTP ответа сервера.
 *
 * Параметры берет из Pager/Properties/HTTP_Header/string и использует всю строку
 * непосредственно как строку заголовка HTTP протокола
 * Для установки нескольких параметров нужно использовать несколько строк. По
 * одному параметру в каждой строке.
 */

class pager_module_http_header implements module_interface {

    function Init($mm) {
        
    }

    function Run($pager, $module) {
        $xp = new DOMXPath($pager->PageXML);
        foreach ( $xp->query("Properties/HTTP_Header/string") as $node ) {
            header($node->nodeValue);
        }
    }


}

?>
