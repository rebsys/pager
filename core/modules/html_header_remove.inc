<?php
/*
 * Ищет и удаляет строку в HTML заголовок страницы в поле OutText
 *
 * Строку берет из собственных параметрах модуля с название RemoveString
 * Если строка из SearchString имеет формат REGEX, то для поиска совпадений
 * используется именно REGEX
 *
 * !!! НЕ ИСПОЛЬЗОВАТЬ!!!
 * Вместо него используем html_header_change с пустым параметром ReplaceString
 *
 */

class pager_module_html_header_remove implements module_interface {

    function Init($mm) {

    }

    function Run($pager, $module) {

        $SelfName = substr(__CLASS__, 13);
        $Log = logger::GetLogger();
        $Log->msg("deprecated module ${SelfName}", LOG_WARNING);

    }


}

?>
