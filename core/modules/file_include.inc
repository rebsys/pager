<?php
/*
 * добавляет в поле OutText содержимое файла с именем из параметра file_src.
 * TODO: file_src может содержат опциональный атрибут EngineType, в котором можно
 * явно указать тип используемого хранилища.
 */

class pager_module_file_include extends module_core implements module_interface {

    function Run($pager, $module) {

        $ContentFile = $this->LoadFile($pager, $module, '.html');
        
        if ( ! empty($ContentFile) ) {
            $pager->OutText .= $ContentFile;
            unset($ContentFile);

        } else {

            $this->Log->msg("module 'file_include' cant find the content, skipped", LOG_WARNING);

        }
    }


}
?>
