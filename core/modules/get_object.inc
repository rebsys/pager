<?php
/*
 * Читает объект и записывает в SrcXML в секцию CData.
 *
 * Запрашиваетв хранилище  объект, с путем, задаваемым в SrcXML через параметр модуля
 * object_path_src, и записывает этот объект в SrcXML в таг из параметра
 * object_path_dst в секцию CData. При отсутствии описанной в object_path_dst
 * структуры - создает ее принудительно.
 *
 * опциональный параметр mandatory позволяет выдавать или не выдавать
 * сообщение об ошибке, при отсутствии в SrcXML пути объекта, заданного
 * с помощью object_path_src
 */

class pager_module_get_object extends module_core implements module_interface {

  function Run($pager, $module) {

        $this->_init_PageXP($pager);
        
        $Mandatory = FALSE;
        if ( $this->PageXP->query('mandatory', $module)->length > 0 )
          $Mandatory = TRUE;
        
        $SrcNList = $this->PageXP->query('object_path_src', $module);
        $DstNList = $this->PageXP->query('object_path_dst', $module);

        if ( $SrcNList->length == 0 )
            throw new Exception('parameter "object_path_src" is not specified', 16);
        if ( $DstNList->length == 0 )
            throw new Exception('parameter "object_path_dst" is not specified', 16);

        $SrcXP = new DOMXPath($pager->SrcXML);
        $ObjPathNL = $SrcXP->query($SrcNList->item(0)->nodeValue);
        if ( $ObjPathNL->length == 0 ) {
            if ( $Mandatory )
              throw new Exception('Xpath from parameter "object_path_src=' . $SrcNList->item(0)->nodeValue . '" not found', 12);
            return;
        }
        $ObjPath = $ObjPathNL->item(0)->nodeValue;
        $DstN = $this->_create_xml_path($SrcXP, $DstNList->item(0)->nodeValue);

        try {
            $ObjFile = $this->se->GetObject(
              $ObjPath,
              array(
                'Auth' => array(
                  'User' => 'noname',
                  'Cred' => '',
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to get an object from "object_path_src"=' . $ObjPath, 12);
        }

        $DstN->appendChild($pager->SrcXML->createCDATASection($ObjFile));

  }


}

?>
