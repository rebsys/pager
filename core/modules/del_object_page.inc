<?php
/*
 * Модуль для удаления объектов типа page, из группы модулей для манипуляции объектами.
 * Требует для работы общие компоненты из _share_object_core.inc
 * 
 * Создает объект по подготовленным request_parser данным
 * При успехе создает в ModuleReportSet отчет с урлом новой страницы
 * Ошибки пишет в ModuleErrorsSet
 *
 * Параметры:
 * activator - XPath для SrcXML, должен быть валидным (существовать) для разрешения работы модуля
 * remove_xpath (опциональный) - XPath для SrcXML, удаляет указанную ноду при успешном завершении
 * object_list_file - путь к xml-файлу, в котором храняться записи о доступных для редактирования объектах
 * object_list_xpath - XPath в структуре object_list_file, указывающий на узел, в котором будут удаляться записи об объектах
 * object_name - XPath для SrcXML, где лежит имя страницы из object_list_file, для которой будут осуществляться манипуляции с объектами
 */

include_once '_share_object_core.inc';

class pager_module_del_object_page extends _share_object_core implements module_interface {


  function Run($pager, $module) {

        parent::Run($pager, $module);

        // только для реализованных валидаторов
        if ( ! isset($pager->TransitArray['RequestDone']) )
            return;
        else
            $ReqResult = $pager->TransitArray['RequestDone'];

        // только для установленных активаторов
        if ( $this->SrcXP->query($this->_check_param('activator', $module))->length == 0 )
          return;

        $ObjectName = $this->_check_param('object_name', $module, TRUE);

        $ObjectListPath = 'file/' . $this->_check_param('object_list_file', $module);
        $ObjectListXML = new DOMDocument('1.0', 'UTF-8');
        $ObjectListXML->preserveWhiteSpace = FALSE;
        $ObjectListXML->loadXML($this->_load_path_file($pager, 'object_list_file', $module));
        $ObjectListXP = new DOMXPath($ObjectListXML);
        $ObjectListHome = $this->_check_param('object_list_xpath', $module);
        $ObjectListNL = $ObjectListXP->query($ObjectListHome);
        if ( $ObjectListNL->length == 0 )
                throw new Exception('XPath from "object_list_xpath" not found in XML from "object_list_file"', 12);
        $ObjectNL = $ObjectListXP->query("Object[Name='$ObjectName']", $ObjectListNL->item(0));
        if ( $ObjectNL->length == 0 )
                throw new Exception('Object with Name from "object_name" not found in XML from "object_list_file"', 12);
        if ( $ObjectNL->length > 1 )
                throw new Exception('Object with Name from "object_name" exists more than once in XML from "object_list_file"', 12);
        $ObjectPageNL = $ObjectNL->item(0)->getElementsByTagName('Page');
        if ( $ObjectPageNL->length == 0 ) {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
          $ObjectDataN->appendChild($this->_create_error(1, "Для страницы с именем '$ObjectName' не определено описание страницы"));
          return;
        }
        $ObjectPage = $ObjectPageNL->item(0)->nodeValue;
        if ( $this->_check_object($ObjectPage) !== NULL ) {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
          $ObjectDataN->appendChild($this->_create_error(1, "страница с именем '$ObjectName' не существует"));
          return;
        }
        $ObjectURL =  preg_replace('|^pager/([a-z]+:)?/|', '/',  $ObjectPage);
        $err = $this->_remove_object($ObjectPage);
        if ( $err != NULL ) {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
          $ObjectDataN->appendChild($err);
        } else {
          $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleReportSet);
          $Report = $ObjectDataN->appendChild($pager->SrcXML->createElement('Report'));
          $Report->appendChild($pager->SrcXML->createElement('Text', "страница '$ObjectURL' успешно удалена и теперь недоступна"));
          try {
            $ActivatorN = $this->SrcXP->query($this->_check_param('remove_xpath', $module));
            if ( $ActivatorN->length > 0 )
              $ActivatorN->item(0)->parentNode->removeChild($ActivatorN->item(0));
          } catch(Exception $e) {}
        }
        
  }

}

?>
