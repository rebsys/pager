<?php
/**
 * debug_printXML_Page - это отладочный модуль, который
 * выводит XML страницы
 */
class pager_module_debug_SrcXML implements module_interface {

    function Init($mm) {
    }

    function Run($pager, $module) {

        echo '<p style="text-align:center">', str_repeat('*', 60), '<br>';
        echo str_repeat('*', 60), '<br>';
        echo str_repeat('*', 60), '<br>';
        echo str_repeat('*', 8) , str_repeat(' !!! DEBUG !!! ', 3), str_repeat('*', 8), '<br>';
        echo str_repeat('*', 8) , str_repeat(' !!! SrcXML !!! ', 3), str_repeat('*', 8), '<br>';
        echo str_repeat('*', 60), '</p>';
        $pager->SrcXML->formatOutput = TRUE;
        echo '<pre>', htmlspecialchars($pager->SrcXML->saveXML()), '</pre>';

    }


}
?>
