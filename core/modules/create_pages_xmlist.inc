<?php
/*
 * Модуль для создания xml-списка страниц
 * 
 * Читает попадающие в условия пути path описания страниц и помещает их общий XML, к
 * которому в дальнейшем применяет XSLT-трансляцию по шаблону из файла, указанного
 * в file_xsl_src, и результат помещает в SrcXML в узел, указанный в xmlist_path_dst.
 * 
 * Опциональный параметр xdebug_path_dst задает узел в SrcXML, куда будут помещены
 * xml-описания всех страниц из path.
 * 
 * В XSLT-шаблоне допускается использование PHP-функций, разрешенных методом
 * XSLTProcessor::registerPHPFunctions(), сейчас это функции:
 * preg_replace, time, strtotime
 * Для использования, в шаблон в таг xsl:stylesheet нужно добавить атрибуты:
 * xmlns:php="http://php.net/xsl"
 * xsl:extension-element-prefixes="php"
 * и еще добавить алиас:
 * <xsl:namespace-alias stylesheet-prefix="php" result-prefix="xsl" />
 * Теперь можно использовать: php:functionString('function_name', args, args,...)
 * Пример: <xsl:if test="php:functionString('strtotime', Page_Created) &lt;= php:functionString('time')">
 * TODO: возможно, стоит разрешить задавать их в параметре модуля, но это может быть опасно
 * 
 * Параметры:
 *  
 * path - задает маску пути, для получения страниц; может быть использован многократно;
 * file_xsl_src - задает файл с xslt-шаблоном для трансляции описаний страниц в результирующий XML;
 * xmlist_path_dst - задает XPath для узла в SrcXML, куда помещает результат трансляции XSLT;
 * xdebug_path_dst - опциональный, задает XPath для узла в SrcXML, куда помещает описания всех страниц;
 * 
 */

class pager_module_create_pages_xmlist extends module_core implements module_interface {

  function Run($pager, $module) {

    try {
      $this->AuthArr['User'] = $this->PaC->GetVar('Auth/UserID', ReadOnly_Section);
    } catch ( Exception $e) {
        $this->AuthArr = array(
            'User' => 'noname',
            'Cred' => '',
        );
    }

    $this->_init_PageXP($pager);
    $SrcXP = new DOMXPath($pager->SrcXML);
    
    $XListNL = $this->PageXP->query('xmlist_path_dst', $module);
    if ( $XListNL->length == 0 )
        throw new Exception('Mandatory parameter \'xmlist_path_dst\' not found', 16);
    $XListDstN = $this->_create_xml_path($SrcXP, $XListNL->item(0)->nodeValue);
    
    $XSLTNL = $this->PageXP->query('file_xsl_src', $module);
    if ( $XSLTNL->length == 0 )
        throw new Exception('Mandatory parameter \'file_xsl_src\' not found', 16);
    $XSL = new DOMDocument('1.0', 'utf-8');
    $XSL->loadXML($this->_load_path_file($pager, 'file_xsl_src', $module));
    $XSLT = new XSLTProcessor();
    $XSLT->registerPHPFunctions(array('preg_replace', 'time', 'strtotime'));
    $XSLT->importStylesheet($XSL);
    
    $AllPagesXML = new DOMDocument('1.0', 'utf-8');
    
    foreach ( $this->PageXP->query("path", $module) as $PathN ) {

        try {
            $PagesList = $this->se->GetObject(
              'pager/' . $PathN->nodeValue,
              array(
                'Auth' => $this->AuthArr,
                'Feature' => array(
                  'Name' => SEF_List,
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to get an pages-list by \'path\' parameter', 12);
        }

        foreach ( $PagesList as $Page=>$PExt ) {

            $PagePath = dirname($PathN->nodeValue) . '/' . $Page;

            try {
                $PageXML = $this->se->GetObject(
                  'pager/' . $PagePath,
                  array(
                    'Auth' => $this->AuthArr,
                  )
                );
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                throw new Exception('failed to get an object from pages-list by \'path\'', 12);
            }
            $CurPageN = $AllPagesXML->createElement('Page');
            $CurPageN->appendChild($AllPagesXML->createElement('URL', preg_replace('|^[a-z]+:/|', '/',  $PagePath)));
            $CurPageXML = $CurPageN->appendChild($AllPagesXML->createElement('XML'));
            $CurPageXML->appendChild($AllPagesXML->importNode($PageXML->documentElement, TRUE));
            $AllPagesXML->appendChild($CurPageN);
        }

      }

    $XDebugNL = $this->PageXP->query('xdebug_path_dst', $module);
    if ( $XDebugNL->length > 0 ) {
        $XDebugDstN = $this->_create_xml_path($SrcXP, $XDebugNL->item(0)->nodeValue);
        foreach ( $AllPagesXML->childNodes as $AllPagesN )
            $XDebugDstN->appendChild($pager->SrcXML->importNode($AllPagesN, TRUE));
    }

      $XListDstN->appendChild($pager->SrcXML->importNode($XSLT->transformToDoc($AllPagesXML)->documentElement, TRUE));

  }

}

?>
