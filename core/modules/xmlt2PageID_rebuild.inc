<?php
/**
 * Анализирует ноду DOMDocument объекта из поля SrcXML как XML-описание меню и
 * удаляет из него содержимое закрытых пунктов меню.
 * Структура меню описывается XML документом или его нодой, где каждый раздел
 * меню описывается одной нодой с произвольным содержимым. Модуль имеет ряд
 * обязательных параметров, задающих название ключевых нод:
 * node_root - задает название корневой ноды меню
 * node_menu - задает название ноды, описывающей пункт меню
 * node_link - задает название ноды, хранящий ссылку (URL) пункта меню
 * 
 * Последняя правка позволяет в структуре SrcXML иметь произвольное число корневых
 * нод меню. При обработке они все будут слиты в одну ноду. Анализ содержимого
 * не проводится, т.е. дублирующиеся ноды копируются как есть.
 * 
 * Текущая позиция меню определяется совпадением содержимого поля PageID и
 * значением ноды с именем из параметра node_link. Нода, содержащая текущую
 * позицию меню, будет отмечена атрибутом <i>active</i>.
 * 
 * Пример XML меню:
 * <RootMenu>
 *   <Menu>
 *     <Title>menu1</Title>
 *     <URL>/page1.html</URL>
 *     <comment>набор параметров может быть любой</comment>
 *   </Menu>
 *   <Menu>
 *     <Title>menu2</Title>
 *     <URL>/page2.html</URL>
 *     <Menu>
 *       <Title>menu21</Title>
 *       <URL>/page2/page1.html</URL>
 *       <style>color:white</style>
 *     </Menu>
 *   </Menu>
 * </RootMenu>
 *
 * Параметры:
 *   node_root = RootMenu
 *   node_menu = Menu
 *   node_link = URL
 *
 */
class pager_module_xmlt2PageID_rebuild extends module_core implements module_interface {

    private $Parms = array(
        'node_link' => 'NodeLink',
        'node_root' => 'NodeRoot',
        'node_menu' => 'NodeMenu',
    );
    private $FindStack = array();

    function Run($pager, $module) {

        $this->_init_PageXP($pager);

        foreach ( $this->Parms as $parm => $var ) {
            if ( $this->PageXP->query($parm, $module)->length > 0 ) {
                $this->Parms[$var] = $this->PageXP->query($parm, $module)->item(0)->nodeValue;
            } else {
                $this->Log->msg("Dependment parametr '$parm' is not found", LOG_ERR);
                throw new Exception("Dependment parametr '$parm' is not found");
            }
        }

        $XML = new DOMDocument('1.0', 'UTF-8');
        $this->FindStack = array();

        $this->SXML_Path = new DOMXPath($pager->SrcXML);
        $this->XML_Path = new DOMXPath($XML);

        $MenuROOTNL = $this->SXML_Path->query('//' . $this->Parms['NodeRoot']);
        if ( $MenuROOTNL->length == 0 ) {
            $this->Log->msg($this->Parms['NodeRoot'] . ' not found in SrcXML', LOG_ERR);
            throw new Exception($this->Parms['NodeRoot'] . ' not found in SrcXML');
        }

        $XMLRoot = $XML->appendChild($XML->createElement($this->Parms['NodeRoot']));
        foreach ( $MenuROOTNL as $MenuROOTNode ) {
            foreach ( $MenuROOTNode->childNodes as $MRCN )
                $XMLRoot->appendChild($XML->importNode($MRCN, TRUE));
            $MenuROOTNode->parentNode->removeChild($MenuROOTNode);
        }

        $MenuROOT = $this->_create_xml_path($this->SXML_Path, $this->Parms['NodeRoot']);

        // найдем текущий пункт меню
        $MenuHIT = $this->XML_Path->query('//' . $this->Parms['NodeMenu'] . '[' . $this->Parms['NodeLink'] . "='" . $pager->PageID  . "']");
        if ( $MenuHIT->length > 0 ) {
            $Node = $MenuHIT->item(0);
            do {
                array_push($this->FindStack, $Node);
                $Node = $Node->parentNode;
            } while ( $Node->nodeName != $this->Parms['NodeRoot'] );
        }

        // перенесем пункты меню
        $this->_transferNodes($XMLRoot, $MenuROOT);

    }

    private function _transferNodes(DOMNode $XSrc, DOMNode $XDst) {

        $FS = array_pop($this->FindStack);
        foreach ( $this->XML_Path->query($this->Parms['NodeMenu'], $XSrc) as $NM ) {
            $Node = $XDst->appendChild($XDst->ownerDocument->importNode($NM));
            if ( $NM === $FS ) {
                if ( sizeof($this->FindStack) == 0 )
                    $Node->setAttribute('active', true);
                $this->_transferNodes($NM, $Node);
            }
            foreach ( $this->XML_Path->query('*', $NM) as $NP ) {
                if ( $NP->nodeName == $this->Parms['NodeMenu'] )
                    continue;
                $Node->appendChild($Node->ownerDocument->importNode($NP, TRUE));
            }
        }

    }

}
?>
