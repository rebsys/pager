<?php
/**
 * Модуль для запроса авторизации методом basic
 *
 * Создает запрос (HTTP Status code 401) для получения авторизованного доступа
 * к странице. Параметры модуля:
 *
 *  AuthRequestText - текст запроса, кириллица только в UTF-8 и далеко не во всех
 *      браузерах работает
 *
 *  account - описание пары идентификационных ключей, может быть произвольное
 *      количество - модуль будет проверять их все
 *
 *      account/login - логин
 *      account/password - пароль
 *
 * в случае если ни одна пара ключей так и не подошла, вызывает исключение с
 * кодом 60, и размещает в массиве $pager->TransitArray['ExceptionData']['Code']
 * код 401. Эти действия модуль modman может использовать для перехода на страницу
 * ошибки авторизации: PM_PagerUnauthorized
 *
 * Если какая-либо пара ключей подошла под переданные клиентом, дальнейший поиск
 * совпадений останавливается и модуль передает управление дальше. В саму страницу
 * PageXML добавляется нода PageAuth с параметрами успешной авторизации:
 *
 *  AuthMethod - использованный метод авторизации
 *  Login - успешный логин и
 *  Password - пароль
 *
 */
class pager_module_basic_auth extends module_core implements module_interface {

    /**
     * запускается для инициализации после создания экземпляра класса
     *
     * @param modman_class $mm - ссылка на ModMan служит для доступа к
     * StoreEngine, ModCount и возможны другие общие ресурсы
     * @return void
     */
    function Init($mm) {

    }

    /**
     * запускается для обработки страницы модулем
     *
     * @param pager_class $pager - ссылка на текущую страницу
     * @param DOMNode $module - на DOMElement, описывающий текущий модуль
     * @return void
     */
    function Run($pager, $module) {



        $this->_init_PageXP($pager);

        // TODO: перенести этот кусок в кору, как метод получения обязательных параметров модуля
        if ( $this->PageXP->query("*", $module)->length > 0 ) {
            $AccountXML = new DOMDocument('1.0', 'UTF-8');
            $AccountXML->appendChild($AccountXML->importNode($module, TRUE));
        } else {
            $AccountXML = $this->GetFileXML($pager, $module);
        }
        // TODO_END

        $AccountXP = new DOMXPath($AccountXML);

        if ( $AccountXP->query('AuthRequestText') )
            $AccountRequestText = $AccountXP->query('AuthRequestText')->item(0)->nodeValue;
        else
            $AccountRequestText = 'This page is authorised requested.';

        if ( isset($_SERVER['PHP_AUTH_USER']) and
                ! empty($_SERVER['PHP_AUTH_USER'])
        ) {

            foreach ( $AccountXP->query('account') as $AccNode ) {

                $LoginNL = $AccountXP->query('login', $AccNode);
                $PasswordNL = $AccountXP->query('password', $AccNode);

                if (
                    $LoginNL->length == 0 or
                    empty($LoginNL->item(0)->nodeValue)
                )
                    continue;

                if (
                    $_SERVER['PHP_AUTH_USER'] == $LoginNL->item(0)->nodeValue and
                    $_SERVER['PHP_AUTH_PW'] == $PasswordNL->item(0)->nodeValue
                ) {

                    $PageXML_ROOT = $this->PageXP->query(PageXML_RootPrefix)->item(0);
                    $PageAuthNode = $pager->PageXML->createElement('PageAuth');
                    $PageAuthNode->appendChild($pager->PageXML->createElement('AuthMethod', 'Basic'));
                    $PageAuthNode->appendChild($pager->PageXML->createElement('Login', $_SERVER['PHP_AUTH_USER']));
                    $PageAuthNode->appendChild($pager->PageXML->createElement('Password', $_SERVER['PHP_AUTH_PW']));
                    $PageXML_ROOT->appendChild($PageAuthNode);
                    return;

                }

            }

        }

        $this->_get_auth_req($AccountRequestText, $pager);

    }

    function _get_auth_req($req_str, $pager) {

        header("WWW-Authenticate: Basic realm=\"$req_str\", encoding=\"UTF-8\"");
        header("HTTP/1.0 401 Unauthorized");

        //redirect to 401 page
        $pager->TransitArray['ExceptionData']['Code'] = 401;
        throw new Exception('', 60);

    }

}

?>
