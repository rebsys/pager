<?php
/**
 * Выполняет обработку DOMDocument объекта из поля SrcXML в соответствии с XSL
 * шаблоном из поля OutXSL. Полученный в результате HTML добавляется в поле
 * OutText, формируя тем самым содержимое страницы.
 * В качестве альтернативы может загрузить XML из опционального параметра
 * file_xml_src, а шаблон из file_xsl_src. Еще может взять по указанному XPath
 * нужную ноду из PageXML, используя опциональный параметр pagexml_xpath.
 *
 * TODO: сделать опциональные параметры, указывающие на необходимость обработки
 * не всех документов SrcXML and OutXSL, а только определенных нод. Ноду можно
 * указать одну для SrcXML, и тогда из OutXSL будет извлечена такая же, а можно
 * сразу обе разных для обоих доментов
 *
 * TODO: file_src может содержат опциональный атрибут EngineType, в котором можно
 * явно указать тип используемого хранилища.
 */
class pager_module_xslt_run extends module_core implements module_interface {

    function Run($pager, $module) {
        
        $this->_init_PageXP($pager);
        libxml_use_internal_errors(true);

        if ( $this->PageXP->query("file_xml_src", $module)->length > 0 ) {

            try {
                $XML = new DOMDocument('1.0', 'UTF-8');
                $XML->loadXML($this->_load_path_file($pager, "file_xml_src", $module));
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg("module 'xslt_run' has a error, skipped", LOG_WARNING);
                throw new Exception('The object of a param "file_xml_src" not found or unable to load', 12);
            }

        } elseif ( $this->PageXP->query("pagexml_xpath", $module)->length > 0 ) {

            $NeedNodes = $this->PageXP->query($this->PageXP->query("pagexml_xpath", $module)->item(0)->nodeValue);
            if ( $NeedNodes->length > 0 ) {
                $XML = new DOMDocument('1.0', 'UTF-8');
                $XML->appendChild($XML->importNode($NeedNodes->item(0), TRUE));
            } else {
                $this->Log->msg('Not found XPath: ' .  (string) $this->PageXP->query("pagexml_xpath", $module)->item(0)->nodeValue, LOG_WARNING);
                $this->Log->msg("module 'xslt_run' has a error, skipped", LOG_WARNING);
                throw new Exception('The node of a param "pagexml_xpath" is not found on PageXML', 12);
            }
            /***
            $XML->formatOutput = TRUE;
            echo '<pre>', htmlspecialchars($XML->saveXML()), '</pre>';
            /***/

        } else {

            $XML = $pager->SrcXML;

        }

        if ( $this->PageXP->query("file_xsl_src", $module)->length > 0 ) {

            $XSL = new DOMDocument('1.0', 'UTF-8');
            try {
                $xsl_src = $this->_load_path_file($pager, "file_xsl_src", $module);
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg("module 'xslt_run' has a error, skipped", LOG_WARNING);
            }
            // TODO: проверить и при необходимости сделать опцию LIBXML_NOCDATA как параметр страницы PageXML
            if ( ! $XSL->loadXML($xsl_src) ) {
                throw new Exception('LibXML Error: ' . libxml_get_last_error()->message);
            }
            unset($xsl_src);

        } else {

            $XSL = $pager->OutXSL;

        }

        $XSLT = new XSLTProcessor();
        if ( ! $XSLT->importStylesheet($XSL) ) {
            throw new Exception('LibXML Error: ' . libxml_get_last_error()->message);
        }
        $html_res = $XSLT->transformToXML($XML);
        if ( $html_res === FALSE ) {
            throw new Exception('LibXML Error: ' . libxml_get_last_error()->message);
        }
        $pager->OutText .= $html_res;
        unset($html_res);

    }


}
?>
