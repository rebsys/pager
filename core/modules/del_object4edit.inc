<?php
/*
 * Модуль для удаления объектов из списка редактирования
 * 
 * Удаляет/редактирует ссылку на объект в списоке объектов для редактирования. Этот список может содержать несколько
 * вложенных списков объектов, но структура у всех должна быть одинаковая:
 * Object
 *   Name - визуальное название страницы, для построения списков доступных страниц
 *   File - путь (может содержать указание типа хранилища) на объект типа file с контентом страницы
 *   Page - путь (может содержать указание типа хранилища) на объект типа pager самой страницы
 * 
 * Параметры:
 *
 * activator - XPath для SrcXML, должен быть валидным (существовать) для разрешения работы модуля
 * activator_file - XPath для SrcXML, должен быть валидным (существовать) для внесения информации о файле в описание страницы
 * activator_page - XPath для SrcXML, должен быть валидным (существовать) для внесения информации о странице в описание страницы
 * remove_xpath (опциональный) - XPath для SrcXML, удаляет указанную ноду при успешном завершении
 * object_name - XPath для SrcXML, где лежит имя страницы из object_list_file, для которой будут осуществляться манипуляции с объектами
 * object_list_file - путь к xml-файлу, в котором храняться записи о доступных для редактирования объектах
 * object_list_xpath - XPath в структуре object_list_file, указывающий на узел, в который будут добавляться новые записи
 * 
 */

include_once '_share_object_core.inc';

class pager_module_del_object4edit extends _share_object_core implements module_interface {


  function Run($pager, $module) {

        parent::Run($pager, $module);

        // только для реализованных валидаторов
        if ( ! isset($pager->TransitArray['RequestDone']) )
            return;
        else
            $ReqResult = $pager->TransitArray['RequestDone'];

        // только для установленных активаторов
        if ( $this->SrcXP->query($this->_check_param('activator', $module))->length == 0 )
          return;

        $ObjectName = $this->_check_param('object_name', $module, TRUE);
        $ObjectListPath = 'file/' . $this->_check_param('object_list_file', $module);
        $ObjectListXML = new DOMDocument('1.0', 'UTF-8');
        $ObjectListXML->preserveWhiteSpace = FALSE;
        $ObjectListXML->loadXML($this->_load_path_file($pager, 'object_list_file', $module));
        $ObjectListXP = new DOMXPath($ObjectListXML);
        $ObjectListHome = $this->_check_param('object_list_xpath', $module);
        $ObjectListNL = $ObjectListXP->query($ObjectListHome);
        if ( $ObjectListNL->length == 0 )
                throw new Exception('XPath from "object_list_xpath" not found in XML from "object_list_file"', 12);
        $ObjectNL = $ObjectListXP->query("Object[Name='$ObjectName']", $ObjectListNL->item(0));
        if ( $ObjectNL->length == 0 )
                throw new Exception('Object with Name from "object_name" not found in XML from "object_list_file"', 12);
        if ( $ObjectNL->length > 1 )
                throw new Exception('Object with Name from "object_name" exists more than once in XML from "object_list_file"', 12);
        $ObjectN = $ObjectNL->item(0);

        $NewObjectSave = FALSE;

        if ($this->SrcXP->query($this->_check_param('activator_file', $module))->length > 0 ) {
          $ObjectFileNL = $ObjectN->getElementsByTagName('File');
          if ($ObjectFileNL->length > 0) {
            $ObjectN->removeChild($ObjectFileNL->item(0));
            $NewObjectSave = TRUE;
          } else {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
            $ObjectDataN->appendChild($this->_create_error(1, "для страницы с названием '$ObjectName' не определен контент"));
          }
        }
        if ($this->SrcXP->query($this->_check_param('activator_page', $module))->length > 0 ) {
          $ObjectPageNL = $ObjectN->getElementsByTagName('Page');
          if ($ObjectPageNL->length > 0) {
            $ObjectN->removeChild($ObjectPageNL->item(0));
            $NewObjectSave = TRUE;
          } else {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
            $ObjectDataN->appendChild($this->_create_error(1, "для страницы с названием '$ObjectName' не определено описание"));
          }
        }
        if ( $NewObjectSave ) {
          if (
            $ObjectN->getElementsByTagName('File')->length == 0
            and $ObjectN->getElementsByTagName('Page')->length == 0
          ) {
            $ObjectListNL->item(0)->removeChild($ObjectN);
          }
          $ObjectListXML->formatOutput = TRUE;
          $err = $this->_put_object($ObjectListPath, $ObjectListXML->saveXML());
          if ( $err != NULL ) {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleErrorsSet);
            $ObjectDataN->appendChild($err);
          } else {
            $ObjectDataN = $this->_create_xml_path($this->SrcXP, ModuleReportSet);
            $Report = $ObjectDataN->appendChild($pager->SrcXML->createElement('Report'));
            $Report->appendChild($pager->SrcXML->createElement('Text', "Данные для страницы '$ObjectName' успешно удалены из списока страниц"));
            try {
              $ActivatorN = $this->SrcXP->query($this->_check_param('remove_xpath', $module));
              if ( $ActivatorN->length > 0 )
                $ActivatorN->item(0)->parentNode->removeChild($ActivatorN->item(0));
            } catch(Exception $e) {}
          }
          
        }

  }

}

?>
