<?php
/**
 * Проверяет корректность указанного в $pager->PageXML/RequestResult пути к
 * объекту для редактирования, проверяет права доступа и прочее.
 * Выполняет чтение этого объекта из хранилища и размещение его в транзитных
 * данных страницы $pager->TransitArray, для дальнейшего встраивании его в
 * редактор.
 * Выполняет контроль и необходимые манипуляции с объектом, по результатам
 * обработки его редактором.
 *
 * Все необходимые структуры находятся в $pager->SrcXML.
 *
 * 1. Описание всех обхектов, доступных для редактирования данным модулем:
 *   <Object>
 *      <Title>Головная страница раздела</Title>
 *      <Path>file/examples/ROOT.html</Path>
 *   </Object>
 *     ...
 *   <LastEditResult>
 *     <Code>20</Code>
 *     <Message>Object saved.</Message>
 *   </LastEditResult>
 * Полный путь к этой ноде в SrcXML задается опциональным параметром
 * PageList_SrcXPath. По умолчанию он принимает значение /root/PageList
 * LastEditResult - опционально содержит результаты предыдущей операции
 * редактирования, сохранения результатов. Code может быть:
 *   10 - операция прервана
 *   20 - успешно завершено
 *   30 - операция запрещена
 *   40 - объект не найден
 *   50 - ошибка чтения
 *   60 - ошибка сохранения
 *
 * 2. Описание одного объекта, который будет загружен в редактор для внесения в
 * него изменений, выглядит так:
 *   <Title>Штатный заголовок</Title>
 *   <Path>file/examples/ROOT.html</Path>
 * Полный путь к этой ноде в SrcXML задается опциональным параметром
 * PageEdit_SrcXPath. По умолчанию он принимает значение /root/PageEdit
 * 
 * 3. Активация шаблона редактора выполняется при корректном значении параметра
 * из структуры RequestResult с именем EDIT. Путь к объекту указывается через
 * ObjectPath. Содержимое запрошенного для редактирования объекта помещается в
 * $pager->TransitArray['Object4Edit'].
 *
 * 4. Активация обработки данных от редактора выполняется при корректном значении
 * параметра  структуры RequestResult с именем SAVE. Измененное содержимое
 * объекта передается модулю напрямую через $_POST['EditorData'], что позволяет
 * передавать полностью несовместимые с XML данные. Путь к объекту по прежжнему
 * передается через параметр ObjectPath структуры RequestResult после валидации.
 *
 * TODO: XSS, CSRF - особо!!!
 * 
 */
class pager_module_object_editor extends module_core implements module_interface {

    /**
     * поиск по SrcXML
     * @var DOMXPath
     */
    private $SrcXP;
    /**
     * нода SrcXML/PageList
     * @var DOMNode
     */
    private $SrcPageListNode;

    /**
     * запускается для обработки страницы модулем
     *
     * @param pager_class $pager - ссылка на текущую страницу
     * @param DOMNode $module - на DOMElement, описывающий текущий модуль
     * @return void
     */
    function Run($pager, $module) {

        $this->_init_PageXP($pager);
        $this->SrcXP = new DOMXPath($pager->SrcXML);

        // начальные настройки
        if ( $this->PageXP->query('PageList', $module)->length > 0 and
             $this->PageXP->query('PageList', $module)->item(0)->nodeValue != ''
        ) {
            $PageListPath = (string) $this->PageXP->query('PageList', $module)->item(0)->nodeValue;
        } else {
            $PageListPath = '/root/PageList';
        }

        if ( $this->PageXP->query('PageEdit', $module)->length > 0 and
             $this->PageXP->query('PageEdit', $module)->item(0)->nodeValue != ''
        ) {
            $PageEditPath = (string) $this->PageXP->query('PageEdit', $module)->item(0)->nodeValue;
        } else {
            $PageEditPath = '/root/PageEdit';
        }
        $NameOfObjectForEdit = 'Object4Edit';

        // проверим целостность XML структур
        if ( $this->SrcXP->query($PageListPath . '/Object')->length > 0 )
            $this->SrcPageListNode = $this->SrcXP->query($PageListPath)->item(0);
        else
            throw new Exception('PageList not found or wrong format!', 14);

        if ( $this->SrcXP->query(dirname($PageEditPath))->length > 0 )
            $SrcPageEditParentNode = $this->SrcXP->query(dirname($PageEditPath))->item(0);
        else
            throw new Exception('Parent of PageEdit not found!', 14);

        if ( $this->PageXP->query(PageXML_RootPrefix . '/RequestResult')->length > 0 )
            $PageXML_RRNode = $this->PageXP->query(PageXML_RootPrefix . '/RequestResult')->item(0);
        else
            throw new Exception('RequestResult node not found in PageXML!', 14);

        if ( $this->SrcXP->query($PageEditPath)->length > 0 )
            $SrcPageEditParentNode->removeChild($this->SrcXP->query($PageEditPath)->item(0));

        // определим объект
        if ( $this->PageXP->query('item[name=\'ObjectPath\']/value', $PageXML_RRNode)->length > 0 ) {
            $ObjectPath = $this->PageXP->query('item[name=\'ObjectPath\']/value', $PageXML_RRNode)->item(0)->nodeValue;
            // поищем такой объект в списке
            if ( $this->SrcXP->query('Object[Path=\'' . $ObjectPath . '\']', $this->SrcPageListNode)->length > 0 )
                $ObjectNode = $this->SrcXP->query('Object[Path=\'' . $ObjectPath . '\']', $this->SrcPageListNode)->item(0);
        }

        // EDIT - begin
        // запросили вывод редактора для объекта
        if ( $this->PageXP->query('item[name=\'EDIT\']/value', $PageXML_RRNode)->length > 0 ) {

            // проверим объект
            if ( ! $ObjectNode instanceof DOMNode ) {
                $this->_setLastEditResult('Object not found', 40);
                throw new Exception('Object not found!', 16);
            }
            // проверим права на доступ (на чтение) к этому объекту
            // .....
            // запросим объект в хранилище и сохраним его в TransitArray
            try {
                $pager->TransitArray[$NameOfObjectForEdit] = $this->se->GetObject($ObjectPath);
            } catch (Exception $e) {
                $this->_setLastEditResult("Unable get object: $ObjectPath", 50);
                throw new Exception("Unable get object: $ObjectPath", 12);
            }
            // перенесем описание объекта в PageEdit
            $PageEditNode = $pager->SrcXML->createElement(basename($PageEditPath));
            foreach ( $this->SrcXP->query('*', $ObjectNode) as $node )
                $PageEditNode->appendChild($node);
            // удалим ненужный PageList
            $this->SrcPageListNode->parentNode->removeChild($this->SrcPageListNode);
            // добавим PageEdit в исходный SrcXML
            $SrcPageEditParentNode->appendChild($PageEditNode);

        } // end of EDIT

        // SAVE - begin
        // запросили сохранение объекта
        if ( $this->PageXP->query('item[name=\'SAVE\']/value', $PageXML_RRNode)->length > 0 ) {

            // проверим объект
            if ( ! $ObjectNode instanceof DOMNode ) {
                $this->_setLastEditResult('Object not found', 40);
                throw new Exception('Object not found!', 16);
            }
            // проверим права на доступ (на запись) к этому объекту
            // .....
            // сохраним данные объекта в хранилище
            try {
                $this->se->SaveObject($ObjectPath,  $_POST['EditorData']);
                $this->_setLastEditResult('Object saved', 20);
            } catch (Exception $e) {
                $this->_setLastEditResult("Unable write object: $ObjectPath", 50);
                throw new Exception("Unable write object: $ObjectPath", 12);
            }

        } // end of SAVE

        // CANCEL - begin
        if ( $this->PageXP->query('item[name=\'CANCEL\']/value', $PageXML_RRNode)->length > 0 ) {

            $this->_setLastEditResult('Canceled', 10);

        } // end of CANCEL

    }

    /**
     * создает LastEditResult и прописывает туда $message и $code 
     * 
     * @param string $message
     * @param int $code 
     * @return void
     */
    private function _setLastEditResult($message, $code) {

        $xml = $this->SrcPageListNode->ownerDocument;
        $node = $xml->createElement('LastEditResult');
        $node->appendChild($xml->createElement('Message', $message));
        $node->appendChild($xml->createElement('Code', $code));
        $this->SrcPageListNode->appendChild($node);

    }


}
?>
