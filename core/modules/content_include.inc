<?php
/**
 * формирует в поле OutText динамическую часть страницы HTML
 * т.е. создает собственно страницу с аутентичным содержимым
 *
 * контент может быть задан как (в порядке убывания приоритета):
 *
 * - параметр модуля file_src
 *
 * - параметр страницы Properties/Page_Content/file_src
 *
 * - просто как существующий объект типа file, c путем совпадающим
 * с текущим PageID и имеющий суфикс из константы FileAutoContent_Ext (.html),
 * а префикс из константы FileAutoContent_Prefix, и если определено свойство
 * страницы Properties/Page_Content/auto_file
 *
 * Пример:
 * 1) для запроса страницы /item
 *    должен быть объект file/item.html
 * 2) для запроса страницы /product/list
 *    должен быть объект file/product/list.html
 * 3) для запроса страницы /users/profile.php
 *    должен быть объект file/users/profile.php.html
 *
 */

class pager_module_content_include extends module_core implements module_interface {

    function Run($pager, $module) {

        $this->_init_PageXP($pager);

        if ( $this->PageXP->query("Properties/Page_Content/auto_file/prefix")->length > 0 ) {
            $auto_prefix = $this->PageXP->query("Properties/Page_Content/auto_file/prefix")->item(0)->nodeValue;
        } else {
            $auto_prefix = FileAutoContent_Prefix;
        }
        if ( $this->PageXP->query("Properties/Page_Content/auto_file/suffix")->length > 0 ) {
            $auto_suffix = $this->PageXP->query("Properties/Page_Content/auto_file/suffix")->item(0)->nodeValue;
        } else {
            $auto_suffix = FileAutoContent_Ext;
        }
        
        $ContentFile = $this->LoadFile($pager, $module, $auto_suffix, $auto_prefix);
        $pager->TransitArray['content_path'] = end($pager->TransitArray['content_objects']);

        if ( ! empty($ContentFile) ) {
            $pager->OutText .= $ContentFile;
            unset($ContentFile);

        } else {

            $this->Log->msg("module 'content_include' cant find the content, skipped", LOG_WARNING);

        }
    }


}
?>
