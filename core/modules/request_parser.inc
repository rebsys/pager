<?php
/*
 * Берет описание входных полей из шаблона в SrcXML, путь к ноде с шаблоном определенного типа
 * берет из параметра RequestTemplatePath от корня SrcXML.
 * Проверяет входящие данные в соответствии с шаблоном RequestTemplate: по источнику, названию,
 * соответствию шаблону или фильтру, и другим опциональным параметрам.
 * Обработанные через фильтр данные помещает в RequestTemplate для возможного повторного вывода
 * формы, добавляя их сведениями об обнаруженных ошибках. Прошедшие все необходимые проверки
 * данные заносятся в структуру RequestResult в блоке SrcXML, откуда потом могут безопасно
 * использоваться другими модулями. Полностью корректные данные после всех необходимых фильтров
 * помещаются элемент <value>, копия необработанных данных помещается в элемент <raw>. Если ввод
 * данных зафиксирован, в $pager->TransitArray помещается элемент RequestDone, который содержит
 * XPath на структуру обработанных данных в SrcXML. Обычно это RequestResult, если не задано другое.
 *
 * Параметры:
 *   RequestTemplatePath - абсолютный путь в блоке SrcXML, где находится шаблон для обработки
 *   входящих данных.
 *   RequestResultPath - абсолютный путь в блоке SrcXML, куда будут записаны обработанные входные
 *   данные, в случае успешого прохождения всех проверок.
 *
 * Формат RequestTemplate:
 *   Предопределенные параметры:
 *     ItemsList - содержит список всех возможных входных переменных в формате:
 *       Item - блок описание пеменной
 *         Name	  - название переменной
 *         Source - источник данных для переменной. Может принимать значение POST, GET, COOKIE.
 *                  Имя переменной во входных данных совпадает с именем переменной в XML-шаблоне.
 *                  Опционально Source может быть снабжен атрибутом SourceName, который задет
 *                  альтернативное имя переменной во входных данных.
 *                  Source может быть не один, тогда, в порядке указания в XML-шаблоне, будут
 *                  проверяться все указанные источники до тех пор, пока в одном из них не найдется
 *                  переменная с требуемым именем. Атрибут SourceName может быть разным для каждого
 *		    экземпляра Source.
 *                  Если во входных данных будет отсутствовать искомая переменная, будет
 *                  генерироваться WARNING во всех случаях, кроме как при использовании тэга
 *                  Optional.
 *		    Можно опустить Source для переменной, тогда будет выдан WARNING и обработка
 *		    перейдет к следующей переменной списка.
 *	   Optional
 *		  - опциональный, не вызывающих даже предупреждений при отсутствии во входном потоке.
 *         Mandatory 
 *		  - необязательный тэг, включающий проверку на обязательность введения каких-либо
 *                  значений во  входных данных, если содержит "On". Проверяется до всех фильтров и шаблонов.
 *		    Mandatory может быть задан в Defaults для всего шаблона.
 *         InputSave
 *		  - опциональный тег, включающий копирование входных данных переменной обратно в шаблон в
 *		    значение Value, что обеспечивает сохранение введенных данных при повторном выводе формы для
 *		    редактирования/исправления. Это действие выполняется только в том случае, если тэг не пустой.
 *		    Может содержать тэг Filters, содержащий произвольный набор поддерживаемых фильтров, которые
 *		    последовательно будут применены к входным данным переменной, перед копированием их обратно в
 *		    шаблон. Обычно не требуется, если в XSL-шаблоне для этой перемнной не использован атрибут
 *		    disable-output-escaping="yes" в тэге xsl:value-of, который отключает обычную экранировку
 *		    специальных символов типа "<".
 *		    Если копирование требуется без каких-либо фильтров, то тэг может иметь пустой фильтр, или любых
 *		    других произвольных потомков - одного или несколько.
 *		    Чтобы заблокировать работу тэга (например, при установке всем InputSave через Defaults) можно
 *		    оставить его пустым, без потомков.
 *	   Filters
 *		  - 
 *	       TagsWhiteList
 *		  - 
 *		   TagName
 *		  - 
 *	       PRegReplace
 *		  - 
 *		   Match
 *		  - 
 *		   Replace
 *		  - 
 *	   Templates
 *		  - 
 *	       PregMatch
 *		  - 
 *	   
 *         
 *         
 *         
 *         
 *         
 *         
 *     RequestValidator
 *		  - задает название переменных во входном потоке, наличие которых свидетельствует об выполнении
 *		    метода SUBMIT для формы. Т.е. о необходимости произвести разбор и анализ входных данных,
 *		    включая обязательные поля.
 *		    Может быть несколько валидаторов. В этом случае парсер включается при наличчи любого из них.
 *		    В описание поле-валидатора, аналогично с полями ItemsList, вписываются источник данных и на
 *		    будущее запланировано указание метода проверки содержимого валидатора, чтобы выполнять
 *		    проверку на CSRF.
 *		    По результатам проверки валидаторов, в транзитном массиве $pager->TransitArray будет создан
 *		    массив 'RequestValidator', в котором для каждого успешно проверенного валидатора создаеся
 *		    ключ, совпадающий с именем валидатора. Массив используется для того, чтобы избежать ненужных
 *		    проверок входных параметров через ItemsList.
 *		    Если хотя бы один валидатор успешно реализован, дополнительно в транзитный массив
 *		    $pager->TransitArray с ключом 'RequestDone' заносится путь сохранения результатов запроса.
 *		
 *     ErrorsRef  - 
 *       Error	  - 
 *	   Id	  - 
 *	   Text	  - 
 *     Defaults	  - определяет тэги, которые будут применены для всех переменных шаблона, если только описание
 *     		    переменной уже не содержит такой тэг. Т.е. для того, чтобы избавиться от применения к
 *		    переменной определенных в Defaults какого-либо тэга, нужно прописать этот тэг в самой
 *     		    переменной. Можно просто пустым, или с какими-либо значениями, но общий тэг из Defaults
 *		    не будет применен к этой переменной.
 * 		    В настоящее время копируются следующие тэги: Templates, Filters, InputSave, Mandatory.
 *		    Дополнение: применяются только те тэги из Defaults, которые не пустые. Содержимое не
 *		    анализируется, но тэги без потомков просто игнорируются.
 * 
 *   Кроме предопределенных, в этом шалоне могут появляться тэги, связанные с обработкой входных данных.
 *   На текущий момент это только сведения об ошибках, возникших в процессе проверке соответствия шаблону,
 *   или отсутствие данных для Mandatory переменных.
 *   Формат динамических тэгов таков:
 *     ErrorsSet  - блок ошибок, выявленных в форме. Ошибки групируются по типам, и в блоке не может быть
 *		    более одной ошибки одного типа.
 *       Error	  - описание ошибки соответствует описанию из ErrorsRef
 *	   Id	  - код ошибки
 *	   Text	  - текст ошибки
 * 
 * Формат RequestResult:
 *     Item
 *       Name	  - копия тэга Name из InputList.
 *	 Source	  - копия того тэга Source из InputList, который содержит реализованный метод получения данных.
 *		    Т.е. указывет на реальный источник данных Source, и имя входной переменной SourceName,
 *		    которые и принесли результат для RAW и Value.
 *	 RAW	  - чистые значения переменной из входных данных, к которым не были применены ни какие фильтры.
 *		    Следует использовать с ОЧЕНЬ БОЛЬШОЙ ОСТОРОЖНОСТЬЮ.
 *	 Value	  - значения переменной из входных данных, после всех фильтров и проверок. Именно эти значения
 *		    рекомендуется использовать. Если в Templates применялся шаблон PregMatch, содержащий
 *		    подзапросы из круглых скобок, типа ([A-z+])-([0-9]+), то после первого Value с содержимым
 *		    переменной, последует еще такое кол-во Value, сколько подзапросов было определено в PregMatch.
 *
 * Предопределяемые константы:
 *   RequestTemplatePathDefault = /RequestTemplate
 *   RequestResultPathDefault = /RequestResult
 *
 * может быть будет удобно размещать описание входных параметров прямо в параметрах
 * модуля, но сейчас эта идея хорошей не кажется
 *
 * TODO: сделать реализацию CSRF токена в валидаторе
 * TODO: вот для этого и других модулей можно делать xml-описание модуля, в котором
 * указывать требуемые классы и интерфейсы.
 */

if ( !defined('RequestTemplatePathDefault')) define('RequestTemplatePathDefault', '/RequestTemplate');
if ( !defined('RequestResultPathDefault')) define('RequestResultPathDefault', '/RequestResult');

// дефолтные параметры для элемента
if ( !defined('RRDef_SRC')) define('RRDef_SRC', 'POST');
if ( !defined('RRDef_TagsWhiteList')) define('RRDef_TagsWhiteList', '<br><p><b><i><em><strong>');
//if ( !defined('RRDef_')) define('RRDef_', '');


class pager_module_request_parser extends module_core implements module_interface {

    private $RequestTemplatePath;
    private $RequestResultPath;
    private $RequestValidatorArr;

    /*
     * выполняет фильтры
     */
    private function _do_filter(DOMXPath $xpath, DOMNode $filter, $data) {
        // определим тип фильтра
        switch ( $filter->nodeName ) {
            case 'TagsWhiteList' :
                $TWL_String = '';
                foreach ( $xpath->query('TagName', $filter) as $RT_TWN ) {
                    $TWL_String .= '<' . $RT_TWN->nodeValue . '>';
                }
                $data = strip_tags($data, $TWL_String);
                break;
            case 'PRegReplace' :
                $Match = $this->_node_value($xpath, 'Match', $filter);
                $Replace = $this->_node_value($xpath, 'Replace', $filter);
                if ( empty($Match) )
                    throw new Exception('PRegReplace filter has not Match node', 14);
                $data = preg_replace($Match, $Replace, $data);
                break;
            default :
                throw new Exception('Unknown Filter: ' . $filter->nodeName, 14);
        }
        return $data;
    }

    /*
     * копирует дефотные настройки шаблона для каждого ItemsList/Item
     */
    private function _set_defaults(DOMXPath $xpath, DOMNode $req_temp) {
        foreach  ( $xpath->query('ItemsList/Item', $req_temp) as $ItemNode ) {
            foreach ( $xpath->query('Defaults/*', $req_temp) as $DefNode ) {
                switch ($DefNode->nodeName) {
                    case 'Templates' :
                    case 'Filters' :
                    case 'InputSave' :
                    case 'Mandatory' :
                        $ftype = $DefNode->nodeName;
                        break;
                    default :
                        continue 2;
                }
                if ( $xpath->query($ftype, $ItemNode)->length == 0
                     and $DefNode->hasChildNodes()
                )
                    $ItemNode->appendChild($DefNode->cloneNode(TRUE));
            }
        }
    }

    /*
     * устанавливает ошибку для Item и дублирует для всего шаблона и заносит в валидатор
     */
    private function _set_error(DOMXPath $xpath, DOMNode $item, $code, $text) {
        $ItemErrorNode = $item->ownerDocument->createElement('Error');
        $ItemErrorNode->appendChild($item->ownerDocument->createElement('Id', $code));
        $ItemErrorNode->appendChild($item->ownerDocument->createElement('Text', $text));
        $item->appendChild($ItemErrorNode);
        if ( $xpath->query("//ErrorsSet/Error[Id=$code]")->length == 0 ) {
            $node = $this->_create_xml_path($xpath, $this->RequestTemplatePath . '/ErrorsSet');
            $node->appendChild($ItemErrorNode->cloneNode(TRUE));
        }
        if ( isset($this->RequestValidatorArr['Error']) )
            $this->RequestValidatorArr['Error']++;
        else
            $this->RequestValidatorArr['Error'] = 1;
    }

    /*
     * возвращает значение первой ноды, найденной по запросу $query
     * с помощью $xpath. Возможно ограничить зону поиска границами
     * опционально заданного DOMElement $node.
     * Если ноды нет, возвращает FALSE
     */
     private function _node_value(DOMXPath $xpath, $query, $node=null) {
     
         if ( $node != NULL and is_subclass_of($node, 'DOMNode', FALSE) ) {
             $XP = $xpath->query($query, $node);
         } else {
             $XP = $xpath->query($query);
         }
        
        if ( $XP->length > 0 ) {
            return $XP->item(0)->nodeValue;
        } else {
            return FALSE;
        }
     
     }

    /**
     * возвращает из предопределеного источника $src переменную $var
     */
    private function _src_value($src, $var) {

        switch ($src) {
            case 'post':
            case 'Post':
            case 'POST':
                if (isset($_POST[$var])) return $_POST[$var];
                else throw new Exception("variable $var not defined in POST", 1); 
            case 'get':
            case 'Get':
            case 'GET':
                if (isset($_GET[$var])) return $_GET[$var];
                else throw new Exception("variable $var not defined in GET", 1); 
            case 'cookie':
            case 'Cookie':
            case 'COOKIE':
                if (isset($_COOKIE[$var])) return $_COOKIE[$var];
                else throw new Exception("variable $var not defined in COOKIE", 1); 
            default:
                throw new Exception("Unknown method '$src'", 2); 
        }

    }

    function Run($pager, $module) {

        $this->_init_PageXP($pager);
        $ReqXP = new DOMXPath($pager->SrcXML);

        // настроим и проверим пути

        $this->RequestTemplatePath = $this->_node_value($this->PageXP, 'RequestTemplatePath', $module) or $this->RequestTemplatePath = RequestTemplatePathDefault;
        $this->RequestResultPath = $this->_node_value($this->PageXP, 'RequestResultPath', $module) or $this->RequestResultPath = RequestResultPathDefault;

        if ( $ReqXP->query($this->RequestTemplatePath)->length > 0 ) {
            $RT_Node = $ReqXP->query($this->RequestTemplatePath)->item(0);
        } else {
            $this->Log->msg("RequestTemplate '" . $this->RequestTemplatePath . "' not found in SrcXML", LOG_ERR);
            throw new Exception("RequestTemplate '" . $this->RequestTemplatePath . "' not found in SrcXML", 404);
        }

        if ( preg_match('#(.*/)([^/]+)#', $this->RequestResultPath, $rr_arr) ) {
            $RR_Node = $pager->SrcXML->createElement($rr_arr[2]);
            $RR_PNode = $this->_create_xml_path($ReqXP, $rr_arr[1]);
        } else {
            $this->Log->msg("Unable parse RequestResultPath '" . $this->RequestResultPath . "'", LOG_ERR);
            throw new Exception("Ugly RequestResultPath: '" . $this->RequestResultPath . "'", 500);
        }

        // скопируем дефолтные параметры проверки данных из Defaults в текущий Item
        $this->_set_defaults($ReqXP, $RT_Node);

        // проверим валидатор запроса: была ли передача данных, нужно ли делать проверки входящих данных?
#        $RT_ValidatorNL = $ReqXP->query('RequestValidator/Item[Name]', $RT_Node);
#        if ( $RT_ValidatorNL->length > 0 ) {
        $pager->TransitArray['RequestValidator'] = array();
        foreach ( $ReqXP->query('RequestValidator/Item[Name]', $RT_Node) as $RT_ValidatorN ) {
                // если валидатор определен - проверяем есть ли он во входных данных
                $RT_ValidatorName = $ReqXP->query('Name', $RT_ValidatorN)->item(0)->nodeValue;
                try {
                    $pager->TransitArray['RequestValidator'][$RT_ValidatorName]['Value'] = $this->_src_value($ReqXP->query('Source', $RT_ValidatorN)->item(0)->nodeValue, $RT_ValidatorName);
                    $this->RequestValidatorArr = &$pager->TransitArray['RequestValidator'][$RT_ValidatorName];
                } catch ( Exception $e) {
                    // неактивный валидатор, смотрим следующий
                }
                // если необходимо, можно тут же проверить значение валидатора, т.е. использовать его как CSRF Token:
                // 1. создается токен и прописывается в Value валидатора в случае
                //    отсутствия валидатора во входном потоке, затем - return
                // 2. проверяется значение токена из присланного валидатора, и если
                //    ок - идем далее, нет - добавляем ошибку и return.
                // значение валидатора - (простой вариант, без хранения. позволяет многократное использование некое
                // небольшое кол-во времени) это временная_метка+разделитель+хэш_на_метке_и_секрете.
        }
        if ( sizeof($pager->TransitArray['RequestValidator']) == 0 )
            return; // ни одного валидатора не обнаружено, ничего не делаем

        // обработает список входных параметров
        foreach ( $ReqXP->query('ItemsList/Item', $RT_Node) as $RT_Item ) {

            // сбросим предыдущие значения
            unset($SrcValue);
            $SrcValueArray = array();

            // определим название переменной
            $RT_ItemNameNL = $ReqXP->query('Name', $RT_Item);
            if ( $RT_ItemNameNL->length > 0  and
                 $RT_ItemNameNL->item(0)->nodeValue != ''
            ) {
                $RR_ItemNode = $pager->SrcXML->createElement('Item');
                $RR_ItemNode->appendChild($RT_ItemNameNL->item(0)->cloneNode(TRUE));
            } else {
                throw new Exception('Undefined "Name" of the input variable', 14);
            }

            // определим список источников данных
            foreach ( $ReqXP->query('Source', $RT_Item) as $RT_ItemSrc ) { // find src - begin
                // определим идентификатор переменной во массиве входящих данных
                if ( $RT_ItemSrc->attributes != NULL and
                     $RT_ItemSrc->attributes->getNamedItem('SourceName') != ''
                ) {
                    $SrcName = (string) $RT_ItemSrc->attributes->getNamedItem('SourceName')->nodeValue;
                } else {
                    $SrcName = $RT_ItemNameNL->item(0)->nodeValue;
                }
                // определим способ передачи данных (POST or GET etc.)
                if ( empty($RT_ItemSrc->nodeValue) ) {
                    throw new Exception('Undefined the method of the input data', 14);
                }
                try {
                    $SrcValue = $this->_src_value( (string) $RT_ItemSrc->nodeValue, $SrcName);
                } catch ( Exception $e) {
                    if ( $ReqXP->query('Optional', $RT_Item)->length == 0 )
                        $this->Log->msg("WARNING: " . $e->getMessage(), LOG_WARNING);
                }
                if ( isset($SrcValue) ) {
                        $RR_ItemNode->appendChild($RT_ItemSrc->cloneNode(TRUE));
                        $CDATA_XML = $pager->SrcXML->createElement('RAW');
                        $CDATA_XML->appendChild($pager->SrcXML->createCDATASection($SrcValue));
                        $RR_ItemNode->appendChild($CDATA_XML);
                        // если требуется, сделаем копию входных данных в шалон для повторного вывода в форме
                        $InputSaveXP = $ReqXP->query('InputSave', $RT_Item);
                        if ( 
                            $InputSaveXP->length > 0 and
                            $InputSaveXP->item(0)->hasChildNodes()
                        ) {
                            $InputSaveValue = $SrcValue;
                            foreach ( $ReqXP->query('InputSave/Filters/*', $RT_Item) as $InputSaveNode ) {
                                // определим и выполним необходимые фильтры, перед сохранением данных в шаблон
                                $InputSaveValue = $this->_do_filter($ReqXP, $InputSaveNode, $InputSaveValue);
                            }
                            $RT_Item->appendChild($pager->SrcXML->createElement('Value', $InputSaveValue));
                        }
                        break;
                }
            }   // find SRC - end

            // проверим наличие mandatory для этого параметра
            if (
                $this->_node_value($ReqXP, 'Mandatory', $RT_Item) == 'On'
                and empty($SrcValue)
            ) {
                $this->_set_error($ReqXP, $RT_Item, 1, 'Mandatory field is empty');
                continue;
            }
            // если данных для этого параметра нет, то пропустим проверки фильтров
            if ( ! isset($SrcValue) ) {
                if ( $ReqXP->query('Optional', $RT_Item)->length == 0 )
                    $this->Log->msg("Not found any data for '$SrcName'", LOG_WARNING);
                continue;
            }

            // выполним необходимые фильтры
            foreach ( $ReqXP->query('Filters/*', $RT_Item) as $RT_ItemFltNode ) {
                $SrcValue = $this->_do_filter($ReqXP, $RT_ItemFltNode, $SrcValue);
/*
                // определим тип фильтра
                switch ( $RT_ItemFltNode->nodeName ) {
                    case 'TagsWhiteList' :
                        $TWL_String = '';
                        foreach ( $ReqXP->query('TagName', $RT_ItemFltNode) as $RT_TWN ) {
                            $TWL_String .= '<' . $RT_TWN->nodeValue . '>';
                        }
                        $SrcValue = strip_tags($SrcValue, $TWL_String);
                        break;
                    case 'PRegReplace' :
                        $Match = $this->_node_value($ReqXP, 'Match', $RT_ItemFltNode);
                        $Replace = $this->_node_value($ReqXP, 'Replace', $RT_ItemFltNode);
                        if ( empty($Match) )
                            throw new Exception('PRegReplace filter has not Match node', 14);
                        $SrcValue = preg_replace($Match, $Replace, $SrcValue);
                        break;
                    default :
                        throw new Exception('Unknown Filter: ' . $RT_ItemFltNode->nodeName, 14);
                }
*/
            }

            // определим список проверочных шаблонов
            try {
                foreach ( $ReqXP->query('Templates/*', $RT_Item) as $RT_ItemTmpNode ) {
                    // определим тип шаблона
                    switch ( $RT_ItemTmpNode->nodeName ) {
                        case 'PregMatch' :
                            // если шаблон содержит под-шаблоны из скобок (), содержимое под-шаблонов
                            // будет сохранено дополнительными Value значениями в результате
                            if ( ! preg_match($RT_ItemTmpNode->nodeValue, $SrcValue, $TemplateArr) ) {
                                $this->_set_error($ReqXP, $RT_Item, 2, 'The field not mathed with template');
                                throw new Exception();
                            }
                            if ( sizeof($TemplateArr) > 1) {
                                $SrcValueArray = array_slice($TemplateArr, 1);
                            }
                            break;
                        default:
                            throw new Exception('Unknown Template: ' . $RT_ItemTmpNode->nodeName, 14);
                    }
                    // проверим соответствие входных данных ($SrcValue) шаблону
                    // запишем ошибки
                } // check templates - end
            } catch ( Exception $e) {
                // шаблон не совпадает с данными, пропускаем эту переменную без сохранения в результате
                continue;
            }

            // сохраним входные данные, если проверки на шаблоны прошли успешно
            if ( isset($SrcValue) )
                array_unshift($SrcValueArray, $SrcValue);

            foreach ($SrcValueArray as $sva) {
                $CDATA_XML = $pager->SrcXML->createElement('Value');
                $CDATA_XML->appendChild($pager->SrcXML->createCDATASection($sva));
                $RR_ItemNode->appendChild($CDATA_XML);
            }

            // добавим готовый параметр в RequestResult
            $RR_Node->appendChild($RR_ItemNode);
            
        }
        
        $RR_PNode->appendChild($RR_Node);
        $pager->TransitArray['RequestDone'] = $this->RequestResultPath;


    }


}
?>
