<?php
/*
 * Аналогично get_object, читает объект из секции CData в SrcXML в соответствии
 * с заданным XPath в object_path_src. Записывает прочитанный объект в хранилище,
 * используя абсолютный путь объекта, заданный в SrcXML с помощью XPath через
 * параметр object_path_dst.
 *
 * опциональный параметр mandatory позволяет выдавать или не выдавать
 * сообщение об ошибке, при отсутствии в SrcXML самого объекта, заданного
 * с помощью object_path_src
 */

class pager_module_put_object extends module_core implements module_interface {

  function Run($pager, $module) {

        $this->_init_PageXP($pager);
        
        $Mandatory = FALSE;
        if ( $this->PageXP->query('mandatory', $module)->length > 0 )
          $Mandatory = TRUE;
        
        $SrcNList = $this->PageXP->query('object_path_src', $module);
        $DstNList = $this->PageXP->query('object_path_dst', $module);

        if ( $SrcNList->length == 0 )
            throw new Exception('parameter "object_path_src" is not specified', 16);
        if ( $DstNList->length == 0 )
            throw new Exception('parameter "object_path_dst" is not specified', 16);

        $SrcXP = new DOMXPath($pager->SrcXML);
        $ObjDataNL = $SrcXP->query($SrcNList->item(0)->nodeValue);
        if ( $ObjDataNL->length == 0 ) {
            if ( $Mandatory )
              throw new Exception('Xpath from parameter "object_path_src=' . $SrcNList->item(0)->nodeValue . '" not found', 12);
            return;
        }
        $ObjData = $ObjDataNL->item(0)->nodeValue;

        $ObjPathNL = $SrcXP->query($DstNList->item(0)->nodeValue);
        if ( $ObjPathNL->length == 0 )
            throw new Exception('Xpath from parameter "object_path_dst=' . $DstNList->item(0)->nodeValue . '" not found', 12);
        $ObjPath = $ObjPathNL->item(0)->nodeValue;

        try {
            $this->se->SaveObject(
              $ObjPath,
              $ObjData,
              array(
                'Auth' => array(
                  'User' => 'noname',
                  'Cred' => '',
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to save an object to "object_path_dst"=' . $ObjPath, 12);
        }

  }


}

?>
