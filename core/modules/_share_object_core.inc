<?php
/*
 * базовый класс для манипулирующих объектами модулей
 */

define('ModuleErrorsSet', '/ModuleErrorsSet');
define('ModuleReportSet', '/ModuleReportSet');

class _share_object_core extends module_core {

  protected $SrcXP;
  protected $pager;
  protected $module;
  protected $AuthArr;

/*  public static function exception_error_handler($errno, $errstr, $errfile, $errline ) {
      throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
  }*/

  /**
   * создает и возвращает узел штатной ошибки для SrcXML
   * поддерживает ошибки:
   * 1 - not found, 2 - already exists
   * @param int $code
   * @param string $text
   * @return DOMNode
   */
  protected function _create_error($code, $text) {
        switch ( $code ) {
          case 1:
            $name = 'Not found';
            break;
          case 2:
            $name = 'Already exists';
            break;
          default:
            $name = 'unknown';
        }
        $ErrorN = $this->pager->SrcXML->createElement('Error');
        $ErrorN->appendChild($this->pager->SrcXML->createElement('Id', $code));
        $ErrorN->appendChild($this->pager->SrcXML->createElement('Name', $name));
        $ErrorN->appendChild($this->pager->SrcXML->createElement('Text', $text));
        return $ErrorN;
  }


  /*
   * проверяет доступность для чтения объекта $object_path в хранилище
   * возвращает DOMNode с описанием ошибки или NULL в случае успеха
   */
  protected function _check_object($object_path) {
        try {
            $ObjExists = $this->se->GetObject(
              $object_path,
              array(
                'Auth' => $this->AuthArr,
                'Feature' => array(
                  'Name' => SEF_Exists,
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to check exists an object ' . $object_path, 12);
        }
        if ( ! $ObjExists ) { // сообщаем об ощибке и выход
            $ErrXML = $this->pager->SrcXML->createElement('Error');
            $ErrXML->appendChild($this->pager->SrcXML->createElement('Id', 1));
            $ErrXML->appendChild($this->pager->SrcXML->createElement('Name', 'Not found'));
            $ErrXML->appendChild($this->pager->SrcXML->createElement('Text', 'Объект не найден или не существует'));
            return $ErrXML;
        }
        return;
  }
            
  /*
   * пытается получить объект $object_path из хранилища
   * возвращает содержимое объекта или NULL в случае неудачи
   */
  protected function _get_object($object_path) {
        try {
            $ObjData = $this->se->GetObject(
              $object_path,
              array(
                'Auth' => $this->AuthArr,
              )
            );
            return $ObjData;
        } catch (Exception $e) {
            if ( $e->getcode() != 404 ) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                throw new Exception('failed to get an object from ' . $object_path, 12);
            }
            return NULL;
        }
  }

  /*
   * пытается сохранить данные из $object_data в хранилище с именем объекта $object_path
   */
  protected function _put_object($object_path, $object_data) {
        try {
            $this->se->SaveObject(
              $object_path,
              $object_data,
              array(
                'Auth' => $this->AuthArr,
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            $ErrXML = $this->pager->SrcXML->createElement('Error');
            if ( $e->getCode() == 403 ) {
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Id', 2));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Name', 'Access deny'));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Text', 'Доступ к объекту на запись запрещен!'));
            } else {
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Id', 12));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Name', 'failed to write'));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Text', 'Во время записи данных возникли ошибки: ' . $e->getMessage()));
            }
            return $ErrXML;
        }
  }

  /**
   * пытается сделать ревизию и затем удалить объект
   */
  protected function _remove_object($object_path) {
        $err = $this->_create_revision($object_path);
        if ( $err != NULL )
          return $err;
        $err = $this->_del_object($object_path);
        if ( $err != NULL )
          return $err;
        if ( $this->_check_object($object_path . StoreEngine_RevisionDelimiter . 'DRAFT') == NULL ) {
          $err = $this->_del_object($object_path . StoreEngine_RevisionDelimiter . 'DRAFT');
          if ( $err != NULL )
            return $err;
        }
        if ( $this->_check_object($object_path . StoreEngine_RevisionDelimiter . 'HEAD') == NULL ) {
          $err = $this->_del_object($object_path . StoreEngine_RevisionDelimiter . 'HEAD');
          if ( $err != NULL )
            return $err;
        }
  }

  /*
   * пытается удалить в хранилище объект с именем $object_path
   */
  protected function _del_object($object_path) {
        try {
            $this->se->DestroyObject(
              $object_path,
              array(
                'Auth' => $this->AuthArr,
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            $ErrXML = $this->pager->SrcXML->createElement('Error');
            if ( $e->getCode() == 403 ) {
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Id', 2));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Name', 'Access deny'));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Text', 'Доступ к объекту на запись запрещен!'));
            } else {
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Id', 12));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Name', 'failed to write'));
              $ErrXML->appendChild($this->pager->SrcXML->createElement('Text', 'Во время записи данных возникли ошибки: ' . $e->getMessage()));
            }
            return $ErrXML;
        }
  }


  /*
   * выполняет XSLT трансляцию $xml документа согласно $xml шаблона
   * если в результате трансляции получается кривой XML, генерирует исключение
   * и пишет в LOG_ERR полченный в результате трансляции XML и найденную ошибку в нем
   */
  protected function _xml_translate(XSLTProcessor $xsl, DOMDocument $xml) {
        // использование трансформации xml через строку позволяет получать вложенные
        // внутрь CDATA структуры как XML-ноды. Иначе они все равно остаются строка.
        $ret = new DOMDocument('1.0', 'UTF-8');
        //$ret = $XSLT->transformToDoc($xml);
//        set_error_handler(array(__CLASS__, 'exception_error_handler'));
        try {
          $ret->loadXML($xsl->transformToXML($xml));
        } catch( Exception $e ) {
//          restore_error_handler();
/*          $error_txt = '';
          $xml_parser = xml_parser_create();
          $str = $this->_xml_view(str_replace("\n", '', $xsl->transformToXML($xml)));
          if (!xml_parse($xml_parser, $str, TRUE)) {
              $error_txt .= sprintf('Error "%s" on string %d<br/>',
                xml_error_string(xml_get_error_code($xml_parser)),
                xml_get_current_line_number($xml_parser));
          }
          $tok = strtok($str, "\n");
          $line = 1;
          $error_txt .= "<pre>\n";
          while ($tok !== false) {
            $error_txt .= $line++ . htmlspecialchars($tok) . "<br/>";
            $tok = strtok("\n");
          }
          $error_txt .= "</pre>\n";
          $this->Log->message('Parsing XML error: ' . $error_txt, $e->getCode(), LOG_ERR);*/
          $this->Log->message('Parsing XML error: ' . $this->_xml_parse_error($xml), $e->getCode(), LOG_ERR);
          throw ($e);
        }
//        restore_error_handler();
        return $ret;
  }

  /*
   * обрезаем ревизию от пути к объекту $object_path
   */
  protected function _trunk_revision($object_path) {
        if ( preg_match('%^(.*)' . StoreEngine_RevisionDelimiter . '(REV[0-9]{14}|HEAD|DRAFT)$%', $object_path, $arr) ) {
            return $arr[1];
        }
        return $object_path;
  }

  /*
   * создать ревизию для объекта $object_path
   */
  protected function _create_revision($object_path) {
        $CorePath = $this->_trunk_revision($object_path);
        $NewRevPath = sprintf('%s%sREV%d%d', $CorePath, StoreEngine_RevisionDelimiter, time(), rand(1000,9999));
        foreach ( array(StoreEngine_RevisionDelimiter . 'HEAD', '') as $ObjectPathAddon ) {
          $OldData = $this->_get_object($CorePath . $ObjectPathAddon, TRUE);
          if ( $OldData != NULL ) {
            $err = $this->_put_object($NewRevPath, $OldData);
            if ( $err != NULL )
              return $err;
          }
        }
        return NULL;
  }

  /*
   * пытается получить доступные для создания черновика ревизии объекта $object_path
   * возвращает DOMNode с готовой структурой ревизий, или NULL - если ревизий нет
   */
  protected function _get_revision($object_path) {
        try {
            $RevFile = $this->se->GetObject(
              $object_path,
              array(
                'Auth' => $this->AuthArr,
                'Feature' => array(
                  'Name' => SEF_RevisionList,
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to get any revisions of object from "object_path_src"=' . $object_path, 12);
        }

        // получим имя без ревизий и путей
        $ObjectBase = basename($this->_trunk_revision($object_path));
        // удалим ревизии HEAD and DRAFT перед обработкой $RevFile
        if ( sizeof($RevFile) > 0 )
          unset(
            $RevFile[$ObjectBase . StoreEngine_RevisionDelimiter . 'DRAFT'],
            $RevFile[$ObjectBase . StoreEngine_RevisionDelimiter . 'HEAD']
          );
        
        date_default_timezone_set('Europe/Moscow');
        if ( sizeof($RevFile) == 0 )
            return;
        $RevNode = $this->pager->SrcXML->createElement('Revisions');

        krsort($RevFile);

        foreach ( $RevFile as $Rev=>$RArr ) {
            $ItemXML = $this->pager->SrcXML->createElement('Object');
            $ItemXML->appendChild($this->pager->SrcXML->createElement('Name', $Rev));
            $ItemXML->appendChild(
              $this->pager->SrcXML->createElement(
                'Rev',
                sprintf('%s at %s by %s', 
                  substr($Rev, strpos($Rev,StoreEngine_RevisionDelimiter)+strlen(StoreEngine_RevisionDelimiter)),
                  strftime('%d-%m-%Y %H:%M', $RArr['time_c']),
                  $RArr['author_id']
                )
              )
            );
            $ItemXML->appendChild($this->pager->SrcXML->createElement('Path', dirname($object_path) . '/' . $Rev));
            $ItemXML->appendChild($this->pager->SrcXML->createElement('Size', $RArr['size']));
            $ItemXML->appendChild($this->pager->SrcXML->createElement('Author', $RArr['author_id']));
            $ItemXML->appendChild($this->pager->SrcXML->createElement('LastChange', $RArr['time_c']));
            $ItemXML->appendChild($this->pager->SrcXML->createElement('LastChangeTxt', strftime('%d-%m-%Y %H:%M', $RArr['time_c'])));
            $RevNode->appendChild($ItemXML);
        }
        return $RevNode;
  }

  /*
   * проверяет наличие параметра $param в ноде $node и возвращает его значение при успехе
   * если $check_content==TRUE, реализует XPath in SrcXML для содержимого пареметра $param
   * и возвращает извлеченное значение найденной ноды в SrcXML.
   */
  protected function _check_param($param, $node, $check_content=FALSE) {
      $ParamNL = $this->PageXP->query($param, $node);
      if ( $ParamNL->length == 0 )
            throw new Exception(sprintf('mandatory parameter "%s/%s" is not specified', $node->nodeName, $param), 16);
      if ( ! $check_content )
        return $ParamNL->item(0)->nodeValue;
      $SrcParamNL = $this->SrcXP->query($ParamNL->item(0)->nodeValue);
      if ( $SrcParamNL->length > 0 )
        return $SrcParamNL->item(0)->nodeValue;
      throw new Exception(sprintf('object "%s" specified by parameter "%s/%s" is not found in SrcXML', $ParamNL->item(0)->nodeValue, $node->nodeName, $param), 16);
  }

  function Run($pager, $module) {

        $this->pager = $pager;
        $this->module = $module;
        $this->_init_PageXP($pager);
        $this->SrcXP = new DOMXPath($pager->SrcXML);

  }

}

?>
