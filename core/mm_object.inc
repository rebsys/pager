<?php
/**
 * служебный класс для облегчения манипуляциями модулями на странице
 */
class mm_object {

    private $ARGS;
    private $ReqStack;

    /**
     * инициирует параметры
     *
     * @param array $args
     */
    public function __construct(array $args) {

        $this->ARGS = array();
        foreach ( $args as $varname ) {
            if ( method_exists($this, $this->_buildVarnameMethod($varname)) )
                $this->ARGS[$varname] = '';
            else
                throw new Exception("$varname is unknown parameter");
        }
        $this->ReqStack = array();

    }

    /**
     * Задает текущее значение параметра $varname
     *
     * @param string $varname
     * @param string $value
     */
    public function InitVar($varname, $value) {

        if ( ! isset($this->ARGS[$varname]) )
            throw new Exception("$varname is unknown parameter");

        $this->ARGS[$varname] = $value;

    }

    /**
     * Возвращает значение параметра $varname
     *
     * @param string $varname
     * @return mixed
     */
    public function GetVar($varname) {

        if ( ! isset($this->ARGS[$varname]) )
            throw new Exception("$varname is unknown parameter");

        if ( empty($this->ARGS[$varname]) ) {

            if ( in_array($varname, $this->ReqStack) )
                return '';

            array_push($this->ReqStack, $varname);
            $this->ARGS[$varname] = call_user_func(array($this, $this->_buildVarnameMethod($varname)));
            array_pop($this->ReqStack);
        }

        return $this->ARGS[$varname];

    }

    /**
     * Проверяет не является ли параметр $varname неопределенным?
     *
     * @param string $varname
     * @return bool
     */
    public function CheckEmpty($varname) {

        if ( empty($this->ARGS[$varname]) )
            return TRUE;
        else
            return FALSE;

    }

    /**
     *
     * @param string $varname
     * @return string
     */
    private function _buildVarnameMethod ($varname) {

        return '_getDefault_' . $varname;

    }

    /**
     * определяет default для параметра action
     *
     * @return string
     */
    private function _getDefault_action() {

        return 'add';

    }

    /**
     * определяет default для параметра location
     *
     * @return string
     */
    private function _getDefault_location() {


        if (
            $this->GetVar('action') == 'add' and
            $this->CheckEmpty('query')
        )
                return 'below';
        elseif (
            $this->GetVar('action') == 'add' and
            ! $this->CheckEmpty('query')
        )
                return 'self';
        elseif (
            $this->GetVar('action') == 'del'
        )
                return 'self';
        else
                throw new Exception('Unknown value of parameter "action" = ' . $this->GetVar('action'));

    }


    /**
     * определяет default для параметра query
     *
     * @return string
     */
    private function _getDefault_query() {

        if (
            $this->GetVar('action') == 'add' and
            $this->GetVar('location') == 'below'
        )
            return 'module[last()]';
        elseif (
            $this->GetVar('action') == 'add' and
            $this->GetVar('location') == 'above'
        )
            return 'module[position()=1]';
        elseif (
            $this->GetVar('action') == 'del' and
            $this->GetVar('location') == 'below'
        )
            return 'module[position()=1]';
        elseif (
            $this->GetVar('action') == 'del' and
            $this->GetVar('location') == 'above'
        )
            return 'module[last()]';
        elseif (
            $this->GetVar('action') == 'del' and
            $this->GetVar('location') == 'self'
        )
            return '.';
        else
                throw new Exception('Unable calculate value of parameter "query", must set it');
    }
/*
     * action=add, location=below, query - в самое начало стиска
     * action=add, location=above, query - в самый конец стиска
     * action=add, location=self, query=address - заменяет текущий модуль или ошибка
     *      если модуль не задан
     *
     * action=del, location=below, query - самый первый
     * action=del, location=above, query - самый последний
     * action=del, location=self, query - все модули
     * action=del, location=any, query=address - удаляет указанный модуль
 */

    /**
     * определяет default для параметра Location
     *
     * @return string
     */
    private function _getDefault_query_fault() {


        if ( $this->GetVar('action') == 'del' )
            return 'error';
        else
            return 'default';

    }


}
?>