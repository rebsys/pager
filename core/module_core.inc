<?php
/**
 * Базовые методы для модулей
 *
 */
class module_core {

    /**
     * Указатель на базовый StoreEngine-класс для работы с объектами
     * Инициализируется автоматически в методе Init
     * @var StoreEngine
     */
    protected $se;

    /**
     * Указатель на систему логирования
     * Инициализируется автоматически в методе Init
     * @var logger
     */
    protected $Log;

    /**
     * Указатель на модуль конфигурирования
     * Инициализируется автоматически в методе Init
     * @var logger
     */
    protected $Pac;

    /**
     * Объект DOMXPath для выполнения запросов по $pager->PageXML
     * Инициализируется методом _init_PageXP($pager)
     * 
     * @var DOMXPath
     */
    protected $PageXP;

    /**
     * Обработчик исключений
     */
    public static function exception_error_handler($errno, $errstr, $errfile, $errline ) {
        throw new ErrorException($errstr, 0, $errno, $errfile, $errline);
    }

    /**
     * запускается для инициализации после создания экземпляра класса
     *
     * @param modman_class $mm - ссылка на ModMan служит для доступа к
     * StoreEngine, ModCount и возможны другие общие ресурсы
     * @return void
     */
    function Init($mm) {
        $this->se = $mm->StoreEngine;
        $this->Log = logger::GetLogger();
        $this->PaC = PagerConfig::GetPagerConfig();

        // перехватим все исключения на свой обработчик, restore_error_handler() для возврата
        set_error_handler(array(__CLASS__, 'exception_error_handler'));

        // зададим пользователя для определения прав и установления владельца
        // в дальнейшем здесь будет полученная авторизация при запросе
        $this->AuthArr = array(
            'User' => 'noname',
            'Cred' => '',
        );

    }


    /**
     * Возвращает XML-документ типа DOMDocument из указанного к модулю источника
     * или пустой XML-документ, если источник не найден
     *
     * @param pager_class $pager
     * @param DOMNode $module
     * @return DOMDocument
     */
    function GetFileXML($pager, $module, $suffix='', $prefix='') {

        /* TODO: сделать опциональный аттрибут, который бы указывал на необходимость
         * загрузки документа с заменой текущего SrcXML, или с подгрузкой его в
         * виде очередной корневой ноды.
         */
        
        $XML = new DOMDocument('1.0', 'UTF-8');
        $xml_str = $this->LoadFile($pager, $module, $suffix, $prefix);
        if ( $xml_str === FALSE ) {
            return $XML;
        }

        try {

            $XML->loadXML($xml_str, LIBXML_NOBLANKS);

        } catch (Exception $e) {

          $this->Log->message('Parsing XML error: ' . $this->_xml_parse_error($xml_str), $e->getCode(), LOG_ERR);
          throw new Exception('The file is not valid XML Document', 12);

        }

        return $XML;

    }

    /**
     * Возвращает содержимое объекта, запрошенного модулем $module через
     * параметр file_src, или совпадающий с идентификатором запрошенной
     * страницы и находящийся в том же каталоге, если file_src не задан,
     * но задан Properties/Page_Content/auto_file или указанный в
     * Properties/Page_Content/file_src
     * Возвращает FALSE, если ни одним из способов получить файл не удалось.
     * 
     * @param pager_class $pager
     * @param DOMNode $module
     * @return string
     */
    function LoadFile($pager, $module, $suffix='', $prefix='') {

        if ( $FileSrc = $this->_load_file_src($pager, $module, $suffix, $prefix) ) {
            return $FileSrc;
        } elseif ( $FileSrc =  $this->_load_auto_file($pager, $module, $suffix, $prefix) ) {
            return $FileSrc;
        } elseif ( $FileSrc = $this->_load_auto_file_src($pager, $module, $suffix, $prefix) ) {
            return $FileSrc;
        } else {
            return FALSE;
        }
    }

    /**
     * Возвращает содержимое объекта, запрошенного модулем $module через
     * параметр file_src, или FALSE, если параметр не задан.
     * 
     * @param pager_class $pager
     * @param DOMNode $module
     * @return string
     */
    protected function _load_file_src($pager, $module) {

        $this->_init_PageXP($pager);
        $NList = $this->PageXP->query("file_src", $module);

        if ( $NList->length > 0 ) {

            try {
                $StrFile = $this->se->GetObject('file/' . $NList->item(0)->nodeValue);
                $pager->TransitArray['content_objects'][] = 'file/' . $NList->item(0)->nodeValue;
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                throw new Exception('failed to get an object from file_src', 12);
            }

        } else {

            $StrFile = FALSE;

        }

        return $StrFile;

    }

    /**
     * Возвращает содержимое объекта, определяемого через 
     * Properties/Page_Content/auto_file, т.е. совпадающее с именем
     * страницы и в текущем каталоге.
     * Имя объекта может быть опционально дополнено спереди через $prefix
     * и позади через $suffix
     * Возвращает FALSE, если параметр не задан.
     * 
     * @param pager_class $pager
     * @param DOMNode $module
     * @param string $suffix
     * @param string $prefix
     * @return string
     */
    protected function _load_auto_file($pager, $module, $suffix='', $prefix='') {

        $this->_init_PageXP($pager);

        $path = $this->PaC->GetVar('PageID', ReadOnly_Section);

        $StrFile = FALSE;
        
        if ( $this->PageXP->query("Properties/Page_Content/auto_file")->length > 0 ) {

            try {
                $StrFile = $this->se->GetObject(
                    'file/' . 
                    $prefix . $path . $suffix
                );
                $pager->TransitArray['content_objects'][] = 'file/' . $prefix . $path . $suffix;
            } catch (Exception $e) {
                if ( $this->PageXP->query("Optional", $module)->length == 0 ) {
                    $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                    $this->Log->msg("module '" . $module->attributes->getNamedItem('name')->nodeValue . "' has a error, skipped", LOG_WARNING);
                }
            }

        }

        return $StrFile;

    }

    /**
     * Возвращает содержимое объекта, определяемого через 
     * Properties/Page_Content/file_src
     * Имя объекта может быть опционально дополнено спереди через $prefix
     * и позади через $suffix
     * Возвращает FALSE, если параметр не задан.
     * 
     * @param pager_class $pager
     * @param DOMNode $module
     * @param string $suffix
     * @param string $prefix
     * @return string
     */
    protected function _load_auto_file_src($pager, $module, $suffix='', $prefix='') {

        $this->_init_PageXP($pager);
        $NList = $this->PageXP->query("Properties/Page_Content/file_src");

        $StrFile = FALSE;
        
        if ( $NList->length > 0 ) {

            try {
                $obj_path = 
                    'file/'
                    . dirname($NList->item(0)->nodeValue)
                    . $prefix
                    . basename($NList->item(0)->nodeValue)
                    . $suffix
                ;
                $StrFile = $this->se->GetObject($obj_path);
                $pager->TransitArray['content_objects'][] = $obj_path;
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                $this->Log->msg("module '" . $module->attributes->getNamedItem('name')->nodeValue . "' has a error, skipped", LOG_WARNING);
            }

        }

        return $StrFile;

    }

    /**
     * Возвращает содержимое объекта, напрямую указанного через
     * строку формата XMLPath для PageXML/$path,
     * или FALSE, если параметр не задан.
     * Опциональный $module может задать контекст для поиска $path внутри PageXML
     * 
     * @param pager_class $pager
     * @param string $path
     * @param DOMNode $module опциональный параметр, указывающий контекст для поиска $path
     * @return string
     */
    protected function _load_path_file(pager_class $pager, $path, $module=NULL) {

        $this->_init_PageXP($pager);
        if ( is_a($module, 'DOMNode') )
            $NList = $this->PageXP->query($path, $module);
        else
            $NList = $this->PageXP->query($path);

        $StrFile = FALSE;
        
        if ( $NList->length > 0 ) {

            try {
                $StrFile = $this->se->GetObject('file/' . $NList->item(0)->nodeValue);
            } catch (Exception $e) {
                $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
                throw new Exception('failed to get an object: ' . $path, 12);
            }

        }

        return $StrFile;

    }

    /**
     * Возвращает DOMXPath для страницы $pager->PageXML
     *
     * @param <type> $pager
     * @return DOMXPath
     */
    protected function _init_PageXP($pager) {

        if ( ! $this->PageXP instanceof DOMXPath )
                $this->PageXP = new DOMXPath($pager->PageXML);

        return $this->PageXP;

    }

    /**
     * В исходном от заданного DOMPath $docXP документе DOMDocument
     * создает структуру нод, в соответствии с переданным
     * абсолютным путем $path. Возвращает указатель на самую последнюю
     * в пути ноду.
     *
     * @param DOMPath $docXP
     * @param string $path
     * @return DOMNode
     */
    protected function _create_xml_path(DOMXPath $docXP, $path) {

        $path = preg_replace('#^(.+)/$#', '\1', $path);	// финальный слэш уберем, если есть

        if ( $docXP->query($path)->length > 0 )
            return $docXP->query($path)->item(0);

        $my_path = '/';
        foreach (explode('/', $path) as $node) {
            if (empty($node)) continue;	// пропустим ведущий и возможные двойные слэши
            if ( $docXP->query($my_path . $node)->length == 0 ) {
                $retXML = $docXP->query($my_path)->item(0)->appendChild($docXP->document->createElement($node));
            }
            $my_path .= '/' . $node;
        }
        return $retXML;

    }


  /**
   * фозвращает форматированное сообщение об ошибках разбора XML-документа
   * в строчном формате
   * 
   * @param string $xml_str
   * @return string
   */
  protected function _xml_parse_error($xml_str) {
      $error_txt = '';
      $xml_parser = xml_parser_create();
      $str = $this->_xml_view(str_replace("\n", '', $xml_str));
      if (!xml_parse($xml_parser, $str, TRUE)) {
          $error_txt .= sprintf('Error "%s" on string %d<br/>',
            xml_error_string(xml_get_error_code($xml_parser)),
            xml_get_current_line_number($xml_parser));
      }
      $tok = strtok($str, "\n");
      $line = 1;
      $error_txt .= "<pre>\n";
      while ($tok !== false) {
        $error_txt .= $line++ . htmlspecialchars($tok) . "<br/>";
        $tok = strtok("\n");
      }
      $error_txt .= "</pre>\n";
      return $error_txt;
  }

  /*
   * форматирует строку $str с XML-тагами в многострочный вид с иерархическими отступами
   * возвращает отформатированную строку
   * входящая строка имеет ограничения по структуре:
   * - не более 1000 узлов первого уровня
   * - не глубже 100 узлов в глубину
   *
   * @param string $str
   * @return string
   */
  protected function _xml_view($str) {
      static $deep=0;
      static $offset=0;
      static $line=0;
      static $return = '';
      $step = '   ';
      if ( $deep > 100 )
        throw (new Exception('overdeep'));
      while ( $offset < strlen($str) ) {
//        $return .= sprintf('%04d', $line);
        if ( $line++ > 1000 )
          throw (new Exception('overline'));
        $bp = strpos($str, '<', $offset);
        if ( $bp === FALSE ) {
          $return .= 'ERROR: token "<" is not found in: ' . str_repeat($step, $deep) . substr($str, $offset) . "\n";
          return $return;
        }
        if ( $offset != $bp ) {
          $return .= str_repeat($step, $deep + 1) . substr($str, $offset, $bp-$offset) . "\n";
        }
        $ep = strpos($str, '>', $bp);
        if ( $ep === FALSE ) {
          $return .= 'ERROR: token ">" is not found in: ' . str_repeat($step, $deep) . substr($str, $bp) . "\n";
          return $return;
        }
        if ( 
          ( $str[$bp+1] == '?' and $str[$ep-1] == '?' ) or
          ( $str[$ep-1] == '/' )
        ) {
          $return .= str_repeat($step, ($deep > 0 ? $deep + 1 : $deep)) . substr($str, $bp, $ep - $bp + 1) . "\n";
          $offset = $ep + 1;
          continue;
        }
        if ( $str[$bp+1] == '/' ) {
          $return .= str_repeat($step, $deep) . substr($str, $bp, $ep - $bp + 1) . "\n";
          $offset = $ep+1;
          return $return;
        } else {
          $deep++;
          $return .= str_repeat($step, $deep) . substr($str, $bp, $ep - $bp + 1) . "\n";
          $offset = $ep+1;
          $this->_xml_view($str);
          $deep--;
        }
      }
      
      return $return;
  }

}
?>
