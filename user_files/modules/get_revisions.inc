<?php
/*
 * возвращает все доступные ревизии объекта, заданного в SrcXML через
 * параметр модуля object_path_src
 * 
 */

class get_revisions extends module_core implements module_interface {

  function Run($pager, $module) {

        if (! array_key_exists('RequestDone', $pager->TransitArray) )
          return;
  
        $this->_init_PageXP($pager);
        $NList = $this->PageXP->query('object_path_src', $module);

        if ( $NList->length == 0 )
            throw new Exception('parameter "object_path_src" is not specified', 16);
        $SrcXP = new DOMXPath($pager->SrcXML);
        $ObjPathNL = $SrcXP->query($NList->item(0)->nodeValue);
        if ( $ObjPathNL->length == 0 )
            throw new Exception('Xpath from parameter "object_path_src=' . $NList->item(0)->nodeValue . '" not found', 12);
        $ObjPath = $ObjPathNL->item(0)->nodeValue;

        $RevXML = $pager->SrcXML->appendChild($pager->SrcXML->createElement('Revision4Edit'));

        // проверим есть ли такой объект
        try {
            $ObjExists = $this->se->GetObject(
              $ObjPath,
              array(
                'Auth' => array(
                  'User' => 'noname',
                  'Cred' => '',
                ),
                'Feature' => array(
                  'Name' => SEF_Exists,
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to check exists an object from "object_path_src"=' . $ObjPath, 12);
        }
        if ( ! $ObjExists ) {
            $ErrXML = $pager->SrcXML->createElement('Error');
            $ErrXML->appendChild($pager->SrcXML->createElement('Id', 1));
            $ErrXML->appendChild($pager->SrcXML->createElement('Name', 'Not found'));
            $ErrXML->appendChild($pager->SrcXML->createElement('Text', 'Объект не найден или не существует'));
            $RevXML->appendChild($ErrXML);
            return;
        }

        // получим объект
        try {
            $RevFile = $this->se->GetObject(
              $ObjPath,
              array(
                'Auth' => array(
                  'User' => 'noname',
                  'Cred' => '',
                ),
                'Feature' => array(
                  'Name' => SEF_RevisionList,
                )
              )
            );
        } catch (Exception $e) {
            $this->Log->message('StoreEngine error: ' . $e->getMessage(), $e->getCode(), LOG_ERR);
            throw new Exception('failed to get any revisions of object from "object_path_src"=' . $ObjPath, 12);
        }

        if ( sizeof($RevFile) == 0 )
            $RevXML->appendChild($pager->SrcXML->createElement('Empty', $ObjPath));

        foreach ( $RevFile as $Rev=>$RArr ) {
            $ItemXML = $pager->SrcXML->createElement('Object');
            $ItemXML->appendChild($pager->SrcXML->createElement('Name', $Rev));
            $ItemXML->appendChild($pager->SrcXML->createElement('Path', dirname($ObjPath) . '/' . $Rev));
            $ItemXML->appendChild($pager->SrcXML->createElement('Size', $RArr['size']));
            $ItemXML->appendChild($pager->SrcXML->createElement('Author', $RArr['author_id']));
            $ItemXML->appendChild($pager->SrcXML->createElement('LastChange', $RArr['time_c']));
            $RevXML->appendChild($ItemXML);
        }
  
  }


}

?>
