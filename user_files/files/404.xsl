<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : 404.xsl
    Created on : November 13, 2010, 7:42 PM
    Author     : mic
    Description:
        template for 404 error
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="/">
        Requested page <b><xsl:value-of select="/PageError/PageID" /></b> not found on this site.
    </xsl:template>

</xsl:stylesheet>
