<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : page4edit.xsl
    Created on : November 15, 2010, 7:15 PM
    Author     : mic
    Description:
        набор шаблонов для редактирования страниц
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/root">
        <xsl:apply-templates select="PageList" />
        <xsl:apply-templates select="PageEdit" />
    </xsl:template>

    <xsl:template match="PageList">
        <xsl:apply-templates select="LastEditResult" />
        Выберем страницу для редактирования:<br/>
        <form method="POST" action="editor">
        <select name="path">
            <xsl:apply-templates select="Object" />
        </select>
        <input type="submit" name="Edit" value="Редактировать" />
        <input type="submit" name="Cancel" value="Отменить" />
        </form>
    </xsl:template>

    <xsl:template match="Object">
        <option>
            <xsl:attribute name="value">
                <xsl:value-of select="Path" />
            </xsl:attribute>
            <xsl:choose>
                <xsl:when test="Title">
                    <xsl:value-of select="Title" />
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="Path" />
                </xsl:otherwise>
            </xsl:choose>
        </option>
    </xsl:template>

    <xsl:template match="LastEditResult">
        <p><xsl:value-of select="Message" /></p>
    </xsl:template>

    <xsl:template match="PageEdit">
        <form method="POST" action="editor">
        Это форма для редактирования страницы:<xsl:text> </xsl:text><b>
        <xsl:value-of select="Path" /></b><br/>
        <input type="submit" name="Save" value="Сохранить" />
        <input type="submit" name="Cancel" value="Отменить" />
        <input type="hidden" name="path">
            <xsl:attribute name="value">
                <xsl:value-of select="Path" />
            </xsl:attribute>
        </input>
        <InsertEditorHere></InsertEditorHere>
        </form>
        <a href="http://docs.cksource.com/CKEditor_3.x/Users_Guide">CKEditor Users Guide</a> /
        <a href="http://translate.google.ru/translate?u=http://docs.cksource.com/CKEditor_3.x/Users_Guide&amp;sl=en&amp;tl=ru&amp;hl=&amp;ie=UTF-8">Документация пользователя по редактору ckeditor</a><br/>
        <a href="http://docs.cksource.com/CKFinder_2.x/Users_Guide">CKFinder Users Guide</a> /
        <a href="http://translate.google.ru/translate?hl=en&amp;sl=en&amp;tl=ru&amp;u=http://docs.cksource.com/CKFinder_2.x/Users_Guide">Документация пользователя по ckfinder</a><br/>
    </xsl:template>

</xsl:stylesheet>
