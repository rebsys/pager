<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон, со входом для ошибок страницы и формой ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/">
при открытии черновика надо проверять на возможность записи в него и выдавать дружелюбный ответ при проблемах
        <xsl:apply-templates select="RequestTemplate/ErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ItemsList" />
        <xsl:apply-templates select="ObjectData" />
        <xsl:apply-templates select="ObjectDataPreview" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок страницы, т.е. ошибок по всей форме ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ErrorsSet">
	<div style="border: solid 1px red">
	<div><xsl:text>Эта форма содержит следующие ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода ошибки в общем блоке
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ErrorsSet/Error">
	<li><xsl:value-of select="Text" /></li>
    </xsl:template>

<!--	***************************************************************************************
	общий шаблон для вывода объекта для редактирования
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectData">
        <xsl:apply-templates select="Error" />
        <xsl:apply-templates select="Revisions" />
        <xsl:apply-templates select="Data" />
    </xsl:template>

<!--	***************************************************************************************
	общий шаблон для вывода объекта для просмотра ревизии
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectDataPreview">
        <xsl:apply-templates select="Error" />
        <xsl:apply-templates select="Revisions" />
        <xsl:apply-templates select="Data" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода объекта для сообщения об ошибке
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Error">
	   <p style="color:red;font-weight:bold">Объект не существует или не найден</p>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для просмотра ревизии объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectDataPreview/Data">
	<form method="POST">
	    <h4>Просмотр ревизии</h4>
	    <p>Это старая ревизия, которую можно сохранить в виде черновика для дальнейшего редактирования</p>
           Это ревизии объекта: <xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /><br/>
           Ревизия имеет путь: <xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /><br/>
           После сохранения эта ревизия будет записана как: <xsl:value-of select="../ObjectPathDRAFT" /><br/>
	   <input type="submit" name="SAVE_TO" value="Сохранить объект"/>
	   <input type="submit" name="CANCEL" value="Отменить сохранение"/><br/>
           Поле для просмотра объекта:<br/>
           <textarea id="CKEditor" cols="100" rows="10" name="ObjectData" readonly="yes" style="cursor:not-allowed;">
              <xsl:value-of select="." disable-output-escaping="yes" />
           </textarea><br/>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <script>CKEDITOR.replace('CKEditor',{toolbar:[{name:'test',items:['-']}]});</script>
	</form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода объекта для редактирования
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectData/Data">
	<form method="POST">
	    <p>
<h4>Редактор</h4>
В начале редактирования объекта проверяем наличие ревизии DRAFT для него, и если она есть - сразу открываем черновик. Черновик
можно:
<ol>
<li>сохранить (авто-сохранение потом будет);</li>
<li>записать его как HEAD, при этом текущая версия черновика будет сохранена как новая ревизия объекта;</li>
<li>удалить, при этом ревизия объекта DRAFT будет удалена.</li>
</ol>
Если черновика нет, даем пользователю выбрать для создания черновика ревизию HEAD как основную, и как вспомогательный вариант
можно выбрать любую ревизию типа REV. После просмотра и выбора нужной ревизии пользователь может создать черновик и перейти
непосредственно к редактированию.<br/>
Если нужно откатиться к некоторой предыдущей ревизии объекта, предлагается взять эту ревизию как черновик, который потом будет
сохранен как HEAD и будет создана дублирующая ревизия. Это хуже, чем когда HEAD назначается на любую ревизию по усмотрению
пользователя, но пока оставим так.
	    </p>
           Для редактирования взят объект: <xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /><br/>
           Текущие изменения будут сохраняться как: <xsl:value-of select="../ObjectPathDRAFT" /><br/>
           Релиз документа будет осуществлен как: <xsl:value-of select="../ObjectPathHEAD" /><br/>
	    <input type="submit" name="SAVE" value="Сохранить черновик"/>
	    <input type="submit" name="SAVE_TO" value="Опубликовать"/>
	    <input type="submit" name="DELETE" value="Удалить черновик"/>
	    <input type="submit" name="CANCEL" value="Отменить сохранение"/><br/>
           Поле для редактирование объекта:<br/>
           <textarea id="CKEditor" cols="100" rows="10" name="ObjectData">
              <xsl:value-of select="." disable-output-escaping="yes" />
           </textarea><br/>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathHEAD" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <script>CKEDITOR.replace('CKEditor');</script>
	</form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы для задания объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item[Name='ObjectName']" />
	<input type="submit" name="EDIT" value="OK" />
        </form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для формы для формирования списка доступных объектов
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList/Item[Name='ObjectName']">
	Выберите объект:
        <select>
           <xsl:attribute name="name">
              <xsl:choose>
                  <xsl:when test="Source[@SourceName]">
                      <xsl:value-of select="Source/@SourceName" />
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="Name" />
                  </xsl:otherwise>
              </xsl:choose>
           </xsl:attribute>
           <xsl:apply-templates select="/Object4Edit/Object" />
        </select>
    </xsl:template>

<!--	******************************************************************************************
	шаблон для формы для формирования элементов списка доступных объектов, загруженных из XML
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/Object4Edit/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='ObjectName']/Value">
                  <xsl:if test="//ItemsList/Item[Name='ObjectName']/Value=Path">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='ObjectName']/StartValue=Path">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Path" />
	   </xsl:attribute>
	   <xsl:value-of select="Name" />
	</option>
    </xsl:template>

<!--	***************************************************************************************
	альтернативный шаблон для индикации ошибки для выбранного объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="1" match="/ObjectRevision[Error]">
	   <p style="color:red;font-weight:bold">Объект не существует или не найден</p>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы номер 2 для задания доступных ревизий объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Revisions">
	<form method="POST">
	    Для этого объекта существуют ревизии:
	    <select name="Revision">
		<xsl:apply-templates select="Object" />
	    </select>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <input type="submit" name="PREVIEW" value="Посмотреть" />
	</form>
    </xsl:template>


<!--	***************************************************************************************
	шаблон для формы для формирования элементов списка доступных ревизий выбранного объекта 
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Revisions/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='Revision']/Value">
                  <xsl:if test="//ItemsList/Item[Name='Revision']/Value=Path">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='Revision']/StartValue=Path">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Path" />
	   </xsl:attribute>
	   <xsl:value-of select="Rev" />
	</option>
    </xsl:template>

</xsl:stylesheet>
