<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!-- чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом -->
    <xsl:template priority="-9" match="*" />

<!-- основной шаблон, со входом для ошибок страницы и формой ввода с входными параметрами -->
    <xsl:template match="/RequestTemplate">
	AJAX Status: <span id="AJAX_Status"></span><br/>
	AJAX Timer: <span id="AJAX_Timer"></span><br/>
	AJAX Result: <div id="AJAX_Result"></div><br/>
        <xsl:apply-templates select="ErrorsSet" />
        <xsl:apply-templates select="ItemsList" />
    </xsl:template>

<!-- шаблон для вывода общего блока ошибок страницы, т.е. ошибок по всей форме ввода -->
    <xsl:template match="ErrorsSet">
	<div style="border: solid 1px red">
	<div><xsl:text>Эта форма содержит следующие ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!-- шаблон для вывода ошибки в общем блоке -->
    <xsl:template match="ErrorsSet/Error">
	<li><xsl:value-of select="Text" /></li>
    </xsl:template>

<!-- шаблон для вывода индивидуальных ошибок для каждого поля ввода -->
    <xsl:template match="Item/Error">
	<strong><xsl:text> The field has error: </xsl:text></strong>
	<span style="color:red">
<!-- можно указать текст из описания самой ошибки:
	   <xsl:value-of select="Text" />
     а можно по коду этой ошибки взять из общего справочника страницы:
-->
	   <xsl:value-of select="//ErrorsRef/Error[Id=current()/Id]/Text" />
	</span>
    </xsl:template>

<!-- шаблон для формы ввода со списком входных параметров -->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item" />
        <input type="submit" name="SUBMIT" value="Редактировать" />
        <input type="submit" name="Cancel" value="Отменить" />
        </form>
    </xsl:template>

<!-- шаблон для вывода option в поле типа Select  -->
    <xsl:template match="Item[InputType='Select']/Option">
	<option>
           <xsl:choose>
              <xsl:when test="../Value">
                 <xsl:if test="../Value=.">
                    <xsl:attribute name="selected">yes</xsl:attribute>
                 </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="../StartValue=.">
                    <xsl:attribute name="selected">yes</xsl:attribute>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:value-of select="." />
	</option>
    </xsl:template>

<!-- шаблон для поля ввода, с поддержкой дополнительных описаний поля, мандатори поля, поля типа input or textarea,
с возможностью задать предварительное значение поля, с выделением мандатори и выводом ошибок для поля -->
    <xsl:template match="ItemsList/Item">
<!-- определим и выведем различные типы описаний/названий для поля -->
	<xsl:choose>
	    <xsl:when test="TextPrefix">
	        <xsl:value-of select="TextPrefix" />
	    </xsl:when>
	    <xsl:when test="Description">
	        <xsl:value-of select="Description" />
	    </xsl:when>
	    <xsl:otherwise>
	        <xsl:value-of select="Name" />
	    </xsl:otherwise>
	</xsl:choose>
<!-- разделитель между описанием и полем ввода -->
	<xsl:text>: </xsl:text>
<!-- указанием на обязательное заполнение этого поля -->
	<xsl:if test="Mandatory='On'">
	    <sup style="color:red">*</sup>
	</xsl:if>
<!-- определяем разные типы полей для разного типа входных данных: input, textarea, etc. -->
	<xsl:choose>
	    <xsl:when test="InputType='Textarea'">
<!-- для поля типа Textarea нарисуем textarea, можно и другие характеристики задать сразу в xml-шаблоне -->
		<textarea cols="30" rows="5">
		   <xsl:attribute name="name">
		      <xsl:choose>
			  <xsl:when test="Source[@SourceName]">
		     	      <xsl:value-of select="Source/@SourceName" />
			  </xsl:when>
			  <xsl:otherwise>
		     	      <xsl:value-of select="Name" />
			  </xsl:otherwise>
	     	      </xsl:choose>
        	   </xsl:attribute>
<!-- выводя в xsl-шаблон блоки данных, включающие html-таги, нужно отключать при выводе квотирование спец.символов типа '<'
     делается это добавлением аттрибута:
	disable-output-escaping="yes"
     Например здесь можно было бы сделать так:
	<xsl:value-of select="Value" disable-output-escaping="yes" />
     но необходимо учитывать, что в данных может попасться закрывающая текущий таг конструкция, и тогда остаток данных
     будет воспринят браузером как полноценный html-код и будет исполнен. Прекрасная возможность внедрять активные XSS.
-->
		   <xsl:value-of select="Value" />
		</textarea>
	    </xsl:when>
	    <xsl:when test="InputType='Select'">
		<select>
		   <xsl:attribute name="name">
		      <xsl:choose>
			  <xsl:when test="Source[@SourceName]">
		     	      <xsl:value-of select="Source/@SourceName" />
			  </xsl:when>
			  <xsl:otherwise>
		     	      <xsl:value-of select="Name" />
			  </xsl:otherwise>
	     	      </xsl:choose>
        	   </xsl:attribute>
		   <xsl:apply-templates select="Option" />
		</select>
	    </xsl:when>
	    <xsl:otherwise>
<!-- для поля типа input, сделаем таг input. таким же способом можно задавать в шаблоне и radio, select и все прочее -->
		<input>
		   <xsl:attribute name="name">
		      <xsl:choose>
			  <xsl:when test="Source[@SourceName]">
		     	      <xsl:value-of select="Source/@SourceName" />
			  </xsl:when>
			  <xsl:otherwise>
		     	      <xsl:value-of select="Name" />
			  </xsl:otherwise>
	     	      </xsl:choose>
        	   </xsl:attribute>
		   <xsl:attribute name="value">
		       <xsl:choose>
			  <xsl:when test="Value">
			     <xsl:value-of select="Value" />
			  </xsl:when>
			  <xsl:otherwise>
		     	      <xsl:value-of select="StartValue" />
			  </xsl:otherwise>
		       </xsl:choose>
	           </xsl:attribute>
		</input>
	    </xsl:otherwise>
	</xsl:choose>
<!-- можно вывести заданный в xml-шаблоне некий суфикс для поля -->
        <xsl:value-of select="TextSuffix" />
<!-- в конце строки можно вывести шаблон для показа ошибки конкретно для этого поля ввода -->
        <xsl:apply-templates select="Error" />
	<br/>
    </xsl:template>

</xsl:stylesheet>
