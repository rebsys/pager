<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!-- чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом -->
    <xsl:template priority="-9" match="*" />

<!-- основной шаблон, со входом для ошибок страницы и формой ввода -->
    <xsl:template match="/">
        <xsl:apply-templates select="RequestTemplate/ErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ItemsList" />
        <xsl:apply-templates select="Revision4Edit" />
        <xsl:apply-templates select="ObjectData" />
    </xsl:template>

<!-- шаблон для вывода общего блока ошибок страницы, т.е. ошибок по всей форме ввода -->
    <xsl:template match="ErrorsSet">
	<div style="border: solid 1px red">
	<div><xsl:text>Эта форма содержит следующие ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!-- шаблон для вывода ошибки в общем блоке -->
    <xsl:template match="ErrorsSet/Error">
	<li><xsl:value-of select="Text" /></li>
    </xsl:template>

<!-- шаблон для вывода объекта для просмотра/редактирования -->
    <xsl:template match="/ObjectData">
	<form method="POST" action="save_object">
           Для редактирования взят объект: <xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /><br/>
           Поле для редактирование объекта:<br/>
           <textarea cols="100" rows="10" name="ObjectData">
              <xsl:value-of select="." disable-output-escaping="yes" />
           </textarea><br/>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /></xsl:attribute>
	    </input>
	    <input type="submit" name="SAVE" value="Сохранить объект"/>
	    <input type="reset" name="CANCEL" value="Отменить сохранение"/>
	</form>
    </xsl:template>

<!-- шаблон для определения формы номер 1 для задания объекта -->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item[Name='ObjectName']" />
	<input type="submit" name="SUBMIT" value="OK" />
        </form>
    </xsl:template>

<!-- шаблон для формы для формирования списка доступных объектов -->
    <xsl:template match="ItemsList/Item[Name='ObjectName']">
	Выберите объект:
        <select>
           <xsl:attribute name="name">
              <xsl:choose>
                  <xsl:when test="Source[@SourceName]">
                      <xsl:value-of select="Source/@SourceName" />
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="Name" />
                  </xsl:otherwise>
              </xsl:choose>
           </xsl:attribute>
           <xsl:apply-templates select="/Object4Edit/Object" />
        </select>
    </xsl:template>

<!-- шаблон для формы для формирования элементов списка доступных объектов -->
    <xsl:template match="/Object4Edit/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='ObjectName']/Value">
                  <xsl:if test="//ItemsList/Item[Name='ObjectName']/Value=Path">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='ObjectName']/StartValue=Path">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Path" />
	   </xsl:attribute>
	   <xsl:value-of select="Name" />
	</option>
    </xsl:template>

<!-- альтернативный шаблон для индикации ошибки для выбранного объекта -->
    <xsl:template priority="1" match="/Revision4Edit[Error]">
	   <p style="color:red;font-weight:bold">Объект не существует или не найден</p>
    </xsl:template>

<!-- шаблон для определения формы номер 2 для задания доступных ревизий объекта -->
    <xsl:template match="/Revision4Edit">
	<form method="POST">
	    Выберите ревизию объекта:
	    <select name="Revision">
		<xsl:apply-templates select="/Revision4Edit/Object" />
		<xsl:apply-templates select="/Revision4Edit/Empty" />
	    </select>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <input type="submit" name="SUBMIT" value="OK" />
	</form>
    </xsl:template>

<!-- шаблон для формы для формирования элементов списка доступных ревизий выбранного объекта -->
    <xsl:template match="/Revision4Edit/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='Revision']/Value">
                  <xsl:if test="//ItemsList/Item[Name='Revision']/Value=Path">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='Revision']/StartValue=Path">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Path" />
	   </xsl:attribute>
	   <xsl:value-of select="Name" />
	</option>
    </xsl:template>

<!-- шаблон для формы для формирования пустого списка ревизий выбранного объектов -->
    <xsl:template match="/Revision4Edit/Empty">
        <option>
	   <xsl:attribute name="value">
	      <xsl:value-of select="//ItemsList/Item[Name='ObjectName']/Value" />
	   </xsl:attribute>
	   <xsl:text>Нет доступных версий</xsl:text>
	</option>
    </xsl:template>

</xsl:stylesheet>
