<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : menu.xsl
    Created on : October 13, 2010, 5:04 PM
    Author     : mic
    Description:
        шаблон для меню
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

    <xsl:template match="RootMenu">
        <xsl:apply-templates select="Menu"/>
    </xsl:template>

    <xsl:template match="Menu">
      <ul>
        <li><xsl:value-of select="Title" />
        <xsl:text> </xsl:text>
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="URL" />
            </xsl:attribute>
            <xsl:value-of select="URL" />
        </a>
        <xsl:if test="@active"> &lt;&lt;&lt; You are here! :)</xsl:if>
        </li>
        <xsl:apply-templates select="Menu"/>
      </ul>
    </xsl:template>

</xsl:stylesheet>
