<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон, со входом для ошибок страницы и формой ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/">
	<h3>Создание новой страницы</h3>
        <xsl:apply-templates select="ModuleErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ErrorsSet" />
        <xsl:apply-templates select="/RequestResult/Report/Text" />
        <xsl:apply-templates select="/ModuleReportSet/Report/Text" />
        <xsl:apply-templates select="RequestTemplate/ItemsList" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок для всех работающих модулей
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ModuleErrorsSet">
	<div style="border: double 2px red">
	<div><xsl:text>В процессе обработки данных возникли ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы для задания объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item" />
	<input type="submit" name="ADD" value="Создать выбранные объекты" />
        </form>
    </xsl:template>


</xsl:stylesheet>
