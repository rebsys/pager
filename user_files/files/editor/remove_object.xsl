<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон, со входом для ошибок страницы и формой ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/">
	<h3>Удаление существующей страницы</h3>
        <xsl:apply-templates select="ModuleErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ErrorsSet" />
        <xsl:apply-templates select="/RequestResult/Report/Text" />
        <xsl:apply-templates select="/ModuleReportSet/Report/Text" />
        <xsl:apply-templates select="RequestTemplate/ItemsList" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок для всех работающих модулей
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ModuleErrorsSet">
	<div style="border: double 2px red">
	<div><xsl:text>В процессе обработки данных возникли ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы для задания объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item" />
	<input type="submit" name="REMOVE" value="Удалить выбранные объекты" />
        </form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для формы для формирования списка доступных объектов
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="1" match="ItemsList/Item[Name='ObjectName']">
	<xsl:choose>
	    <xsl:when test="TextPrefix">
	        <xsl:value-of select="TextPrefix" />
	    </xsl:when>
	    <xsl:when test="Description">
	        <xsl:value-of select="Description" />
	    </xsl:when>
	    <xsl:otherwise>
	        <xsl:value-of select="Name" />
	    </xsl:otherwise>
	</xsl:choose>
<!-- разделитель между описанием и полем ввода -->
	<xsl:text>: </xsl:text>
<!-- указанием на обязательное заполнение этого поля -->
	<xsl:if test="Mandatory='On'">
	    <sup style="color:red">*</sup>
	</xsl:if>
	<select>
           <xsl:attribute name="name">
              <xsl:choose>
                  <xsl:when test="Source[@SourceName]">
                      <xsl:value-of select="Source/@SourceName" />
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="Name" />
                  </xsl:otherwise>
              </xsl:choose>
           </xsl:attribute>
           <xsl:apply-templates select="/Object4Edit/Object" />
        </select>
	<xsl:value-of select="TextSuffix" />
        <xsl:apply-templates select="Error" />
	<br/>
    </xsl:template>

<!--	******************************************************************************************
	шаблон для формы для формирования элементов списка доступных объектов, загруженных из XML
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/Object4Edit/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='ObjectName']/Value">
                  <xsl:if test="//ItemsList/Item[Name='ObjectName']/Value=Name">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='ObjectName']/StartValue=Name">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Name" />
	   </xsl:attribute>
	   <xsl:value-of select="Name" />
	</option>
    </xsl:template>

</xsl:stylesheet>
