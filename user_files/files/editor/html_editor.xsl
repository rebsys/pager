<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон, со входом для ошибок страницы и формой ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/">
        <xsl:apply-templates select="ModuleErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ItemsList" />
        <xsl:apply-templates select="ObjectData" />
        <xsl:apply-templates select="ObjectDataPreview" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок страницы, т.е. ошибок по всей форме ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ErrorsSet">
	<div style="border: solid 1px red">
	<div><xsl:text>Эта форма содержит следующие ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок для всех работающих модулей
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ModuleErrorsSet">
	<div style="border: double 2px red">
	<div><xsl:text>В процессе обработки данных возникли ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода ошибки в общем блоке
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ErrorsSet/Error">
	<li><xsl:value-of select="Text" /></li>
    </xsl:template>

<!--	***************************************************************************************
	общий шаблон для вывода объекта для редактирования
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectData">
        <xsl:apply-templates select="Error" />
        <xsl:apply-templates select="Revisions" />
        <xsl:apply-templates select="Data" />
    </xsl:template>

<!--	***************************************************************************************
	общий шаблон для вывода объекта для просмотра ревизии
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectDataPreview">
        <xsl:apply-templates select="Error" />
        <xsl:apply-templates select="Revisions" />
        <xsl:apply-templates select="Data" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода объекта для сообщения об ошибке
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Error">
	   <p style="color:red;font-weight:bold">
		<xsl:value-of select="Text" />
	   </p>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для просмотра ревизии объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectDataPreview/Data">
	<form method="POST">
	Возможные действия: <input type="submit" name="SAVE_TO" value="Сохранить как черновик"/>
	   <input type="submit" name="CANCEL" value="Отменить сохранение"/><br/>
	<a href="#" style="font-size:9pt;border: ridge blue;text-decoration:none;margin-right:10px" onClick="$('#t-info').toggle(300);">отладочная информация</a><br/>
	<div style="border: solid 1px black;font-size:small;font-style:italic" id="t-info">
           Это ревизии объекта: <xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /><br/>
           Ревизия имеет путь: <xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /><br/>
           После сохранения эта ревизия будет записана как: <xsl:value-of select="../ObjectPathDRAFT" /><br/>
	</div>
	   <p style="font-size:small">В окне ниже отображена одна из более старых версий страницы, которую можно
	   сохранить вместо текущего черновика страницы. Этот черновик можно будет в дальнейшем использовать для
	   редактирования и последующей публикации.</p>
           <textarea id="CKEditor" cols="100" rows="10" name="ObjectData" readonly="yes" style="cursor:not-allowed;">
              <xsl:value-of select="." disable-output-escaping="yes" />
           </textarea><br/>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <script>
		var editor;
		$(document).ready(function(){
			//$('textarea#CKEditor').ckeditor({toolbar:[{name:'test',items:['-']}]});
			$('textarea#CKEditor').ckeditor({toolbar:[[ 'Source', 'Copy', 'Find', 'Preview', 'Maximize', 'ShowBlocks', 'About' ]]});
			$('#t-info').hide();
		});
                CKEDITOR.on( 'instanceReady', function( ev ) {
                        editor = ev.editor;
	                editor.setReadOnly( true );
                });
	    </script>
	</form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода объекта для редактирования
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectData/Data">
	<form method="POST">
	Возможные действия: <input type="submit" name="SAVE_TO" value="Опубликовать черновик как замену текущему содержимому страницы"/>
	<div>
	<a href="#" style="font-size:9pt;border: ridge magenta;text-decoration:none;margin-right:10px" onClick="$('#s-do').toggle(300);">дополнительные действия</a> 
	<a href="#" style="font-size:9pt;border: ridge blue;text-decoration:none;margin-right:10px" onClick="$('#t-info').toggle(300);">отладочная информация</a>
	<div style="border: solid 1px black;font-size:small;font-style:italic" id="t-info">
           Для редактирования взят объект: <xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /><br/>
           Текущие изменения будут сохраняться как: <xsl:value-of select="../ObjectPathDRAFT" /><br/>
           Релиз документа будет осуществлен как: <xsl:value-of select="../ObjectPathHEAD" /><br/>
	   Статус автоматического сохранения документа: <span id='AJAX_Result'/><br/>
	   Ответ сервера при сохранении: <div id='AJAX_Response'/>
	</div>
	<div style="" id="s-do">
	    <input type="submit" name="SAVE" value="Сохранить черновик"/>
	    <input type="submit" name="DELETE" value="Удалить черновик"/>
	    <input type="submit" name="CANCEL" value="Отменить редактирование"/><br/>
	</div>
	</div>
<!--
	   Документ будет автоматически сохранен через: <span id="auto-timer">0</span> секунд.<br/>
	   Статус авто-сохранения документа: <span id="auto-status">Документ не требует сохранения</span><br/>
-->
	<p style="font-size:small">Черновик этой страницы доступен в
	редакторе ниже.  Можно опубликовать этот черновик, как основное содержимое
	страницы, при этом текущий вариант страницы будет сохранен в виде одной из
	прошлых версий. Статус документа: <span id="auto_status">сохранен</span><span id="auto_timer"></span>
	</p>
           <textarea id="CKEditor" cols="100" rows="10" name="ObjectData">
              <xsl:value-of select="." disable-output-escaping="yes" />
           </textarea><br/>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathHEAD" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <script>
		var editor;
                var TimerAJAX=0;
		var AS;
		$(document).ready(function(){
			CKFinder.setupCKEditor( null, '/pager/ckfinder/' );
			$('textarea#CKEditor').ckeditor();
			AS = $('span#auto_status');
			AT = $('span#auto_timer');
                        //InitSaveTime();
			$('#t-info').hide();
                        $('#s-do').hide();
		});
                CKEDITOR.on( 'instanceReady', function( ev ) {
                        editor = ev.editor;
			editor.on('change', function(ev) {
				InitSaveTime();
			});
                });

                function InitSaveTime() {
			if ( TimerAJAX == 0 ) {
                                AS.html("Документ изменен, будет сохранен через: ");
	                        TimerAJAX = 5;
				SaveTimeClock();
			}
                        TimerAJAX = 5;
			AT.html(TimerAJAX+' секунд');
                }

                function SaveTimeClock() {
                        if ( TimerAJAX == 1 ) {
                                $.post(
                                   'editor_ajax_gate',
                                   {
                                     ObjectPath: $("input[name='ObjectPath']").val(),
                                     ObjectData: $("textarea[name='ObjectData']").val(),
                                   },
                                   function(data,status) {GetResultAJAX(data,status);} 
                                );
                        }
			if ( TimerAJAX &gt; 0 ) {
                                TimerAJAX = TimerAJAX-1;
				if ( TimerAJAX == 0 ) {
					AT.html('');
				} else {
                        		AT.html(TimerAJAX+' секунд');
				}
                        	window.setTimeout('SaveTimeClock()', 1000);
			}
		}
                $(document).ajaxStart(function(){
                        ResultAJAX = false;
                        AS.html("Начали передавать данные...");
                });
                $(document).ajaxSuccess(function(){
                        if ( ResultAJAX ) {
                                AS.html("Документ сохранен");
                        } else {
                                AS.html("&lt;span style='color:red'&gt;При автоматическом сохранении документа возникли проблемы, документ не сохранен!&lt;/span&gt;");
                        }
                });
                $(document).ajaxError(function(){
                        AS.html("&lt;span style='color:red'&gt;ОШИБКА СОХРАНЕНИЯ!!!&lt;/span&gt;");
                });

                function GetResultAJAX(data, status) {
                        $("span[id='AJAX_Result']").html(status);
                        $("div[id='AJAX_Response']").html(data);
                        if (
                                status == 'success'
                                &amp;&amp; data == 'OK'
                        ) {
                                ResultAJAX = true;
                        }
		}
	    </script>
	</form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы для задания объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item[Name='ObjectName']" />
	<input type="submit" name="EDIT" value="OK" />
        </form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для формы для формирования списка доступных объектов
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList/Item[Name='ObjectName']">
	Чтобы начать редактировать содержимое страницы, пожалуйста, выберите нужную:
	<select>
           <xsl:attribute name="name">
              <xsl:choose>
                  <xsl:when test="Source[@SourceName]">
                      <xsl:value-of select="Source/@SourceName" />
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="Name" />
                  </xsl:otherwise>
              </xsl:choose>
           </xsl:attribute>
           <xsl:apply-templates select="/Object4Edit/Object" />
        </select>
    </xsl:template>

<!--	******************************************************************************************
	шаблон для формы для формирования элементов списка доступных объектов, загруженных из XML
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/Object4Edit/Object[File]">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='ObjectName']/Value">
                  <xsl:if test="//ItemsList/Item[Name='ObjectName']/Value=File">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='ObjectName']/StartValue=File">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="File" />
	   </xsl:attribute>
	   <xsl:value-of select="Name" />
	</option>
    </xsl:template>

<!--	***************************************************************************************
	альтернативный шаблон для индикации ошибки для выбранного объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="1" match="/ObjectRevision[Error]">
	   <p style="color:red;font-weight:bold">Объект не существует или не найден</p>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы номер 2 для задания доступных ревизий объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Revisions">
	<form method="POST">
	    Кроме текущей версии, для этой страницы существуют более ранние версии:
	    <select name="Revision">
		<xsl:apply-templates select="Object" />
	    </select>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <input type="submit" name="PREVIEW" value="Посмотреть" />
	</form>
    </xsl:template>


<!--	***************************************************************************************
	шаблон для формы для формирования элементов списка доступных ревизий выбранного объекта 
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Revisions/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='Revision']/Value">
                  <xsl:if test="//ItemsList/Item[Name='Revision']/Value=Path">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='Revision']/StartValue=Path">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Path" />
	   </xsl:attribute>
	   <xsl:value-of select="Rev" />
	</option>
    </xsl:template>

</xsl:stylesheet>
