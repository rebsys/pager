<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для получения редактируемых полей в объекте типа ObjectPage
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml"/>

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/root">
	<Data>
		<xsl:apply-templates select="Properties" />
	</Data>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для свойств страницы
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Properties">
	<Node>
	    <Name>HTML_Header</Name>
            <XPath>/root/Properties/HTML_Header</XPath>
            <Value><xsl:value-of select="HTML_Header" disable-output-escaping="yes" /></Value>
	</Node>
	<Node>
	    <Name>TestParam</Name>
            <XPath>/root/Properties/TestParam</XPath>
            <Value><xsl:value-of select="TestParam" /></Value>
	</Node>
    </xsl:template>

</xsl:stylesheet>
