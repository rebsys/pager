<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для получения редактируемых полей в объекте типа ObjectPage
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="xml" encoding="UTF-8" />

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/">
	<Data>
		<xsl:apply-templates select="RequestResult" />
	</Data>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для свойств страницы
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="RequestResult">
	<Node>
	    <Name>HTML_Header</Name>
            <XPath>/root/Properties/HTML_Header</XPath>
            <Value><xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text>
		<title><xsl:value-of select="Item[Name='title']/Value" /></title>
		<meta name="keywords">
			<xsl:attribute name="content"><xsl:value-of select="Item[Name='keywords']/Value" /></xsl:attribute>
		</meta>
		<meta name="description">
			<xsl:attribute name="content"><xsl:value-of select="Item[Name='description']/Value" /></xsl:attribute>
		</meta>
	    <xsl:text disable-output-escaping="yes">]]&gt;</xsl:text></Value>
	</Node>
	<Node>
	    <Name>TestParam</Name>
            <XPath>/root/Properties/TestParam</XPath>
            <Value><xsl:value-of select="Item[Name='TestParam']/Value" /></Value>
	</Node>
    </xsl:template>

</xsl:stylesheet>
