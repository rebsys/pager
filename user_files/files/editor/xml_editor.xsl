<?xml version="1.0" encoding="UTF-8"?>

<!--
	Шаблон для построения формы ввода данных,
	использующей AJAX для обновления данных
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html"/>

<!--	***************************************************************************************
	чтобы все прочие данные не засоряли нам вывод, сделаем для них пустой шаблон с самым низким приоритетом
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="-9" match="*" />

<!--	***************************************************************************************
	основной шаблон, со входом для ошибок страницы и формой ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/">
        <xsl:apply-templates select="ModuleErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ErrorsSet" />
        <xsl:apply-templates select="RequestTemplate/ItemsList" />
        <xsl:apply-templates select="ObjectData" />
        <xsl:apply-templates select="ObjectDataPreview" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок страницы, т.е. ошибок по всей форме ввода
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ErrorsSet">
	<div style="border: solid 1px red">
	<div><xsl:text>Эта форма содержит следующие ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода общего блока ошибок для всех работающих модулей
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ModuleErrorsSet">
	<div style="border: double 2px red">
	<div><xsl:text>В процессе обработки данных возникли ошибки:</xsl:text></div>
	<ul>
        <xsl:apply-templates select="Error" />
	</ul>
	</div>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода ошибки в общем блоке
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ErrorsSet/Error">
	<li><xsl:value-of select="Text" /></li>
    </xsl:template>

<!--	***************************************************************************************
	общий шаблон для вывода объекта для редактирования
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectData">
        <xsl:apply-templates select="Error" />
        <xsl:apply-templates select="Revisions" />
        <xsl:apply-templates select="Data" />
    </xsl:template>

<!--	***************************************************************************************
	общий шаблон для вывода объекта для просмотра ревизии
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectDataPreview">
        <xsl:apply-templates select="Error" />
        <xsl:apply-templates select="Revisions" />
        <xsl:apply-templates select="Data" />
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода объекта для сообщения об ошибке
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Error">
	   <p style="color:red;font-weight:bold">
		<xsl:value-of select="Text" />
	   </p>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для просмотра ревизии объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectDataPreview/Data">
	<form method="POST">
	    Возможные действия: <input type="submit" name="SAVE_TO" value="Сохранить как черновик"/>
	   <input type="submit" name="CANCEL" value="Отменить сохранение"/><br/>
	<a href="#" style="font-size:9pt;border: ridge blue;text-decoration:none;margin-right:10px" onClick="$('#t-info').toggle(300);">отладочная информация</a><br/>
	<div style="border: solid 1px black;font-size:small;font-style:italic" id="t-info">
           Это ревизии объекта: <xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /><br/>
           Ревизия имеет путь: <xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /><br/>
           После сохранения эта ревизия будет записана как: <xsl:value-of select="../ObjectPathDRAFT" /><br/>
	</div>
	<p>
           Эта версия страницы имела следующие свойства:<br/>
	   <p>
	   Заголовок окна страницы (title) : 
	   <input readonly="yes" name="title" size="80">
              <xsl:attribute name="value"><xsl:value-of select="Node[Name='HTML_Header']/Value/title" /></xsl:attribute>
           </input><br/>
	   Ключевые слова для SEO (keywords) : 
	   <input readonly="yes" name="keywords" size="80">
	      <xsl:attribute name="value"><xsl:value-of select="Node[Name='HTML_Header']/Value/meta[@name='keywords']/@content" /></xsl:attribute>
           </input><br/>
	   Краткое описание страницы для SEO (description) : 
	   <input readonly="yes" name="description" size="80">
	      <xsl:attribute name="value"><xsl:value-of select="Node[Name='HTML_Header']/Value/meta[@name='description']/@content" /></xsl:attribute>
           </input><br/>
	   </p>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	</p>
	</form>
	<script>
		$(document).ready(function(){
			$('#t-info').hide();
		});
	</script>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для вывода объекта для редактирования
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/ObjectData/Data">
	<form method="POST">
	Возможные действия: 
	    <input type="submit" name="SAVE" value="Сохранить черновик"/>
	    <input type="submit" name="SAVE_TO" value="Опубликовать черновик как замену текущим параметрам страницы"/>
	<div>
	<a href="#" style="font-size:9pt;border: ridge magenta;text-decoration:none;margin-right:10px" onClick="$('#s-do').toggle(300);">дополнительные действия</a> 
	<a href="#" style="font-size:9pt;border: ridge blue;text-decoration:none;margin-right:10px" onClick="$('#t-info').toggle(300);">отладочная информация</a>
	<div style="border: solid 1px black;font-size:small;font-style:italic" id="t-info">
           Для редактирования взят объект: <xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /><br/>
           Текущие изменения будут сохраняться как: <xsl:value-of select="../ObjectPathDRAFT" /><br/>
           Релиз документа будет осуществлен как: <xsl:value-of select="../ObjectPathHEAD" /><br/>
	</div>
	<div style="" id="s-do">
	    <input type="submit" name="DELETE" value="Удалить черновик"/>
	    <input type="submit" name="CANCEL" value="Отменить редактирование"/><br/>
	</div>
	</div>
	<p>
           Черновик этой версии страницы сейчас имеет следующие свойства:<br/>
	   <p>
	   Заголовок окна страницы (title) : 
	   <input name="title" size="80">
              <xsl:attribute name="value"><xsl:value-of select="Node[Name='HTML_Header']/Value/title" /></xsl:attribute>
           </input><br/>
	   Ключевые слова для SEO (keywords) : 
	   <input name="keywords" size="80">
              <xsl:attribute name="value"><xsl:value-of select="Node[Name='HTML_Header']/Value/meta[@name='keywords']/@content" /></xsl:attribute>
           </input><br/>
	   Краткое описание страницы для SEO (description) : 
	   <input name="description" size="80">
              <xsl:attribute name="value"><xsl:value-of select="Node[Name='HTML_Header']/Value/meta[@name='description']/@content" /></xsl:attribute>
           </input><br/>
	   </p>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='Revision']/Value" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPath">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathDRAFT" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectPathDst">
                <xsl:attribute name="value"><xsl:value-of select="../ObjectPathHEAD" /></xsl:attribute>
	    </input>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	</p>
	</form>
	<script>
		$(document).ready(function(){
			$('#t-info').hide();
			$('#s-do').hide();
		});
	</script>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы для задания объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList">
	<form method="POST">
        <xsl:apply-templates select="Item[Name='ObjectName']" />
	<input type="submit" name="EDIT" value="OK" />
        </form>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для формы для формирования списка доступных объектов
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="ItemsList/Item[Name='ObjectName']">
	Чтобы начать редактировать свойства страницы, пожалуйста, выберите нужную:
        <select>
           <xsl:attribute name="name">
              <xsl:choose>
                  <xsl:when test="Source[@SourceName]">
                      <xsl:value-of select="Source/@SourceName" />
                  </xsl:when>
                  <xsl:otherwise>
                      <xsl:value-of select="Name" />
                  </xsl:otherwise>
              </xsl:choose>
           </xsl:attribute>
           <xsl:apply-templates select="/Object4Edit/Object" />
        </select>
    </xsl:template>

<!--	******************************************************************************************
	шаблон для формы для формирования элементов списка доступных объектов, загруженных из XML
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="/Object4Edit/Object[Page]">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='ObjectName']/Value">
                  <xsl:if test="//ItemsList/Item[Name='ObjectName']/Value=Page">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='ObjectName']/StartValue=Page">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Page" />
	   </xsl:attribute>
	   <xsl:value-of select="Name" />
	</option>
    </xsl:template>

<!--	***************************************************************************************
	альтернативный шаблон для индикации ошибки для выбранного объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template priority="1" match="/ObjectRevision[Error]">
	   <p style="color:red;font-weight:bold">Объект не существует или не найден</p>
    </xsl:template>

<!--	***************************************************************************************
	шаблон для определения формы номер 2 для задания доступных ревизий объекта
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Revisions">
	<form method="POST">
	    Кроме текущей версии, для этой страницы существуют более ранние версии:
	    <select name="Revision">
		<xsl:apply-templates select="Object" />
	    </select>
	    <input type="hidden" name="ObjectName">
                <xsl:attribute name="value"><xsl:value-of select="/RequestResult/Item[Name='ObjectName']/Value" /></xsl:attribute>
	    </input>
	    <input type="submit" name="PREVIEW" value="Посмотреть" />
	</form>
    </xsl:template>


<!--	***************************************************************************************
	шаблон для формы для формирования элементов списка доступных ревизий выбранного объекта 
	^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
-->
    <xsl:template match="Revisions/Object">
	<option>
           <xsl:choose>
              <xsl:when test="//ItemsList/Item[Name='Revision']/Value">
                  <xsl:if test="//ItemsList/Item[Name='Revision']/Value=Path">
                    <xsl:attribute name="selected"/>
		  </xsl:if>
              </xsl:when>
              <xsl:otherwise>
                 <xsl:if test="//ItemsList/Item[Name='Revision']/StartValue=Path">
                    <xsl:attribute name="selected"/>
                 </xsl:if>
              </xsl:otherwise>
           </xsl:choose>
	   <xsl:attribute name="value">
	      <xsl:value-of select="Path" />
	   </xsl:attribute>
	   <xsl:value-of select="Rev" />
	</option>
    </xsl:template>

</xsl:stylesheet>
